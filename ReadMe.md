# Galaxias
© 1986 by Fergus McNeill

Genre: Science Fiction

YUID: zl8pbm6e1oz6pe2f

IFDB: https://ifdb.org/viewgame?id=zl8pbm6e1oz6pe2f

Play ZX Spectrum original online: https://spectrumcomputing.co.uk/playonline.php?eml=2&downid=39258

## Port Info

This is an act of love, a try to port old ZX IF games to ngPAWS to be able to play them on a browser.
Keep in mind this port is still in alpha (or pre-alpha state) there will be errors and issues. Please, test and report or help with the code :)

Tools: [UnQuill](http://www.seasip.info/Unix/UnQuill/), [The Inker](https://github.com/8bat/the_inker/), [ngPAWS](https://github.com/Utodev/ngPAWS/)
- N Game fully tested?
- Y Missing images?
- Y Errors rendering images?
- Y Modern code for ngPAWS?
- 2 File to edit: txp (1) or sce (2)

## Testing ngPAWS Port

Test alpha port: https://interactivefiction.gitlab.io/galaxias-1986-fergus_mcneill/
and report issues: https://gitlab.com/interactivefiction/galaxias-1986-fergus_mcneill/-/issues/
