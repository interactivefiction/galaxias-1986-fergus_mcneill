//CND LISTSAVEDGAMES A 0 0 0 0

function ACClistsavedgames()
{
  var numberofgames = 0;
  for(var savedgames in localStorage)
  {
    gamePrefix = savedgames.substring(0,16); // takes out ngpaws_savegame_
    if (gamePrefix == "ngpaws_savegame_")
    {
      gameName = savedgames.substring(16);
      writelnText(implementTag("EXTERN|loadgame('" + gameName + "')|" + gameName));
      numberofgames++;
    }
  }
  if (numberofgames == 0) 
  {
     if (getLang()=='EN') writelnText("NO SAVED GAMES FOUND."); else writelnText("No hay ninguna partida guardada.");
  }
}

//LIB LOADGAME

function loadgame(gametitle) 	
{
    filename = gametitle;
	var json_str;
	if (filename == null) filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
    json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
	if (json_str)
	{
		savegame_object = JSON.parse(json_str.trim());
		restoreSaveGameObject(savegame_object);
        ACCanykey();    //Only to make ACCdesc to work
		ACCdesc();
	}
	else
	{
		writeSysMessage(SYSMESS_FILENOTFOUND);
		ACCnewline();
		done_flag = true;
	}
	focusInput();
}
