// This file is (C) Carlos Sanchez 2014, released under the MIT license


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// GLOBAL VARIABLES AND CONSTANTS ///////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// CONSTANTS 
var VOCABULARY_ID = 0;
var VOCABULARY_WORD = 1;
var VOCABULARY_TYPE = 2;

var WORDTYPE_VERB = 0;
var WORDTYPE_NOUN = 1
var WORDTYPE_ADJECT = 2;
var WORDTYPE_ADVERB = 3;
var WORDTYPE_PRONOUN = 4;
var WORDTYPE_CONJUNCTION = 5;
var WORDTYPE_PREPOSITION = 6;

var TIMER_MILLISECONDS  = 40;

var RESOURCE_TYPE_IMG = 1;
var RESOURCE_TYPE_SND = 2;

var PROCESS_RESPONSE = 0;
var PROCESS_DESCRIPTION = 1;
var PROCESS_TURN = 2;

var DIV_TEXT_SCROLL_STEP = 40;


// Aux
var SET_VALUE = 255; // Value assigned by SET condact
var EMPTY_WORD = 255; // Value for word types when no match is found (as for  sentences without adjective or name)
var MAX_WORD_LENGHT = 10;  // Number of characters considered per word
var FLAG_COUNT = 256;  // Number of flags
var NUM_CONNECTION_VERBS = 14; // Number of verbs used as connection, from 0 to N - 1
var NUM_CONVERTIBLE_NOUNS = 20;
var NUM_PROPER_NOUNS = 50; // Number of proper nouns, can't be used as pronoun reference
var EMPTY_OBJECT = 255; // To remark there is no object when the action requires a objno parameter
var NO_EXIT = 255;  // If an exit does not exist, its value is this value
var MAX_CHANNELS = 17; // Number of SFX channels
var RESOURCES_DIR='dat/';


//Attributes
var ATTR_LIGHT=0;			// Object produces light
var ATTR_WEARABLE=1;		// Object is wearable
var ATTR_CONTAINER=2;       // Object is a container
var ATTR_NPC=3;             // Object is actually an NPC
var ATTR_CONCEALED = 4; /// Present but not visible
var ATTR_EDIBLE = 5;   /// Can be eaten
var ATTR_DRINKABLE=6;
var ATTR_ENTERABLE = 7;
var ATTR_FEMALE = 8;
var ATTR_LOCKABLE = 9;
var ATTR_LOCKED = 10;
var ATTR_MALE = 11;
var ATTR_NEUTER=12;
var ATTR_OPENABLE =13;
var ATTR_OPEN=14;
var ATTR_PLURALNAME = 15;
var ATTR_TRANSPARENT=16;
var ATTR_SCENERY=17;
var ATTR_SUPPORTER = 18;
var ATTR_SWITCHABLE=19;
var ATTR_ON  =20;
var ATTR_STATIC  =21;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// INTERNAL STRINGS ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General messages & strings
var STR_NEWLINE = '<br />';
var STR_PROMPT_START = '<span class="feedback">&gt; ';
var STR_PROMPT_END = '</span>';
var STR_RAMSAVE_FILENAME = 'RAMSAVE_SAVEGAME';



// Runtime error messages
var STR_WRONG_SYSMESS = 'WARNING: System message requested does not exist.'; 
var STR_WRONG_LOCATION = 'WARNING: Location requested does not exist.'; 
var STR_WRONG_MESSAGE = 'WARNING: Message requested does not exist.'; 
var STR_WRONG_PROCESS = 'WARNING: Process requested does not exist.' 
var STR_RAMLOAD_ERROR= 'WARNING: You can\'t restore game as it has not yet been saved.'; 
var STR_RUNTIME_VERSION  = 'ngPAWS runtime (C) 2014 Carlos Sanchez.  Released under {URL|http://www.opensource.org/licenses/MIT| MIT license}.\nBuzz sound libray (C) Jay Salvat. Released under the {URL|http://www.opensource.org/licenses/MIT| MIT license} \n jQuery (C) jQuery Foundation. Released under the {URL|https://jquery.org/license/| MIT license}.';
var STR_TRANSCRIPT = 'To copy the transcript to your clipboard, press Ctrl+C, then press Enter';

var STR_INVALID_TAG_SEQUENCE = 'Invalid tag sequence: ';
var STR_INVALID_TAG_SEQUENCE_EMPTY = 'Invalid tag sequence.';
var STR_INVALID_TAG_SEQUENCE_BADPARAMS = 'Invalid tag sequence: bad parameters.';
var STR_INVALID_TAG_SEQUENCE_BADTAG = 'Invalid tag sequence: unknown tag.';
var STR_BADIE = 'You are using a very old version of Internet Explorer. Some features of this product won\'t be avaliable, and other may not work properly. For a better experience please upgrade your browser or install some other one like Firefox, Chrome or Opera.\n\nIt\'s up to you to continue but be warned your experience may be affected.';
var STR_INVALID_OBJECT = 'WARNING: Trying to access object that does not exist'


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////     FLAGS     ///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


var FLAG_LIGHT = 0;
var FLAG_OBJECTS_CARRIED_COUNT = 1;
var FLAG_AUTODEC2 = 2; 
var FLAG_AUTODEC3 = 3;
var FLAG_AUTODEC4 = 4;
var FLAG_AUTODEC5 = 5;
var FLAG_AUTODEC6 = 6;
var FLAG_AUTODEC7 = 7;
var FLAG_AUTODEC8 = 8;
var FLAG_AUTODEC9 = 9;
var FLAG_AUTODEC10 = 10;
var FLAG_ESCAPE = 11;
var FLAG_PARSER_SETTINGS = 12;
var FLAG_PICTURE_SETTINGS = 29
var FLAG_SCORE = 30;
var FLAG_TURNS_LOW = 31;
var FLAG_TURNS_HIGH = 32;
var FLAG_VERB = 33;
var FLAG_NOUN1 =34;
var FLAG_ADJECT1 = 35;
var FLAG_ADVERB = 36;
var FLAG_MAXOBJECTS_CARRIED = 37;
var FLAG_LOCATION = 38;
var FLAG_TOPLINE = 39;   // deprecated
var FLAG_MODE = 40;  // deprecated
var FLAG_PROTECT = 41;   // deprecated
var FLAG_PROMPT = 42; 
var FLAG_PREP = 43;
var FLAG_NOUN2 = 44;
var FLAG_ADJECT2 = 45;
var FLAG_PRONOUN = 46;
var FLAG_PRONOUN_ADJECT = 47;
var FLAG_TIMEOUT_LENGTH = 48;
var FLAG_TIMEOUT_SETTINGS = 49; 
var FLAG_DOALL_LOC = 50;
var FLAG_REFERRED_OBJECT = 51;
var FLAG_MAXWEIGHT_CARRIED = 52;
var FLAG_OBJECT_LIST_FORMAT = 53;
var FLAG_REFERRED_OBJECT_LOCATION = 54;
var FLAG_REFERRED_OBJECT_WEIGHT = 55;
var FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES = 56;
var FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES = 57;
var FLAG_EXPANSION1 = 58;
var FLAG_EXPANSION2 = 59;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////// SPECIAL LOCATIONS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var LOCATION_WORN = 253;
var LOCATION_CARRIED = 254;
var LOCATION_NONCREATED = 252;
var LOCATION_HERE = 255;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////  SYSTEM MESSAGES  ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



var SYSMESS_ISDARK = 0;
var SYSMESS_YOUCANSEE = 1;
var SYSMESS_PROMPT0 = 2;
var SYSMESS_PROMPT1 = 3;
var SYSMESS_PROMPT2 = 4
var SYSMESS_PROMPT3= 5;
var SYSMESS_IDONTUNDERSTAND = 6;
var SYSMESS_WRONGDIRECTION = 7
var SYSMESS_CANTDOTHAT = 8;
var SYSMESS_YOUARECARRYING = 9;
var SYSMESS_WORN = 10;
var SYSMESS_CARRYING_NOTHING = 11;
var SYSMESS_AREYOUSURE = 12;
var SYSMESS_PLAYAGAIN = 13;
var SYSMESS_FAREWELL = 14;
var SYSMESS_OK = 15;
var SYSMESS_PRESSANYKEY = 16;
var SYSMESS_TURNS_START = 17;
var SYSMESS_TURNS_CONTINUE = 18;
var SYSMESS_TURNS_PLURAL = 19;
var SYSMESS_TURNS_END = 20;
var SYSMESS_SCORE_START= 21;
var SYSMESS_SCORE_END =22;
var SYSMESS_YOURENOTWEARINGTHAT = 23;
var SYSMESS_YOUAREALREADYWEARINGTHAT = 24;
var SYSMESS_YOUALREADYHAVEOBJECT = 25;
var SYSMESS_CANTSEETHAT = 26;
var SYSMESS_CANTCARRYANYMORE = 27;
var SYSMESS_YOUDONTHAVETHAT = 28;
var SYSMESS_YOUAREALREADYWAERINGOBJECT = 29;
var SYSMESS_YES = 30;
var SYSMESS_NO = 31;
var SYSMESS_MORE = 32;
var SYSMESS_CARET = 33;
var SYSMESS_TIMEOUT=35;
var SYSMESS_YOUTAKEOBJECT = 36;
var SYSMESS_YOUWEAROBJECT = 37;
var SYSMESS_YOUREMOVEOBJECT = 38;
var SYSMESS_YOUDROPOBJECT = 39;
var SYSMESS_YOUCANTWEAROBJECT = 40;
var SYSMESS_YOUCANTREMOVEOBJECT = 41;
var SYSMESS_CANTREMOVE_TOOMANYOBJECTS = 42;
var SYSMESS_WEIGHSTOOMUCH = 43;
var SYSMESS_YOUPUTOBJECTIN = 44;
var SYSMESS_YOUCANTTAKEOBJECTOUTOF = 45;
var SYSMESS_LISTSEPARATOR = 46;
var SYSMESS_LISTLASTSEPARATOR = 47;
var SYSMESS_LISTEND = 48;
var SYSMESS_YOUDONTHAVEOBJECT = 49;
var SYSMESS_YOUARENOTWEARINGOBJECT = 50;
var SYSMESS_PUTINTAKEOUTTERMINATION = 51;
var SYSMESS_THATISNOTIN = 52;
var SYSMESS_EMPTYOBJECTLIST = 53;
var SYSMESS_FILENOTFOUND = 54;
var SYSMESS_CORRUPTFILE = 55;
var SYSMESS_IOFAILURE = 56;
var SYSMESS_DIRECTORYFULL = 57;
var SYSMESS_LOADFILE = 58;
var SYSMESS_FILENOTFOUND = 59;
var SYSMESS_SAVEFILE = 60;
var SYSMESS_SORRY = 61;
var SYSMESS_NONSENSE_SENTENCE = 62;
var SYSMESS_NPCLISTSTART = 63;
var SYSMESS_NPCLISTCONTINUE = 64;
var SYSMESS_NPCLISTCONTINUE_PLURAL = 65;
var SYSMESS_INSIDE_YOUCANSEE = 66;
var SYSMESS_OVER_YOUCANSEE = 67;
var SYSMESS_YOUPUTOBJECTON = 68;
var SYSMESS_YOUCANTTAKEOBJECTFROM = 69;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////// GLOBAL VARS //////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Parser vars
var last_player_orders = [];   // Store last player orders, to be able to restore it when pressing arrow up
var last_player_orders_pointer = 0;
var parser_word_found;
var player_order_buffer = '';
var player_order = ''; // Current player order
var previous_verb = EMPTY_WORD;
var previous_noun = EMPTY_WORD;
var previous_adject = EMPTY_WORD;
var pronoun_suffixes = [];


//Settings
var graphicsON = true; 
var soundsON = true; 
var interruptDisabled = false;
var showWarnings = true;

// waitkey commands callback function
var waitkey_callback_function = [];

//PAUSE
var inPause=false;
var pauseRemainingTime = 0;



// Transcript
var inTranscript = false;
var transcript = '';


// Block
var inBlock = false;
var unblock_process = null;


// END
var inEND = false;

//QUIT
var inQUIT = false;

//ANYKEY
var inAnykey = false;

//GETKEY
var inGetkey = false;
var getkey_return_flag = null;

// Status flags
var done_flag;
var describe_location_flag;
var in_response;
var success;

// doall control
var doall_flag;
var process_in_doall;
var entry_for_doall	= '';
var current_process;


var timeout_progress = 0;
var ramsave_value = null;
var num_objects;


// The flags
var flags = new Array();


// The sound channels
var soundChannels = [];
var soundLoopCount = [];

//The last free object attribute
var nextFreeAttr = 22;

//Autocomplete array
var autocomplete = new Array();
var autocompleteStep = 0;
var autocompleteBaseWord = '';
// PROCESSES

interruptProcessExists = false;

function pro000()
{
process_restart=true;
pro000_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p000e0000:
	{
 		if (skipdoall('p000e0000')) break p000e0000;
 		ACChook(0);
		if (done_flag) break pro000_restart;
		{}

	}

	// GO _
	p000e0001:
	{
 		if (skipdoall('p000e0001')) break p000e0001;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0001;
 		}
		if (!CNDlt(34,14)) break p000e0001;
 		ACCcopyff(34,33);
		{}

	}

	// _ _
	p000e0002:
	{
 		if (skipdoall('p000e0002')) break p000e0002;
		if (!CNDprep(7)) break p000e0002;
		if (!CNDeq(33,255)) break p000e0002;
		if (!CNDeq(34,255)) break p000e0002;
 		ACClet(33,11);
 		ACClet(43,255);
		{}

	}

	// _ _
	p000e0003:
	{
 		if (skipdoall('p000e0003')) break p000e0003;
		if (!CNDprep(10)) break p000e0003;
		if (!CNDeq(33,255)) break p000e0003;
		if (!CNDeq(34,255)) break p000e0003;
 		ACClet(33,12);
 		ACClet(43,255);
		{}

	}

	// _ INVE
	p000e0004:
	{
 		if (skipdoall('p000e0004')) break p000e0004;
 		if (in_response)
		{
			if (!CNDnoun1(104)) break p000e0004;
 		}
		if (!CNDeq(33,255)) break p000e0004;
 		ACClet(33,104);
 		ACClet(43,255);
		{}

	}

	// L _
	p000e0005:
	{
 		if (skipdoall('p000e0005')) break p000e0005;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0005;
 		}
		if (!CNDnoteq(34,255)) break p000e0005;
 		ACClet(33,110);
		{}

	}

	// L _
	p000e0006:
	{
 		if (skipdoall('p000e0006')) break p000e0006;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0006;
 		}
		if (!CNDbnotzero(12,1)) break p000e0006;
 		ACClet(33,110);
		{}

	}

	// EXITS _
	p000e0007:
	{
 		if (skipdoall('p000e0007')) break p000e0007;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0007;
 		}
		if (!CNDeq(34,255)) break p000e0007;
		if (!CNDbnotzero(12,1)) break p000e0007;
 		ACClet(33,110);
		{}

	}

	// EXITS _
	p000e0008:
	{
 		if (skipdoall('p000e0008')) break p000e0008;
 		if (in_response)
		{
			if (!CNDverb(111)) break p000e0008;
 		}
		if (!CNDnoteq(34,255)) break p000e0008;
 		ACClet(33,110);
		{}

	}

	// GET _
	p000e0009:
	{
 		if (skipdoall('p000e0009')) break p000e0009;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0009;
 		}
		if (!CNDprep(4)) break p000e0009;
 		ACClet(33,112);
		{}

	}

	// PUT _
	p000e0010:
	{
 		if (skipdoall('p000e0010')) break p000e0010;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0010;
 		}
		if (!CNDprep(12)) break p000e0010;
		if (!CNDnoteq(51,255)) break p000e0010;
		if (!CNDpresent(getFlag(51))) break p000e0010;
		if (!CNDeq(44,255)) break p000e0010;
		if (!CNDonotzero(getFlag(51),1)) break p000e0010;
 		ACClet(33,113);
		{}

	}

	// PUT _
	p000e0011:
	{
 		if (skipdoall('p000e0011')) break p000e0011;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0011;
 		}
		if (!CNDprep(12)) break p000e0011;
		if (!CNDnoteq(51,255)) break p000e0011;
		if (!CNDpresent(getFlag(51))) break p000e0011;
		if (!CNDnoteq(44,255)) break p000e0011;
 		ACClet(33,39);
		{}

	}

	// PUT _
	p000e0012:
	{
 		if (skipdoall('p000e0012')) break p000e0012;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0012;
 		}
		if (!CNDprep(11)) break p000e0012;
		if (!CNDnoteq(51,255)) break p000e0012;
		if (!CNDpresent(getFlag(51))) break p000e0012;
		if (!CNDnoteq(44,255)) break p000e0012;
 		ACClet(33,39);
		{}

	}

	// PUT _
	p000e0013:
	{
 		if (skipdoall('p000e0013')) break p000e0013;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0013;
 		}
		if (!CNDprep(7)) break p000e0013;
		if (!CNDnoteq(51,255)) break p000e0013;
		if (!CNDpresent(getFlag(51))) break p000e0013;
		if (!CNDnoteq(44,255)) break p000e0013;
 		ACClet(33,39);
		{}

	}

	// PUT _
	p000e0014:
	{
 		if (skipdoall('p000e0014')) break p000e0014;
 		if (in_response)
		{
			if (!CNDverb(113)) break p000e0014;
 		}
		if (!CNDprep(6)) break p000e0014;
		if (!CNDnoteq(51,255)) break p000e0014;
		if (!CNDpresent(getFlag(51))) break p000e0014;
		if (!CNDnoteq(44,255)) break p000e0014;
 		ACClet(33,39);
		{}

	}

	// DROP _
	p000e0015:
	{
 		if (skipdoall('p000e0015')) break p000e0015;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0015;
 		}
		if (!CNDprep(6)) break p000e0015;
 		ACClet(33,39);
		{}

	}

	// GET _
	p000e0016:
	{
 		if (skipdoall('p000e0016')) break p000e0016;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0016;
 		}
		if (!CNDprep(3)) break p000e0016;
 		ACClet(33,114);
		{}

	}

	// GET _
	p000e0017:
	{
 		if (skipdoall('p000e0017')) break p000e0017;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0017;
 		}
		if (!CNDprep(10)) break p000e0017;
 		ACClet(33,114);
		{}

	}

	// SPIT _
	p000e0018:
	{
 		if (skipdoall('p000e0018')) break p000e0018;
 		if (in_response)
		{
			if (!CNDverb(115)) break p000e0018;
 		}
		if (!CNDprep(13)) break p000e0018;
		if (!CNDnoteq(34,255)) break p000e0018;
		if (!CNDnoteq(51,255)) break p000e0018;
		if (!CNDonotzero(getFlag(51),3)) break p000e0018;
 		ACClet(33,90);
		{}

	}

	// THROW _
	p000e0019:
	{
 		if (skipdoall('p000e0019')) break p000e0019;
 		if (in_response)
		{
			if (!CNDverb(116)) break p000e0019;
 		}
		if (!CNDprep(2)) break p000e0019;
		if (!CNDnoteq(34,255)) break p000e0019;
		if (!CNDnoteq(51,255)) break p000e0019;
		if (!CNDcarried(getFlag(51))) break p000e0019;
		if (!CNDnoteq(44,255)) break p000e0019;
 		ACCwhatox2(13);
		if (!CNDnoteq(13,255)) break p000e0019;
		if (!CNDpresent(getFlag(13))) break p000e0019;
		if (!CNDonotzero(getFlag(13),3)) break p000e0019;
 		ACClet(33,90);
 		ACCcopyff(44,34);
 		ACCcopyff(45,35);
 		ACCwhato();
		{}

	}

	// THROW _
	p000e0020:
	{
 		if (skipdoall('p000e0020')) break p000e0020;
 		if (in_response)
		{
			if (!CNDverb(116)) break p000e0020;
 		}
		if (!CNDnoteq(34,255)) break p000e0020;
		if (!CNDnoteq(51,255)) break p000e0020;
		if (!CNDcarried(getFlag(51))) break p000e0020;
 		ACClet(33,101);
		{}

	}

	// TURN _
	p000e0021:
	{
 		if (skipdoall('p000e0021')) break p000e0021;
 		if (in_response)
		{
			if (!CNDverb(117)) break p000e0021;
 		}
		if (!CNDprep(12)) break p000e0021;
 		ACClet(33,118);
		{}

	}

	// SWITCH _
	p000e0022:
	{
 		if (skipdoall('p000e0022')) break p000e0022;
 		if (in_response)
		{
			if (!CNDverb(119)) break p000e0022;
 		}
		if (!CNDprep(12)) break p000e0022;
 		ACClet(33,118);
		{}

	}

	// TURN _
	p000e0023:
	{
 		if (skipdoall('p000e0023')) break p000e0023;
 		if (in_response)
		{
			if (!CNDverb(117)) break p000e0023;
 		}
		if (!CNDprep(4)) break p000e0023;
 		ACClet(33,37);
		{}

	}

	// SWITCH _
	p000e0024:
	{
 		if (skipdoall('p000e0024')) break p000e0024;
 		if (in_response)
		{
			if (!CNDverb(119)) break p000e0024;
 		}
		if (!CNDprep(4)) break p000e0024;
 		ACClet(33,37);
		{}

	}

	// CHANGECSS _
	p000e0025:
	{
 		if (skipdoall('p000e0025')) break p000e0025;
 		if (in_response)
		{
			if (!CNDverb(122)) break p000e0025;
 		}
 		ACCchangecss();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0026:
	{
 		if (skipdoall('p000e0026')) break p000e0026;
 		ACChook(1);
		if (done_flag) break pro000_restart;
		{}

	}

	//  _
	p000e0027:
	{
 		if (skipdoall('p000e0027')) break p000e0027;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0027;
 		}
		if (!CNDat(15)) break p000e0027;
		if (!CNDcarried(17)) break p000e0027;
 		ACCgoto(16);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0028:
	{
 		if (skipdoall('p000e0028')) break p000e0028;
 		if (in_response)
		{
			if (!CNDverb(2)) break p000e0028;
 		}
		if (!CNDat(15)) break p000e0028;
		if (!CNDnotcarr(17)) break p000e0028;
 		ACCgoto(15);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  _
	p000e0029:
	{
 		if (skipdoall('p000e0029')) break p000e0029;
 		if (in_response)
		{
			if (!CNDverb(3)) break p000e0029;
 		}
		if (!CNDat(19)) break p000e0029;
		if (!CNDnotzero(114)) break p000e0029;
 		ACCgoto(20);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	//  TREE
	p000e0030:
	{
 		if (skipdoall('p000e0030')) break p000e0030;
 		if (in_response)
		{
			if (!CNDverb(9)) break p000e0030;
			if (!CNDnoun1(80)) break p000e0030;
 		}
		if (!CNDatgt(33)) break p000e0030;
		if (!CNDatlt(37)) break p000e0030;
 		ACCwriteln(2);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TELE _
	p000e0031:
	{
 		if (skipdoall('p000e0031')) break p000e0031;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0031;
 		}
		if (CNDat(2))
  		{
 		ACClet(38,13);
 		ACCdesc();
		break pro000_restart;
 		}
		if (CNDat(13))
  		{
 		ACClet(38,2);
 		ACCdesc();
		break pro000_restart;
 		}
		if (CNDat(98))
  		{
 		ACClet(38,25);
 		ACCdesc();
		break pro000_restart;
 		}
		{}

	}

	// TELE _
	p000e0032:
	{
 		if (skipdoall('p000e0032')) break p000e0032;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0032;
 		}
		if (!CNDatgt(33)) break p000e0032;
		if (!CNDatlt(81)) break p000e0032;
		if (!CNDnotat(55)) break p000e0032;
		if (!CNDeq(20,0)) break p000e0032;
 		ACCwriteln(3);
 		ACCpause(100);
 		function anykey00001() 
		{
 		ACCgoto(25);
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		function anykey00000() 
		{
 		ACCanykey();
 		waitKey(anykey00001);
		}
 		waitKey(anykey00000);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TELE _
	p000e0033:
	{
 		if (skipdoall('p000e0033')) break p000e0033;
 		if (in_response)
		{
			if (!CNDverb(14)) break p000e0033;
 		}
		if (!CNDat(55)) break p000e0033;
 		ACCwriteln(4);
 		ACCend();
		break pro000_restart;
		{}

	}

	// _ CRUI
	p000e0034:
	{
 		if (skipdoall('p000e0034')) break p000e0034;
 		if (in_response)
		{
			if (!CNDnoun1(21)) break p000e0034;
 		}
		if (!CNDprep(6)) break p000e0034;
 		ACClet(43,7);
		{}

	}

	// GO CRUI
	p000e0035:
	{
 		if (skipdoall('p000e0035')) break p000e0035;
 		if (in_response)
		{
			if (!CNDverb(109)) break p000e0035;
			if (!CNDnoun1(21)) break p000e0035;
 		}
		if (!CNDprep(7)) break p000e0035;
 		ACClet(33,11);
 		ACClet(43,255);
		{}

	}

	// GET CRUI
	p000e0036:
	{
 		if (skipdoall('p000e0036')) break p000e0036;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0036;
			if (!CNDnoun1(21)) break p000e0036;
 		}
		if (!CNDprep(7)) break p000e0036;
 		ACClet(33,11);
 		ACClet(43,255);
		{}

	}

	//  CRUI
	p000e0037:
	{
 		if (skipdoall('p000e0037')) break p000e0037;
 		if (in_response)
		{
			if (!CNDverb(11)) break p000e0037;
			if (!CNDnoun1(21)) break p000e0037;
 		}
		if (!CNDat(3)) break p000e0037;
 		ACCgoto(23);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// PLAY _
	p000e0038:
	{
 		if (skipdoall('p000e0038')) break p000e0038;
 		if (in_response)
		{
			if (!CNDverb(15)) break p000e0038;
 		}
		if (!CNDat(27)) break p000e0038;
 		ACCwriteln(5);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SAY _
	p000e0039:
	{
 		if (skipdoall('p000e0039')) break p000e0039;
 		if (in_response)
		{
			if (!CNDverb(16)) break p000e0039;
 		}
		if (!CNDnotzero(18)) break p000e0039;
		if (!CNDpresent(16)) break p000e0039;
 		ACCwriteln(6);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// SIT _
	p000e0040:
	{
 		if (skipdoall('p000e0040')) break p000e0040;
 		if (in_response)
		{
			if (!CNDverb(18)) break p000e0040;
 		}
 		ACCok();
		break pro000_restart;
		{}

	}

	// STAN _
	p000e0041:
	{
 		if (skipdoall('p000e0041')) break p000e0041;
 		if (in_response)
		{
			if (!CNDverb(19)) break p000e0041;
 		}
 		ACCok();
		break pro000_restart;
		{}

	}

	// HELP _
	p000e0042:
	{
 		if (skipdoall('p000e0042')) break p000e0042;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0042;
 		}
		if (!CNDat(23)) break p000e0042;
 		ACCwriteln(7);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HELP _
	p000e0043:
	{
 		if (skipdoall('p000e0043')) break p000e0043;
 		if (in_response)
		{
			if (!CNDverb(20)) break p000e0043;
 		}
 		ACCwriteln(8);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE LIST
	p000e0044:
	{
 		if (skipdoall('p000e0044')) break p000e0044;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0044;
			if (!CNDnoun1(23)) break p000e0044;
 		}
		if (!CNDat(23)) break p000e0044;
 		ACCwriteln(9);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE ZAGR
	p000e0045:
	{
 		if (skipdoall('p000e0045')) break p000e0045;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0045;
			if (!CNDnoun1(25)) break p000e0045;
 		}
		if (!CNDat(23)) break p000e0045;
 		ACCwriteln(10);
 		ACCpause(100);
 		function anykey00003() 
		{
 		ACCgoto(3);
 		ACCdesc();
		return;
		}
 		function anykey00002() 
		{
 		ACCanykey();
 		waitKey(anykey00003);
		}
 		waitKey(anykey00002);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TYPE GRAF
	p000e0046:
	{
 		if (skipdoall('p000e0046')) break p000e0046;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0046;
			if (!CNDnoun1(26)) break p000e0046;
 		}
		if (!CNDat(23)) break p000e0046;
 		ACCwriteln(11);
 		ACCpause(100);
 		function anykey00005() 
		{
 		ACCgoto(35);
 		ACCdesc();
		return;
		}
 		function anykey00004() 
		{
 		ACCanykey();
 		waitKey(anykey00005);
		}
 		waitKey(anykey00004);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TYPE TERM
	p000e0047:
	{
 		if (skipdoall('p000e0047')) break p000e0047;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0047;
			if (!CNDnoun1(27)) break p000e0047;
 		}
		if (!CNDat(23)) break p000e0047;
 		ACCwriteln(12);
 		ACCpause(100);
 		function anykey00007() 
		{
 		ACCgoto(49);
 		ACCdesc();
		return;
		}
 		function anykey00006() 
		{
 		ACCanykey();
 		waitKey(anykey00007);
		}
 		waitKey(anykey00006);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TYPE SEPT
	p000e0048:
	{
 		if (skipdoall('p000e0048')) break p000e0048;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0048;
			if (!CNDnoun1(28)) break p000e0048;
 		}
		if (!CNDat(23)) break p000e0048;
 		ACCwriteln(13);
 		ACCpause(100);
 		function anykey00009() 
		{
 		ACCgoto(61);
 		ACCdesc();
		return;
		}
 		function anykey00008() 
		{
 		ACCanykey();
 		waitKey(anykey00009);
		}
 		waitKey(anykey00008);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TYPE STAT
	p000e0049:
	{
 		if (skipdoall('p000e0049')) break p000e0049;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0049;
			if (!CNDnoun1(29)) break p000e0049;
 		}
		if (!CNDat(23)) break p000e0049;
 		ACCwriteln(14);
 		ACCpause(100);
 		function anykey00011() 
		{
 		ACCgoto(82);
 		ACCdesc();
		return;
		}
 		function anykey00010() 
		{
 		ACCanykey();
 		waitKey(anykey00011);
		}
 		waitKey(anykey00010);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TYPE AKRO
	p000e0050:
	{
 		if (skipdoall('p000e0050')) break p000e0050;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0050;
			if (!CNDnoun1(30)) break p000e0050;
 		}
		if (!CNDat(23)) break p000e0050;
 		ACCwriteln(15);
 		ACCpause(100);
 		function anykey00013() 
		{
 		ACCgoto(98);
 		ACCdesc();
		return;
 		ACCdone();
		return;
		}
 		function anykey00012() 
		{
 		ACCanykey();
 		waitKey(anykey00013);
		}
 		waitKey(anykey00012);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// TYPE DEAC
	p000e0051:
	{
 		if (skipdoall('p000e0051')) break p000e0051;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0051;
			if (!CNDnoun1(37)) break p000e0051;
 		}
		if (!CNDat(19)) break p000e0051;
		if (!CNDeq(115,0)) break p000e0051;
 		ACCset(115);
 		ACCwriteln(16);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// TYPE _
	p000e0052:
	{
 		if (skipdoall('p000e0052')) break p000e0052;
 		if (in_response)
		{
			if (!CNDverb(22)) break p000e0052;
 		}
		if (!CNDat(19)) break p000e0052;
		if (!CNDeq(115,0)) break p000e0052;
 		ACCwriteln(17);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// WAIT _
	p000e0053:
	{
 		if (skipdoall('p000e0053')) break p000e0053;
 		if (in_response)
		{
			if (!CNDverb(24)) break p000e0053;
 		}
 		ACCwriteln(18);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX CARD
	p000e0054:
	{
 		if (skipdoall('p000e0054')) break p000e0054;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0054;
			if (!CNDnoun1(38)) break p000e0054;
 		}
		if (!CNDpresent(23)) break p000e0054;
 		ACCwriteln(19);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX CRYS
	p000e0055:
	{
 		if (skipdoall('p000e0055')) break p000e0055;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0055;
			if (!CNDnoun1(57)) break p000e0055;
 		}
		if (!CNDpresent(12)) break p000e0055;
 		ACCwriteln(20);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX COVA
	p000e0056:
	{
 		if (skipdoall('p000e0056')) break p000e0056;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0056;
			if (!CNDnoun1(58)) break p000e0056;
 		}
		if (!CNDpresent(14)) break p000e0056;
 		ACCwriteln(21);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX LANC
	p000e0057:
	{
 		if (skipdoall('p000e0057')) break p000e0057;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0057;
			if (!CNDnoun1(59)) break p000e0057;
 		}
		if (!CNDpresent(15)) break p000e0057;
 		ACCwriteln(22);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX MAP
	p000e0058:
	{
 		if (skipdoall('p000e0058')) break p000e0058;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0058;
			if (!CNDnoun1(60)) break p000e0058;
 		}
		if (!CNDcarried(17)) break p000e0058;
 		ACCwriteln(23);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX RIFL
	p000e0059:
	{
 		if (skipdoall('p000e0059')) break p000e0059;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0059;
			if (!CNDnoun1(61)) break p000e0059;
 		}
		if (!CNDpresent(18)) break p000e0059;
 		ACCwriteln(24);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX TRAI
	p000e0060:
	{
 		if (skipdoall('p000e0060')) break p000e0060;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0060;
			if (!CNDnoun1(47)) break p000e0060;
 		}
		if (!CNDat(63)) break p000e0060;
 		ACCwriteln(25);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX TRAI
	p000e0061:
	{
 		if (skipdoall('p000e0061')) break p000e0061;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0061;
			if (!CNDnoun1(47)) break p000e0061;
 		}
		if (!CNDat(78)) break p000e0061;
 		ACCwriteln(26);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX TRAI
	p000e0062:
	{
 		if (skipdoall('p000e0062')) break p000e0062;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0062;
			if (!CNDnoun1(47)) break p000e0062;
 		}
 		ACCwriteln(27);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0063:
	{
 		if (skipdoall('p000e0063')) break p000e0063;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0063;
 		}
		if (!CNDat(97)) break p000e0063;
 		ACCwriteln(28);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0064:
	{
 		if (skipdoall('p000e0064')) break p000e0064;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0064;
 		}
		if (!CNDat(31)) break p000e0064;
 		ACCwriteln(29);
 		ACClet(61,69);
 		ACCanykey();
 		function anykey00014() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00014);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// EX _
	p000e0065:
	{
 		if (skipdoall('p000e0065')) break p000e0065;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0065;
 		}
		if (!CNDat(39)) break p000e0065;
 		ACCwriteln(30);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0066:
	{
 		if (skipdoall('p000e0066')) break p000e0066;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0066;
 		}
		if (!CNDnoteq(51,255)) break p000e0066;
		if (!CNDabsent(getFlag(51))) break p000e0066;
 		ACCwriteln(31);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0067:
	{
 		if (skipdoall('p000e0067')) break p000e0067;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0067;
 		}
		if (!CNDbnotzero(12,1)) break p000e0067;
 		ACCwriteln(32);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0068:
	{
 		if (skipdoall('p000e0068')) break p000e0068;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0068;
 		}
		if (!CNDnoteq(34,255)) break p000e0068;
		if (!CNDeq(51,255)) break p000e0068;
 		ACCwriteln(33);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// EX _
	p000e0069:
	{
 		if (skipdoall('p000e0069')) break p000e0069;
 		if (in_response)
		{
			if (!CNDverb(32)) break p000e0069;
 		}
 		ACCwriteln(34);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// HEAT ICE
	p000e0070:
	{
 		if (skipdoall('p000e0070')) break p000e0070;
 		if (in_response)
		{
			if (!CNDverb(33)) break p000e0070;
			if (!CNDnoun1(35)) break p000e0070;
 		}
		if (!CNDat(97)) break p000e0070;
		if (!CNDeq(116,0)) break p000e0070;
		if (!CNDcarried(15)) break p000e0070;
 		ACCwriteln(35);
 		ACCset(116);
 		ACCcreate(18);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// USE LANC
	p000e0071:
	{
 		if (skipdoall('p000e0071')) break p000e0071;
 		if (in_response)
		{
			if (!CNDverb(34)) break p000e0071;
			if (!CNDnoun1(59)) break p000e0071;
 		}
		if (!CNDat(97)) break p000e0071;
		if (!CNDeq(116,0)) break p000e0071;
		if (!CNDcarried(15)) break p000e0071;
 		ACCwriteln(36);
 		ACCcreate(18);
 		ACCset(116);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// INSE CARD
	p000e0072:
	{
 		if (skipdoall('p000e0072')) break p000e0072;
 		if (in_response)
		{
			if (!CNDverb(39)) break p000e0072;
			if (!CNDnoun1(38)) break p000e0072;
 		}
		if (!CNDat(16)) break p000e0072;
		if (!CNDcarried(23)) break p000e0072;
 		ACCwriteln(37);
 		ACCdestroy(23);
 		ACCpause(100);
 		function anykey00016() 
		{
 		ACCgoto(19);
 		ACCdesc();
		return;
		}
 		function anykey00015() 
		{
 		ACCanykey();
 		waitKey(anykey00016);
		}
 		waitKey(anykey00015);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0073:
	{
 		if (skipdoall('p000e0073')) break p000e0073;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0073;
 		}
		if (!CNDpresent(1)) break p000e0073;
 		ACCdestroy(1);
 		ACCwriteln(38);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0074:
	{
 		if (skipdoall('p000e0074')) break p000e0074;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0074;
 		}
		if (!CNDpresent(2)) break p000e0074;
 		ACCdestroy(2);
 		ACCwriteln(39);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0075:
	{
 		if (skipdoall('p000e0075')) break p000e0075;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0075;
 		}
		if (!CNDpresent(3)) break p000e0075;
 		ACCdestroy(3);
 		ACCwriteln(40);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0076:
	{
 		if (skipdoall('p000e0076')) break p000e0076;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0076;
 		}
		if (!CNDpresent(4)) break p000e0076;
 		ACCdestroy(4);
 		ACCwriteln(41);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0077:
	{
 		if (skipdoall('p000e0077')) break p000e0077;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0077;
 		}
		if (!CNDpresent(5)) break p000e0077;
 		ACCdestroy(5);
 		ACCwriteln(42);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0078:
	{
 		if (skipdoall('p000e0078')) break p000e0078;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0078;
 		}
		if (!CNDpresent(6)) break p000e0078;
 		ACCdestroy(6);
 		ACCwriteln(43);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0079:
	{
 		if (skipdoall('p000e0079')) break p000e0079;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0079;
 		}
		if (!CNDpresent(7)) break p000e0079;
 		ACCdestroy(7);
 		ACCwriteln(44);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// CONS _
	p000e0080:
	{
 		if (skipdoall('p000e0080')) break p000e0080;
 		if (in_response)
		{
			if (!CNDverb(40)) break p000e0080;
 		}
		if (!CNDpresent(8)) break p000e0080;
 		ACCdestroy(8);
 		ACCwriteln(45);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// JOSH 5
	p000e0081:
	{
 		if (skipdoall('p000e0081')) break p000e0081;
 		if (in_response)
		{
			if (!CNDverb(41)) break p000e0081;
			if (!CNDnoun1(42)) break p000e0081;
 		}
 		ACCcls();
 		ACCwriteln(46);
 		ACCanykey();
 		function anykey00017() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00017);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// ABOUT _
	p000e0082:
	{
 		if (skipdoall('p000e0082')) break p000e0082;
 		if (in_response)
		{
			if (!CNDverb(123)) break p000e0082;
 		}
 		ACCcls();
 		ACCwriteln(47);
 		ACCwriteln(48);
 		ACCwriteln(49);
 		ACCwriteln(50);
 		ACCanykey();
 		function anykey00018() 
		{
 		ACCdesc();
		return;
		}
 		waitKey(anykey00018);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SLEE _
	p000e0083:
	{
 		if (skipdoall('p000e0083')) break p000e0083;
 		if (in_response)
		{
			if (!CNDverb(45)) break p000e0083;
 		}
 		ACCwriteln(51);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// PRES BUTT
	p000e0084:
	{
 		if (skipdoall('p000e0084')) break p000e0084;
 		if (in_response)
		{
			if (!CNDverb(50)) break p000e0084;
			if (!CNDnoun1(51)) break p000e0084;
 		}
		if (!CNDat(51)) break p000e0084;
		if (!CNDeq(20,0)) break p000e0084;
 		ACCset(20);
 		ACCwriteln(52);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ BEVERAGE
	p000e0085:
	{
 		if (skipdoall('p000e0085')) break p000e0085;
 		if (in_response)
		{
			if (!CNDnoun1(64)) break p000e0085;
 		}
		if (!CNDeq(33,255)) break p000e0085;
		if (!CNDnoun2(64)) break p000e0085;
		if (CNDcarried(20))
  		{
 		ACCwriteln(53);
 		ACCdone();
		break pro000_restart;
 		}
		if (CNDcarried(21))
  		{
 		ACCwriteln(54);
 		ACCdone();
		break pro000_restart;
 		}
		if (CNDcarried(22))
  		{
 		ACCwriteln(55);
 		ACCdone();
		break pro000_restart;
 		}
 		ACCwriteln(56);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIVE _
	p000e0086:
	{
 		if (skipdoall('p000e0086')) break p000e0086;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0086;
 		}
		if (!CNDat(41)) break p000e0086;
 		ACCwriteln(57);
 		ACCend();
		break pro000_restart;
		{}

	}

	// DIVE _
	p000e0087:
	{
 		if (skipdoall('p000e0087')) break p000e0087;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0087;
 		}
		if (!CNDat(37)) break p000e0087;
 		ACCwriteln(58);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIVE _
	p000e0088:
	{
 		if (skipdoall('p000e0088')) break p000e0088;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0088;
 		}
		if (!CNDat(39)) break p000e0088;
 		ACCwriteln(59);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DIVE _
	p000e0089:
	{
 		if (skipdoall('p000e0089')) break p000e0089;
 		if (in_response)
		{
			if (!CNDverb(65)) break p000e0089;
 		}
		if (!CNDat(17)) break p000e0089;
 		ACCwriteln(60);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0090:
	{
 		if (skipdoall('p000e0090')) break p000e0090;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0090;
			if (!CNDnoun1(71)) break p000e0090;
 		}
		if (!CNDat(33)) break p000e0090;
 		ACCwriteln(61);
 		ACCend();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0091:
	{
 		if (skipdoall('p000e0091')) break p000e0091;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0091;
			if (!CNDnoun1(71)) break p000e0091;
 		}
		if (!CNDat(26)) break p000e0091;
 		ACCwriteln(62);
 		ACCend();
		break pro000_restart;
		{}

	}

	// OPEN DOOR
	p000e0092:
	{
 		if (skipdoall('p000e0092')) break p000e0092;
 		if (in_response)
		{
			if (!CNDverb(70)) break p000e0092;
			if (!CNDnoun1(71)) break p000e0092;
 		}
		if (!CNDat(19)) break p000e0092;
		if (!CNDcarried(0)) break p000e0092;
		if (!CNDeq(114,0)) break p000e0092;
 		ACCwriteln(63);
 		ACCset(114);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ATTA PLAN
	p000e0093:
	{
 		if (skipdoall('p000e0093')) break p000e0093;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0093;
			if (!CNDnoun1(54)) break p000e0093;
 		}
		if (!CNDpresent(13)) break p000e0093;
 		ACCwriteln(64);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// ATTA JEKR
	p000e0094:
	{
 		if (skipdoall('p000e0094')) break p000e0094;
 		if (in_response)
		{
			if (!CNDverb(90)) break p000e0094;
			if (!CNDnoun1(56)) break p000e0094;
 		}
		if (!CNDpresent(16)) break p000e0094;
 		ACCwriteln(65);
 		ACCswap(16,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLAS PLAN
	p000e0095:
	{
 		if (skipdoall('p000e0095')) break p000e0095;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0095;
			if (!CNDnoun1(54)) break p000e0095;
 		}
		if (!CNDcarried(18)) break p000e0095;
 		ACCwriteln(66);
 		ACCswap(13,9);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// BLAS JEKR
	p000e0096:
	{
 		if (skipdoall('p000e0096')) break p000e0096;
 		if (in_response)
		{
			if (!CNDverb(91)) break p000e0096;
			if (!CNDnoun1(56)) break p000e0096;
 		}
		if (!CNDpresent(16)) break p000e0096;
 		ACCwriteln(67);
 		ACCswap(16,10);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GET CARD
	p000e0097:
	{
 		if (skipdoall('p000e0097')) break p000e0097;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0097;
			if (!CNDnoun1(38)) break p000e0097;
 		}
		if (!CNDpresent(23)) break p000e0097;
 		ACCget(23);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET PROB
	p000e0098:
	{
 		if (skipdoall('p000e0098')) break p000e0098;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0098;
			if (!CNDnoun1(52)) break p000e0098;
 		}
		if (!CNDpresent(0)) break p000e0098;
 		ACCget(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0099:
	{
 		if (skipdoall('p000e0099')) break p000e0099;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0099;
			if (!CNDnoun1(53)) break p000e0099;
 		}
		if (!CNDpresent(1)) break p000e0099;
 		ACCget(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0100:
	{
 		if (skipdoall('p000e0100')) break p000e0100;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0100;
			if (!CNDnoun1(53)) break p000e0100;
 		}
		if (!CNDpresent(2)) break p000e0100;
 		ACCget(2);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0101:
	{
 		if (skipdoall('p000e0101')) break p000e0101;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0101;
			if (!CNDnoun1(53)) break p000e0101;
 		}
		if (!CNDpresent(3)) break p000e0101;
 		ACCget(3);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0102:
	{
 		if (skipdoall('p000e0102')) break p000e0102;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0102;
			if (!CNDnoun1(53)) break p000e0102;
 		}
		if (!CNDpresent(4)) break p000e0102;
 		ACCget(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0103:
	{
 		if (skipdoall('p000e0103')) break p000e0103;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0103;
			if (!CNDnoun1(53)) break p000e0103;
 		}
		if (!CNDpresent(5)) break p000e0103;
 		ACCget(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0104:
	{
 		if (skipdoall('p000e0104')) break p000e0104;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0104;
			if (!CNDnoun1(53)) break p000e0104;
 		}
		if (!CNDpresent(6)) break p000e0104;
 		ACCget(6);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0105:
	{
 		if (skipdoall('p000e0105')) break p000e0105;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0105;
			if (!CNDnoun1(53)) break p000e0105;
 		}
		if (!CNDpresent(7)) break p000e0105;
 		ACCget(7);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET FOOD
	p000e0106:
	{
 		if (skipdoall('p000e0106')) break p000e0106;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0106;
			if (!CNDnoun1(53)) break p000e0106;
 		}
		if (!CNDpresent(8)) break p000e0106;
 		ACCget(8);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET CRYS
	p000e0107:
	{
 		if (skipdoall('p000e0107')) break p000e0107;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0107;
			if (!CNDnoun1(57)) break p000e0107;
 		}
		if (!CNDpresent(12)) break p000e0107;
 		ACCget(12);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET COVA
	p000e0108:
	{
 		if (skipdoall('p000e0108')) break p000e0108;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0108;
			if (!CNDnoun1(58)) break p000e0108;
 		}
		if (!CNDpresent(14)) break p000e0108;
 		ACCget(14);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET LANC
	p000e0109:
	{
 		if (skipdoall('p000e0109')) break p000e0109;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0109;
			if (!CNDnoun1(59)) break p000e0109;
 		}
		if (!CNDpresent(15)) break p000e0109;
 		ACCget(15);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET MAP
	p000e0110:
	{
 		if (skipdoall('p000e0110')) break p000e0110;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0110;
			if (!CNDnoun1(60)) break p000e0110;
 		}
		if (!CNDpresent(17)) break p000e0110;
 		ACCget(17);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET RIFL
	p000e0111:
	{
 		if (skipdoall('p000e0111')) break p000e0111;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0111;
			if (!CNDnoun1(61)) break p000e0111;
 		}
		if (!CNDpresent(18)) break p000e0111;
 		ACCget(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BAR
	p000e0112:
	{
 		if (skipdoall('p000e0112')) break p000e0112;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0112;
			if (!CNDnoun1(62)) break p000e0112;
 		}
		if (!CNDpresent(19)) break p000e0112;
 		ACCget(19);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BEVERAGE
	p000e0113:
	{
 		if (skipdoall('p000e0113')) break p000e0113;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0113;
			if (!CNDnoun1(64)) break p000e0113;
 		}
		if (!CNDpresent(20)) break p000e0113;
 		ACCget(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BEVERAGE
	p000e0114:
	{
 		if (skipdoall('p000e0114')) break p000e0114;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0114;
			if (!CNDnoun1(64)) break p000e0114;
 		}
		if (!CNDpresent(21)) break p000e0114;
 		ACCget(21);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// GET BEVERAGE
	p000e0115:
	{
 		if (skipdoall('p000e0115')) break p000e0115;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0115;
			if (!CNDnoun1(64)) break p000e0115;
 		}
		if (!CNDpresent(22)) break p000e0115;
 		ACCget(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// _ BEVERAGE
	p000e0116:
	{
 		if (skipdoall('p000e0116')) break p000e0116;
 		if (in_response)
		{
			if (!CNDnoun1(64)) break p000e0116;
 		}
		if (!CNDeq(33,255)) break p000e0116;
		if (!CNDeq(44,255)) break p000e0116;
 		ACCwriteln(68);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// GET INVE
	p000e0117:
	{
 		if (skipdoall('p000e0117')) break p000e0117;
 		if (in_response)
		{
			if (!CNDverb(100)) break p000e0117;
			if (!CNDnoun1(104)) break p000e0117;
 		}
 		ACClet(33,104);
		{}

	}

	// DROP CARD
	p000e0118:
	{
 		if (skipdoall('p000e0118')) break p000e0118;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0118;
			if (!CNDnoun1(38)) break p000e0118;
 		}
		if (!CNDpresent(23)) break p000e0118;
 		ACCdrop(23);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP PROB
	p000e0119:
	{
 		if (skipdoall('p000e0119')) break p000e0119;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0119;
			if (!CNDnoun1(52)) break p000e0119;
 		}
		if (!CNDcarried(0)) break p000e0119;
 		ACCdrop(0);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0120:
	{
 		if (skipdoall('p000e0120')) break p000e0120;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0120;
			if (!CNDnoun1(53)) break p000e0120;
 		}
		if (!CNDcarried(1)) break p000e0120;
 		ACCdrop(1);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0121:
	{
 		if (skipdoall('p000e0121')) break p000e0121;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0121;
			if (!CNDnoun1(53)) break p000e0121;
 		}
		if (!CNDcarried(2)) break p000e0121;
 		ACCdrop(2);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0122:
	{
 		if (skipdoall('p000e0122')) break p000e0122;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0122;
			if (!CNDnoun1(53)) break p000e0122;
 		}
		if (!CNDcarried(3)) break p000e0122;
 		ACCdrop(3);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0123:
	{
 		if (skipdoall('p000e0123')) break p000e0123;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0123;
			if (!CNDnoun1(53)) break p000e0123;
 		}
		if (!CNDcarried(4)) break p000e0123;
 		ACCdrop(4);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0124:
	{
 		if (skipdoall('p000e0124')) break p000e0124;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0124;
			if (!CNDnoun1(53)) break p000e0124;
 		}
		if (!CNDcarried(5)) break p000e0124;
 		ACCdrop(5);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0125:
	{
 		if (skipdoall('p000e0125')) break p000e0125;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0125;
			if (!CNDnoun1(53)) break p000e0125;
 		}
		if (!CNDcarried(6)) break p000e0125;
 		ACCdrop(6);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0126:
	{
 		if (skipdoall('p000e0126')) break p000e0126;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0126;
			if (!CNDnoun1(53)) break p000e0126;
 		}
		if (!CNDcarried(7)) break p000e0126;
 		ACCdrop(7);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP FOOD
	p000e0127:
	{
 		if (skipdoall('p000e0127')) break p000e0127;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0127;
			if (!CNDnoun1(53)) break p000e0127;
 		}
		if (!CNDcarried(8)) break p000e0127;
 		ACCdrop(8);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP COVA
	p000e0128:
	{
 		if (skipdoall('p000e0128')) break p000e0128;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0128;
			if (!CNDnoun1(58)) break p000e0128;
 		}
		if (!CNDabsent(16)) break p000e0128;
		if (!CNDcarried(14)) break p000e0128;
 		ACCdrop(14);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP COVA
	p000e0129:
	{
 		if (skipdoall('p000e0129')) break p000e0129;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0129;
			if (!CNDnoun1(58)) break p000e0129;
 		}
		if (!CNDpresent(16)) break p000e0129;
		if (!CNDcarried(14)) break p000e0129;
 		ACCwriteln(69);
 		ACCswap(14,17);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// DROP LANC
	p000e0130:
	{
 		if (skipdoall('p000e0130')) break p000e0130;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0130;
			if (!CNDnoun1(59)) break p000e0130;
 		}
		if (!CNDcarried(15)) break p000e0130;
 		ACCdrop(15);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP MAP
	p000e0131:
	{
 		if (skipdoall('p000e0131')) break p000e0131;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0131;
			if (!CNDnoun1(60)) break p000e0131;
 		}
		if (!CNDcarried(17)) break p000e0131;
 		ACCdrop(17);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP RIFL
	p000e0132:
	{
 		if (skipdoall('p000e0132')) break p000e0132;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0132;
			if (!CNDnoun1(61)) break p000e0132;
 		}
		if (!CNDcarried(18)) break p000e0132;
 		ACCdrop(18);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BAR
	p000e0133:
	{
 		if (skipdoall('p000e0133')) break p000e0133;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0133;
			if (!CNDnoun1(62)) break p000e0133;
 		}
		if (!CNDcarried(19)) break p000e0133;
 		ACCdrop(19);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BEVERAGE
	p000e0134:
	{
 		if (skipdoall('p000e0134')) break p000e0134;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0134;
			if (!CNDnoun1(64)) break p000e0134;
 		}
		if (!CNDcarried(20)) break p000e0134;
 		ACCdrop(20);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BEVERAGE
	p000e0135:
	{
 		if (skipdoall('p000e0135')) break p000e0135;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0135;
			if (!CNDnoun1(64)) break p000e0135;
 		}
		if (!CNDcarried(21)) break p000e0135;
 		ACCdrop(21);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// DROP BEVERAGE
	p000e0136:
	{
 		if (skipdoall('p000e0136')) break p000e0136;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0136;
			if (!CNDnoun1(64)) break p000e0136;
 		}
		if (!CNDcarried(22)) break p000e0136;
 		ACCdrop(22);
		if (!success) break pro000_restart;
 		ACCok();
		break pro000_restart;
		{}

	}

	// _ SAVED
	p000e0137:
	{
 		if (skipdoall('p000e0137')) break p000e0137;
 		if (in_response)
		{
			if (!CNDnoun1(120)) break p000e0137;
 		}
		if (!CNDnoun2(121)) break p000e0137;
 		ACClistsavedgames();
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0138:
	{
 		if (skipdoall('p000e0138')) break p000e0138;
 		ACChook(70);
		if (done_flag) break pro000_restart;
		{}

	}

	// DROP _
	p000e0139:
	{
 		if (skipdoall('p000e0139')) break p000e0139;
 		if (in_response)
		{
			if (!CNDverb(101)) break p000e0139;
 		}
		if (!CNDat(67)) break p000e0139;
 		ACCwriteln(71);
 		ACCend();
		break pro000_restart;
		{}

	}

	// I _
	p000e0140:
	{
 		if (skipdoall('p000e0140')) break p000e0140;
 		if (in_response)
		{
			if (!CNDverb(104)) break p000e0140;
 		}
 		ACCinven();
		break pro000_restart;
		{}

	}

	// L _
	p000e0141:
	{
 		if (skipdoall('p000e0141')) break p000e0141;
 		if (in_response)
		{
			if (!CNDverb(105)) break p000e0141;
 		}
 		ACCset(29);
 		ACCdesc();
		break pro000_restart;
		{}

	}

	// QUIT _
	p000e0142:
	{
 		if (skipdoall('p000e0142')) break p000e0142;
 		if (in_response)
		{
			if (!CNDverb(106)) break p000e0142;
 		}
 		ACCquit();
 		function anykey00019() 
		{
 		ACCturns();
 		ACCend();
		return;
		}
 		waitKey(anykey00019);
		done_flag=true;
		break pro000_restart;
		{}

	}

	// SAVE _
	p000e0143:
	{
 		if (skipdoall('p000e0143')) break p000e0143;
 		if (in_response)
		{
			if (!CNDverb(107)) break p000e0143;
 		}
 		ACCsave();
		break pro000_restart;
		{}

	}

	// LOAD _
	p000e0144:
	{
 		if (skipdoall('p000e0144')) break p000e0144;
 		if (in_response)
		{
			if (!CNDverb(108)) break p000e0144;
 		}
 		ACCload();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0145:
	{
 		if (skipdoall('p000e0145')) break p000e0145;
		if (!CNDpresent(16)) break p000e0145;
		if (!CNDeq(18,0)) break p000e0145;
 		ACCwriteln(72);
 		ACCset(18);
 		ACCdone();
		break pro000_restart;
		{}

	}

	// _ _
	p000e0146:
	{
 		if (skipdoall('p000e0146')) break p000e0146;
 		ACChook(73);
		if (done_flag) break pro000_restart;
		{}

	}


}
}

function pro001()
{
process_restart=true;
pro001_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p001e0000:
	{
 		if (skipdoall('p001e0000')) break p001e0000;
 		ACChook(74);
		if (done_flag) break pro001_restart;
		{}

	}

	// _ _
	p001e0001:
	{
 		if (skipdoall('p001e0001')) break p001e0001;
		if (!CNDat(0)) break p001e0001;
 		ACCbclear(12,5);
		if (!CNDzero(250)) break p001e0001;
 		ACClet(250,1);
 		ACCgoto(102);
 		ACCdesc();
		break pro001_restart;
		{}

	}

	// _ _
	p001e0002:
	{
 		if (skipdoall('p001e0002')) break p001e0002;
		if (!CNDat(102)) break p001e0002;
 		ACCblock(75,102,3);
		{}

	}

	// _ _
	p001e0003:
	{
 		if (skipdoall('p001e0003')) break p001e0003;
		if (!CNDislight()) break p001e0003;
 		ACClistobj();
 		ACClistnpc(getFlag(38));
		{}

	}


}
}

function pro002()
{
process_restart=true;
pro002_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p002e0000:
	{
 		if (skipdoall('p002e0000')) break p002e0000;
		if (!CNDeq(31,0)) break p002e0000;
		if (!CNDeq(32,0)) break p002e0000;
 		ACCability(5,5);
		{}

	}

	// _ _
	p002e0001:
	{
 		if (skipdoall('p002e0001')) break p002e0001;
 		ACChook(76);
		if (done_flag) break pro002_restart;
		{}

	}

	// _ _
	p002e0002:
	{
 		if (skipdoall('p002e0002')) break p002e0002;
		if (!CNDat(8)) break p002e0002;
 		ACCturns();
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0003:
	{
 		if (skipdoall('p002e0003')) break p002e0003;
		if (!CNDat(36)) break p002e0003;
		if (!CNDpresent(13)) break p002e0003;
 		ACCwriteln(77);
 		ACCplus(19,1);
		{}

	}

	// _ _
	p002e0004:
	{
 		if (skipdoall('p002e0004')) break p002e0004;
		if (!CNDcarried(12)) break p002e0004;
 		ACCwriteln(78);
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0005:
	{
 		if (skipdoall('p002e0005')) break p002e0005;
		if (!CNDat(20)) break p002e0005;
		if (!CNDeq(115,0)) break p002e0005;
 		ACCwriteln(79);
 		ACCend();
		break pro002_restart;
		{}

	}

	// _ _
	p002e0006:
	{
 		if (skipdoall('p002e0006')) break p002e0006;
		if (!CNDeq(19,3)) break p002e0006;
 		ACCwriteln(80);
 		ACCend();
		break pro002_restart;
		{}

	}


}
}

function pro003()
{
process_restart=true;
pro003_restart: while(process_restart)
{
	process_restart=false;
	// _ _
	p003e0000:
	{
 		if (skipdoall('p003e0000')) break p003e0000;
 		ACClet(61,68);
 		ACCcommand(1);
 		ACCgoto(0);
 		ACCdesc();
		break pro003_restart;
		{}

	}


}
}

last_process = 3;
// This file is (C) Carlos Sanchez 2014, released under the MIT license

// This function is called first by the start() function that runs when the game starts for the first time
var h_init = function()
{
}


// This function is called last by the start() function that runs when the game starts for the first time
var h_post =  function()
{
}

// This function is called when the engine tries to write any text
var h_writeText =  function (text)
{
	return text;
}

//This function is called every time the user types any order
var h_playerOrder = function(player_order)
{
	return player_order;
}

// This function is called every time a location is described, just after the location text is written
var h_description_init =  function ()
{
}

// This function is called every time a location is described, just after the process 1 is executed
var h_description_post = function()
{
}


// this function is called when the savegame object has been created, in order to be able to add more custom properties
var h_saveGame = function(savegame_object)
{
	return savegame_object;
}


// this function is called after the restore game function has restored the standard information in savegame, in order to restore any additional data included in a patched (by h_saveGame) savegame.
var h_restoreGame = function(savegame_object)
{
}

// this funcion is called before writing a message about player order beeing impossible to understand
var h_invalidOrder = function(player_order)
{
}

// this function is called when a sequence tag is found giving a chance for any hook library to provide a response
// tagparams receives the params inside the tag as an array  {XXXX|nn|mm|yy} => ['XXXX', 'nn', 'mm', 'yy']
var h_sequencetag = function (tagparams)
{
	return '';
}

// this function is called from certain points in the response or process tables via the HOOK condact. Depending on the string received it can do something or not.
// it's designed to allow direct javascript code to take control in the start database just installing a plugin library (avoiding the wirter need to enter code to activate the library)
var h_code = function(str)
{
	return false;
}


// this function is called from the keydown evente handler used by block and other functions to emulate a pause or waiting for a keypress. It is designed to allow plugin condacts or
// libraries to attend those key presses and react accordingly. In case a hook function decides that the standard keydown functions should not be processed, the hook function should return false.
// Also, any h_keydown replacement should probably do the same.
var h_keydown = function (event)
{
	return true;
}


// this function is called every time a process is called,  either by the internall loop of by the PROCESS condact, just before running it.
var h_preProcess = function(procno)
{

}

// this function is called every time a process is called just after the process exits (no matter which DONE status it has), either by the internall loop of by the PROCESS condact
var h_postProcess= function (procno)
{

}// This file is (C) Carlos Sanchez 2014, and is released under the MIT license
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS ///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function ACCdesc()
{
	describe_location_flag = true;
	ACCbreak(); // Cancel doall loop
}


function ACCdone()
{
	done_flag = true;
}

function CNDat(locno)
{
  return (loc_here()==locno);
}

function CNDnotat(locno)
{
	 return (loc_here()!=locno);
}


function CNDatgt(locno)
{
	 return (loc_here()>locno);
}


function CNDatlt(locno)
{
	 return (loc_here()<locno);
}

function CNDpresent(objno)
{
	var loc = getObjectLocation(objno);
	if (loc == loc_here()) return true;
	if (loc == LOCATION_WORN) return true;
	if (loc == LOCATION_CARRIED) return true;
	if ( (!bittest(getFlag(FLAG_PARSER_SETTINGS),7)) && (objectIsContainer(loc) || objectIsSupporter(loc))  &&  (loc<=last_object_number)  && (CNDpresent(loc)) )  // Extended context and object in another object that is present
	{
		if (objectIsSupporter(loc)) return true;  // On supporter
		if ( objectIsContainer(loc) && objectIsAttr(loc, ATTR_OPENABLE) && objectIsAttr(loc, ATTR_OPEN)) return true; // In a openable & open container
		if ( objectIsContainer(loc) && (!objectIsAttr(loc, ATTR_OPENABLE)) ) return true; // In a not openable container
	}
	return false;
}

function CNDabsent(objno)
{
	return !CNDpresent(objno);
}

function CNDworn(objno)
{
	return (getObjectLocation(objno) == LOCATION_WORN);
}

function CNDnotworn(objno)
{
	return !CNDworn(objno);
}

function CNDcarried(objno)
{
	return (getObjectLocation(objno) == LOCATION_CARRIED);	
}

function CNDnotcarr(objno)
{
	return !CNDcarried(objno);
}


function CNDchance(percent)
{
	 var val = Math.floor((Math.random()*101));
	 return (val<=percent);
}

function CNDzero(flagno)
{
	return (getFlag(flagno) == 0);
}

function CNDnotzero(flagno)
{
	 return !CNDzero(flagno)
}


function CNDeq(flagno, value)
{
	return (getFlag(flagno) == value);
}

function CNDnoteq(flagno,value)
{
	return !CNDeq(flagno, value);
}

function CNDgt(flagno, value)
{
	return (getFlag(flagno) > value);
}

function CNDlt(flagno, value)
{
	return (getFlag(flagno) < value);
}


function CNDadject1(wordno)
{
	return (getFlag(FLAG_ADJECT1) == wordno);
}

function CNDadverb(wordno)
{
	return (getFlag(FLAG_ADVERB) == wordno);
}


function CNDtimeout()
{
	 return bittest(getFlag(FLAG_TIMEOUT_SETTINGS),7);
}


function CNDisat(objno, locno)
{
	return (getObjectLocation(objno) == locno);

}


function CNDisnotat(objno, locno)
{
	return !CNDisat(objno, locno);
}



function CNDprep(wordno)
{
	return (getFlag(FLAG_PREP) == wordno);
}




function CNDnoun2(wordno)
{
	return (getFlag(FLAG_NOUN2) == wordno);
}

function CNDadject2(wordno)
{
	return (getFlag(FLAG_ADJECT2) == wordno);
}

function CNDsame(flagno1,flagno2)
{
	return (getFlag(flagno1) == getFlag(flagno2));
}


function CNDnotsame(flagno1,flagno2)
{
	return (getFlag(flagno1) != getFlag(flagno2));
}

function ACCinven()
{
	var count = 0;
	writeSysMessage(SYSMESS_YOUARECARRYING);
	ACCnewline();
	var listnpcs_with_objects = !bittest(getFlag(FLAG_PARSER_SETTINGS),3);
	var i;
	for (i=0;i<num_objects;i++)
	{
		if ((getObjectLocation(i)) == LOCATION_CARRIED)
		{
			
			if ((listnpcs_with_objects) || (!objectIsNPC(i)))
			{
				writeObject(i);
				if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
				ACCnewline();
				count++;
			}
		}
		if (getObjectLocation(i) == LOCATION_WORN)
		{
			if (listnpcs_with_objects || (!objectIsNPC(i)))
			{
				writeObject(i);
				writeSysMessage(SYSMESS_WORN);
				count++;
				ACCnewline();
			}
		}
	}
	if (!count) 
	{
		 writeSysMessage(SYSMESS_CARRYING_NOTHING);
		 ACCnewline();
	}

	if (!listnpcs_with_objects)
	{
		var numNPC = getNPCCountAt(LOCATION_CARRIED);
		if (numNPC)	ACClistnpc(LOCATION_CARRIED);
	}
	done_flag = true;
}

function desc()
{
	describe_location_flag = true;
}


function ACCquit()
{
	inQUIT = true;
	writeSysMessage(SYSMESS_AREYOUSURE);
}


function ACCend()
{
	$('.input').hide();
	inEND = true;
	writeSysMessage(SYSMESS_PLAYAGAIN);
	done_flag = true;
}


function done()
{
	done_flag = true;
}

function ACCok()
{
	writeSysMessage(SYSMESS_OK);
	done_flag = true;
}



function ACCramsave()
{
	ramsave_value = getSaveGameObject();
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	localStorage.setItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME, savegame);
}

function ACCramload()
{
	if (ramsave_value==null) 
	{
		var json_str = localStorage.getItem('ngpaws_savegame_' + STR_RAMSAVE_FILENAME);
		if (json_str)
		{
			savegame_object = JSON.parse(json_str.trim());
			restoreSaveGameObject(savegame_object);
			ACCdesc();
			focusInput();
			return;
		}
		else
		{
			writeText (STR_RAMLOAD_ERROR);
			ACCnewline();
			done_flag = true;
			return;
		}
	}
	restoreSaveGameObject(ramsave_value);
	ACCdesc();
}

function ACCsave()
{
	var savegame_object = getSaveGameObject();	
	savegame =   JSON.stringify(savegame_object);
	filename = prompt(getSysMessageText(SYSMESS_SAVEFILE),'');
	if ( filename !== null ) localStorage.setItem('ngpaws_savegame_' + filename.toUpperCase(), savegame);
	ACCok();
}

 
function ACCload() 	
{
	var json_str;
	filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
	if ( filename !== null ) json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
	if (json_str)
	{
		savegame_object = JSON.parse(json_str.trim());
		restoreSaveGameObject(savegame_object);
	}
	else
	{
		writeSysMessage(SYSMESS_FILENOTFOUND);
		ACCnewline();
		done_flag = true; return;
	}
	ACCdesc();
	focusInput();
}



function ACCturns()
{
	var turns = getFlag(FLAG_TURNS_HIGH) * 256 +  getFlag(FLAG_TURNS_LOW);
	writeSysMessage(SYSMESS_TURNS_START);
	writeText(turns + '');
	writeSysMessage(SYSMESS_TURNS_CONTINUE);
	if (turns > 1) writeSysMessage(SYSMESS_TURNS_PLURAL);
	writeSysMessage(SYSMESS_TURNS_END);
}

function ACCscore()
{
	var score = getFlag(FLAG_SCORE);
	writeSysMessage(SYSMESS_SCORE_START);
	writeText(score + '');
	writeSysMessage(SYSMESS_SCORE_END);
}


function ACCcls()
{
	clearScreen();
}

function ACCdropall()
{
	// Done in two different loops cause PAW did it like that, just a question of retro compatibility
	var i;
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_CARRIED)setObjectLocation(i, getFlag(FLAG_LOCATION));
	for (i=0;i<num_objects;i++)	if (getObjectLocation(i) == LOCATION_WORN)setObjectLocation(i, getFlag(FLAG_LOCATION));
}


function ACCautog()
{
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),7))  // Extended context for objects
	for (var i=0; i<num_objects;i++) // Try to find it in present containers/supporters
	{
		if (CNDpresent(i) && (isAccesibleContainer(i) || objectIsAttr(i, ATTR_SUPPORTER)) )  // If there is another object present that is an accesible container or a supporter
		{
			objno =findMatchingObject(i);
			if (objno != EMPTY_OBJECT) { ACCget(objno); return; };
		}
	}
	success = false;
	writeSysMessage(SYSMESS_CANTSEETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautod()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };  
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCdrop(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautow()
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCwear(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautor()
{
	var objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	objno =findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCremove(objno); return; };
	success = false;
	writeSysMessage(SYSMESS_YOURENOTWEARINGTHAT);
	ACCnewtext();
	ACCdone();
}



function ACCpause(value)
{
 if (value == 0) value = 256;
 pauseRemainingTime = Math.floor(value /50 * 1000);	
 inPause = true;
 showAnykeyLayer();
} 

function ACCgoto(locno)
{
 	setFlag(FLAG_LOCATION,locno);
}

function ACCmessage(mesno)
{
	writeMessage(mesno);
	ACCnewline();
}


function ACCremove(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case loc_here():
			writeSysMessage(SYSMESS_YOUARENOTWEARINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case LOCATION_WORN:
			if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
			{
				writeSysMessage(SYSMESS_CANTREMOVE_TOOMANYOBJECTS);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_CARRIED);
			writeSysMessage(SYSMESS_YOUREMOVEOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUARENOTWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}


function trytoGet(objno)  // auxiliaty function for ACCget
{
	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		doall_flag = false;
		return;
	}
	var weight = 0;
	weight += getObjectWeight(objno);
	weight +=  getLocationObjectsWeight(LOCATION_CARRIED);
	weight +=  getLocationObjectsWeight(LOCATION_WORN);
	if (weight > getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}
	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;
}


 function ACCget(objno)
 {
 	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_CARRIED:  
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():
			trytoGet(objno);
			break;

		default: 
			if  ((locno<=last_object_number) && (CNDpresent(locno)))    // If it's not here, carried or worn but it present, that means that bit 7 of flag 12 is cleared, thus you can get objects from present containers/supporters
			{
				trytoGet(objno);
			}
			else
			{
				writeSysMessage(SYSMESS_CANTSEETHAT);
				ACCnewtext();
				ACCdone();
				return;
				break;
		    }
	}
 }

function ACCdrop(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			setObjectLocation(objno, loc_here());
			writeSysMessage(SYSMESS_YOUDROPOBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}

function ACCwear(objno)
{
	success = false; 
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setReferredObject(objno);
	var locno = getObjectLocation(objno);
	switch (locno)
	{
		case LOCATION_WORN:  
			writeSysMessage(SYSMESS_YOUAREALREADYWAERINGOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;

		case loc_here():  
			writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
			ACCnewtext();
			ACCdone();
			return;
			break;


		case LOCATION_CARRIED:  
			if (!objectIsWearable(objno))
			{
				writeSysMessage(SYSMESS_YOUCANTWEAROBJECT);
				ACCnewtext();
				ACCdone();
				return;
			}
			setObjectLocation(objno, LOCATION_WORN);
			writeSysMessage(SYSMESS_YOUWEAROBJECT);
			success = true;
			break;

		default: 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return;
			break;
	}
}



function ACCdestroy(objno)
{
	setObjectLocation(objno, LOCATION_NONCREATED);
}


function ACCcreate(objno)
{
	setObjectLocation(objno, loc_here());
}


function ACCswap(objno1,objno2)
{
	var locno1 = getObjectLocation (objno1);
	var locno2 = getObjectLocation (objno2);
	ACCplace (objno1,locno2);
	ACCplace (objno2,locno1);
	setReferredObject(objno2);
}


function ACCplace(objno, locno)
{
	setObjectLocation(objno, locno);
}

function ACCset(flagno)
{
	setFlag(flagno, SET_VALUE);
}

function ACCclear(flagno)
{
	setFlag(flagno,0);
}

function ACCplus(flagno,value)
{
	var newval = getFlag(flagno) + value;
	setFlag(flagno, newval);
}

function ACCminus(flagno,value)
{
    var newval = getFlag(flagno) - value;
    if (newval < 0) newval = 0;
	setFlag(flagno, newval);
}

function ACClet(flagno,value)
{
	setFlag(flagno,value);
}

function ACCnewline()
{
	writeText(STR_NEWLINE);
}

function ACCprint(flagno)
{
	writeText(getFlag(flagno) +'');
}

function ACCsysmess(sysno)
{
	writeSysMessage(sysno);
}

function ACCcopyof(objno,flagno)
{
	setFlag(flagno, getObjectLocation(objno))
}

function ACCcopyoo(objno1, objno2)
{
	setObjectLocation(objno2,getObjectLocation(objno1));
	setReferredObject(objno2);
}

function ACCcopyfo(flagno,objno)
{
	setObjectLocation(objno, getFlag(flagno));
}

function ACCcopyff(flagno1, flagno2)
{
	setFlag(flagno2, getFlag(flagno1));
}

function ACCadd(flagno1, flagno2)
{
	var newval = getFlag(flagno1) + getFlag(flagno2);
	setFlag(flagno2, newval);
}

function ACCsub(flagno1,flagno2)
{
	var newval = getFlag(flagno2) - getFlag(flagno1);
	if (newval < 0) newval = 0;
	setFlag(flagno2, newval);
}


function CNDparse()
{
	return (!getLogicSentence());
}


function ACClistat(locno, container_objno)   // objno is a container/suppoter number, used to list contents of objects
{
  var listingContainer = false;
  if (arguments.length > 1) listingContainer = true;
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);  
  objscount = objscount - concealed_or_scenery_objcount;
  if (!listingContainer) setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
  if (!objscount) return;
  var continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  if (listingContainer) 
  	{
  		writeText(' (');
  		if (objectIsAttr(container_objno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_OVER_YOUCANSEE); else if (objectIsAttr(container_objno, ATTR_CONTAINER)) writeSysMessage(SYSMESS_INSIDE_YOUCANSEE);
  		continouslisting = true;  // listing contents of container always continuous
  	}
  
  if (!listingContainer)
  {
    setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),7)); 
    if (!continouslisting) ACCnewline();
  }
  var progresscount = 0;
  for (var i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if  ( ((!objectIsNPC(i)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)))  && (!objectIsAttr(i,ATTR_CONCEALED)) && (!objectIsAttr(i,ATTR_SCENERY))   ) // if not an NPC or parser setting say NPCs are considered objects, and object is not concealed nor scenery
  		  { 
  		     writeText(getObjectText(i)); 
  		     if ((objectIsAttr(i,ATTR_SUPPORTER))  || (  (objectIsAttr(i,ATTR_TRANSPARENT))  && (objectIsAttr(i,ATTR_CONTAINER))))  ACClistat(i, i);
  		     progresscount++
  		     if (continouslisting)
  		     {
		  			if (progresscount <= objscount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  					if (progresscount == objscount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  					if (!listingContainer) if (progresscount == objscount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
  if (arguments.length > 1) writeText(')');
}


function ACClistnpc(locno)
{
  var npccount =  getNPCCountAt(locno);
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitclear(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  if (!npccount) return;
  setFlag(FLAG_OBJECT_LIST_FORMAT, bitset(getFlag(FLAG_OBJECT_LIST_FORMAT),5)); 
  continouslisting = bittest(getFlag(FLAG_OBJECT_LIST_FORMAT),6);
  writeSysMessage(SYSMESS_NPCLISTSTART);
  if (!continouslisting) ACCnewline();
  if (npccount==1)  writeSysMessage(SYSMESS_NPCLISTCONTINUE); else writeSysMessage(SYSMESS_NPCLISTCONTINUE_PLURAL);
  var progresscount = 0;
  var i;
  for (i=0;i<num_objects;i++)
  {
  	if (getObjectLocation(i) == locno)
  		if ( (objectIsNPC(i)) && (!objectIsAttr(i,ATTR_CONCEALED)) ) // only NPCs not concealed
  		  { 
  		     writeText(getObjectText(i)); 
  		     progresscount++
  		     if (continouslisting)
  		     {
		  	 	if (progresscount <= npccount - 2) writeSysMessage(SYSMESS_LISTSEPARATOR);
  			 	if (progresscount == npccount - 1) writeSysMessage(SYSMESS_LISTLASTSEPARATOR);
  			 	if (progresscount == npccount ) writeSysMessage(SYSMESS_LISTEND);
  			 } else ACCnewline();
  		  }; 
  }
}


function ACClistobj()
{
  var locno = loc_here();
  var objscount =  getObjectCountAt(locno);
  var concealed_or_scenery_objcount = getObjectCountAtWithAttr(locno, [ATTR_CONCEALED, ATTR_SCENERY]);

  objscount = objscount - concealed_or_scenery_objcount;
  if (objscount)
  {
	  writeSysMessage(SYSMESS_YOUCANSEE);
      ACClistat(loc_here());
  }
}

function ACCprocess(procno)
{
	if (procno > last_process) 
	{
		writeText(STR_WRONG_PROCESS);
		ACCnewtext();
		ACCdone();
	}
	callProcess(procno);
    if (describe_location_flag) done_flag = true;
}

function ACCmes(mesno)
{
	writeMessage(mesno);
}

function ACCmode(mode)
{
	setFlag(FLAG_MODE, mode);
}

function ACCtime(length, settings)
{
	setFlag(FLAG_TIMEOUT_LENGTH, length);
	setFlag(FLAG_TIMEOUT_SETTINGS, settings);
}

function ACCdoall(locno)
{
	doall_flag = true;
	if (locno == LOCATION_HERE) locno = loc_here();
	// Each object will be considered for doall loop if is at locno and it's not the object specified by the NOUN2/ADJECT2 pair and it's not a NPC (or setting to consider NPCs as objects is set)
	setFlag(FLAG_DOALL_LOC, locno);
	var doall_obj;
	doall_loop:
	for (doall_obj=0;(doall_obj<num_objects) && (doall_flag);doall_obj++)  
	{
		if (getObjectLocation(doall_obj) == locno)
			if ((!objectIsNPC(doall_obj)) || (!bittest(getFlag(FLAG_PARSER_SETTINGS),3))) 
 			 if (!objectIsAttr(doall_obj, ATTR_CONCEALED)) 
 			  if (!objectIsAttr(doall_obj, ATTR_SCENERY)) 
				if (!( (objectsNoun[doall_obj]==getFlag(FLAG_NOUN2))  &&    ((objectsAdjective[doall_obj]==getFlag(FLAG_ADJECT2)) || (objectsAdjective[doall_obj]==EMPTY_WORD)) ) ) // implements "TAKE ALL EXCEPT BIG SWORD"
				{
					setFlag(FLAG_NOUN1, objectsNoun[doall_obj]);
					setFlag(FLAG_ADJECT1, objectsAdjective[doall_obj]);
					setReferredObject(doall_obj);
					callProcess(process_in_doall);
					if (describe_location_flag) 
						{
							doall_flag = false;
							entry_for_doall = '';
							break doall_loop;
						}
				}
	}
	doall_flag = false;
	entry_for_doall = '';
	if (describe_location_flag) descriptionLoop();
}

function ACCprompt(value)  // deprecated
{
	setFlag(FLAG_PROMPT, value);
	setInputPlaceHolder();
}


function ACCweigh(objno, flagno)
{
	var weight = getObjectWeight(objno);
	setFlag(flagno, weight);
}

function ACCputin(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if (getObjectLocation(objno) == LOCATION_WORN)
	{
		writeSysMessage(SYSMESS_YOUAREALREADYWEARINGTHAT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == LOCATION_CARRIED)
	{
		setObjectLocation(objno, locno);
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUPUTOBJECTON); else writeSysMessage(SYSMESS_YOUPUTOBJECTIN);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		success = true;
		return;
	}

	writeSysMessage(SYSMESS_YOUDONTHAVEOBJECT);
	ACCnewtext();
	ACCdone();
}


function ACCtakeout(objno, locno)
{
	success = false;
	setReferredObject(objno);
	if ((getObjectLocation(objno) == LOCATION_WORN) || (getObjectLocation(objno) == LOCATION_CARRIED))
	{
		writeSysMessage(SYSMESS_YOUALREADYHAVEOBJECT);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectLocation(objno) == loc_here())
	{
		if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
		writeText(getObjectFixArticles(locno));
		writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getObjectWeight(objno) + getLocationObjectsWeight(LOCATION_WORN) + getLocationObjectsWeight(LOCATION_CARRIED) >  getFlag(FLAG_MAXWEIGHT_CARRIED))
	{
		writeSysMessage(SYSMESS_WEIGHSTOOMUCH);
		ACCnewtext();
		ACCdone();
		return;
	}

	if (getFlag(FLAG_OBJECTS_CARRIED_COUNT) >= getFlag(FLAG_MAXOBJECTS_CARRIED))
	{		
		writeSysMessage(SYSMESS_CANTCARRYANYMORE);
		ACCnewtext();
		ACCdone();
		return;
	}

	setObjectLocation(objno, LOCATION_CARRIED);
	writeSysMessage(SYSMESS_YOUTAKEOBJECT);
	success = true;


}
function ACCnewtext()
{
	player_order_buffer = '';
}

function ACCability(maxObjectsCarried, maxWeightCarried)
{
	setFlag(FLAG_MAXOBJECTS_CARRIED, maxObjectsCarried);
	setFlag(FLAG_MAXWEIGHT_CARRIED, maxWeightCarried);
}

function ACCweight(flagno)
{
	var weight_carried = getLocationObjectsWeight(LOCATION_CARRIED);
	var weight_worn = getLocationObjectsWeight(LOCATION_WORN);
	var total_weight = weight_worn + weight_carried;
	setFlag(flagno, total_weight);
}


function ACCrandom(flagno)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*100)));
}

function ACCwhato()
{
	var whatofound = getReferredObject();
	if (whatofound != EMPTY_OBJECT) setReferredObject(whatofound);
}

function ACCputo(locno)
{
	setObjectLocation(getFlag(FLAG_REFERRED_OBJECT), locno);
}

function ACCnotdone()
{
	done_flag = false;
}

function ACCautop(locno)
{
	var objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCputin(objno, locno); return; };
	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			writeSysMessage(SYSMESS_YOUDONTHAVETHAT);
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
}


function ACCautot(locno)
{

	var objno =findMatchingObject(locno);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_CARRIED);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno =findMatchingObject(LOCATION_WORN);
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };
	objno = findMatchingObject(loc_here());
	if (objno != EMPTY_OBJECT) { ACCtakeout(objno, locno); return; };

	objno = findMatchingObject(null); // anywhere
	if (objno != EMPTY_OBJECT) 
		{ 
			if (objectIsAttr(locno, ATTR_SUPPORTER)) writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTFROM); else writeSysMessage(SYSMESS_YOUCANTTAKEOBJECTOUTOF);
			writeText(getObjectFixArticles(locno));
			writeSysMessage(SYSMESS_PUTINTAKEOUTTERMINATION)
			ACCnewtext();
			ACCdone();
			return; 
		};

	success = false;
	writeSysMessage(SYSMESS_CANTDOTHAT);
	ACCnewtext();
	ACCdone();
	
}


function CNDmove(flagno)
{
	var locno = getFlag(flagno);
	var dirno = getFlag(FLAG_VERB);
	var destination = getConnection( locno,  dirno);
	if (destination != -1) 
		{
			 setFlag(flagno, destination);
			 return true;
		}
	return false;
}


function ACCextern(writeno)
{
	eval(writemessages[writeno]);
}


function ACCpicture(picno)
{
	drawPicture(picno);
}



function ACCgraphic(option)
{
	graphicsON = (option==1);  
	if (!graphicsON) hideGraphicsWindow();	
}

function ACCbeep(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'play');
}

function ACCsound(value)
{
	soundsON = (value==1);  
	if (!soundsON) sfxstopall();
}

function CNDozero(objno, attrno)
{
	if (attrno > 63) return false;
	return !objectIsAttr(objno, attrno);

}

function CNDonotzero(objno, attrno)
{
	return objectIsAttr(objno, attrno);
}

function ACCoset(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		attrs = getObjectLowAttributes(objno);
		var attrs = bitset(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitset(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}

function ACCoclear(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitclear(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitclear(attrs, attrno);
	setObjectHighAttributes(objno, attrs);

}


function CNDislight()
{
	if (!isDarkHere()) return true;
	return lightObjectsPresent();
}



function CNDisnotlight()
{
	return ! CNDislight();
}

function ACCversion()
{
	writeText(filterText(STR_RUNTIME_VERSION));
}


function ACCwrite(writeno)
{
	writeWriteMessage(writeno);
}

function ACCwriteln(writeno)
{
	writeWriteMessage(writeno);
	ACCnewline();
}

function ACCrestart()
{
  process_restart = true;
}


function ACCtranscript()
{
	$('#transcript_area').html(transcript);
	$('.transcript_layer').show();
	inTranscript = true;
}

function ACCanykey()
{
	writeSysMessage(SYSMESS_PRESSANYKEY);
	inAnykey = true;
}

function ACCgetkey(flagno)
{
	getkey_return_flag = flagno;
	inGetkey = true;
}


//////////////////
//   LEGACY     //
//////////////////

// From PAW PC
function ACCbell()
{
 	// Empty, PAW PC legacy, just does nothing 
}


// From PAW Spectrum
function ACCreset()
{
	// Legacy condact, does nothing now
}


function ACCpaper(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCink(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCborder(color)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCcharset(value)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCline(lineno)
{
	// Legacy condact, does nothing now, use CSS styles
}

function ACCinput()
{
	// Legacy condact, does nothing now
}

function ACCsaveat()
{
	// Legacy condact, does nothing now
}

function ACCbackat()
{
	// Legacy condact, does nothing now
}

function ACCprintat()
{
	// Legacy condact, does nothing now
}

function ACCprotect()
{
	// Legacy condact, does nothing now
}

// From Superglus


function ACCdebug()
{
	// Legacy condact, does nothing now		
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////// CONDACTS FOR COMPILER //////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function CNDverb(wordno)
{
	return (getFlag(FLAG_VERB) == wordno);
}


function CNDnoun1(wordno)
{
	return (getFlag(FLAG_NOUN1) == wordno);
}

//   PLUGINS    ;

//CND RNDWRITELN A 14 14 14 0

function ACCrndwriteln(writeno1,writeno2,writeno3)
{
	ACCrndwrite(writeno1,writeno2,writeno3);
	ACCnewline();
}
//CND SYNONYM A 15 13 0 0

function ACCsynonym(wordno1, wordno2)
{
   if (wordno1!=EMPTY_WORD) setFlag(FLAG_VERB, wordno1);
   if (wordno2!=EMPTY_WORD)	setFlag(FLAG_NOUN1, wordno2);
}
//CND RNDWRITE A 14 14 14 0

function ACCrndwrite(writeno1,writeno2,writeno3)
{
	var val = Math.floor((Math.random()*3));
	switch (val)
	{
		case 0 : writeWriteMessage(writeno1);break;
		case 1 : writeWriteMessage(writeno2);break;
		case 2 : writeWriteMessage(writeno3);break;
	}
}
//CND SETWEIGHT A 4 2 0 0

function ACCsetweight(objno, value)
{
   objectsWeight[objno] = value;
}

//CND BSET A 1 2 0 0

function ACCbset(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitset(getFlag(flagno),bitno));
}
//CND ISNOTMOV C 0 0 0 0

function CNDisnotmov()
{
	return !CNDismov();	
}

//CND VOLUME A 2 2 0 0

function ACCvolume(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxvolume(channelno, value);
}

//CND ISVIDEO C 0 0 0 0

function CNDisvideo()
{
	if (typeof videoElement == 'undefined') return false;
	if (!videoLoopCount) return false;
	if (videoElement.paused) return false;
	return true;
}

//CND GE C 1 2 0 0

function CNDge(flagno, valor)
{
	return (getFlag(flagno)>=valor);
}
//CND ISNOTDOALL C 0 0 0 0

function CNDisnotdoall()
{
	return !CNDisdoall();
}

//CND RESUMEVIDEO A 0 0 0 0


function ACCresumevideo()
{
	if (typeof videoElement != 'undefined') 
		if (videoElement.paused)
		  videoElement.play();
}

//CND PAUSEVIDEO A 0 0 0 0


function ACCpausevideo()
{
	if (typeof videoElement != 'undefined') 
		if (!videoElement.ended) 
		if (!videoElement.paused)
		   videoElement.pause();
}

//CND BZERO C 1 2 0 0

function CNDbzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (!bittest(getFlag(flagno), bitno));
}
//CND BNEG A 1 2 0 0

function ACCbneg(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitneg(getFlag(flagno),bitno));
}
//CND ISDOALL C 0 0 0 0

function CNDisdoall()
{
	return doall_flag;	
}

//CND ONEG A 4 2 0 0

function ACConeg(objno, attrno)
{
	if (attrno > 63) return;
	if (attrno <= 31)
	{
		var attrs = getObjectLowAttributes(objno);
		attrs = bitneg(attrs, attrno);
		setObjectLowAttributes(objno, attrs);
		return;
	}
	var attrs = getObjectHighAttributes(objno);
	attrno = attrno - 32;
	attrs = bitneg(attrs, attrno);
	setObjectHighAttributes(objno, attrs);
}

//CND ISDONE C 0 0 0 0

function CNDisdone()
{
	return done_flag;	
}

//CND BLOCK A 14 2 2 0

function ACCblock(writeno, picno, procno)
{
   inBlock = true;
   disableInterrupt();
   $('.block_layer').hide();
   var text = getWriteMessageText(writeno);
   $('.block_text').html(text);
   
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var imgsrc = '<img class="block_picture" src="' + filename + '" />';
		$('.block_graphics').html(imgsrc);
	}
    if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
    $('.block_layer').show();

}

//CND HELP A 0 0 0 0

function ACChelp()
{
	if (getLang()=='EN') EnglishHelp(); else SpanishHelp();
}	

function EnglishHelp()
{
	writeText('HOW DO I SEND COMMANDS TO THE PC?');
	writeText(STR_NEWLINE);
	writeText('Use simple orders: OPEN DOOR, TAKE KEY, GO UP, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I MOVE IN THE MAP?');
	writeText(STR_NEWLINE);
	writeText('Usually you will have to use compass directions as north (shortcut: "N"), south (S), east (E), west (W) or other directions (up, down, enter, leave, etc.). Some games allow complex order like "go to well". Usually you would be able to know avaliable exits by location description, some games also provide the "EXITS" command.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK MY INVENTORY?');
	writeText(STR_NEWLINE);
	writeText('type INVENTORY (shortcut "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I USE THE OBJECTS?');
	writeText(STR_NEWLINE);
	writeText('Use the proper verb, that is, instead of USE KEY type OPEN.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I CHECK SOMETHING CLOSELY?');
	writeText(STR_NEWLINE);
	writeText('Use "examine" verb: EXAMINE DISH. (shortcut: EX)');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SEE AGAIN THE CURRENT LOCATION DSCRIPTION?');
	writeText(STR_NEWLINE);
	writeText('Type LOOK (shortcut "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I TALK TO OTHER CHARACTERS?');
	writeText(STR_NEWLINE);
	writeText('Most common methods are [CHARACTER, SENTENCE] or [SAY CHARACTER "SENTENCE"]. For instance: [JOHN, HELLO] o [SAY JOHN "HELLO"]. Some games also allow just [TALK TO JOHN]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING IN A CONTAINER, HOW CAN I TAKE SOMETHING OUT?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY IN BOX. TAKE KEY OUT OF BOX. INSERT KEY IN BOX. EXTRACT KEY FROM BOX.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I PUT SOMETHING ON SOMETHING ELSE?');
	writeText(STR_NEWLINE);
	writeText('PUT KEY ON TABLE. TAKE KEY FROM TABLE');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('HOW CAN I SAVE/RESTORE MY GAME?');
	writeText(STR_NEWLINE);
	writeText('Use SAVE/LOAD commands.');
	writeText(STR_NEWLINE + STR_NEWLINE);

}

function SpanishHelp()
{
	writeText('¿CÓMO DOY ORDENES AL PERSONAJE?');
	writeText(STR_NEWLINE);
	writeText('Utiliza órdenes en imperativo o infinitivo: ABRE PUERTA, COGER LLAVE, SUBIR, etc.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO ME MUEVO POR EL JUEGO?');
	writeText(STR_NEWLINE);
	writeText('Por regla general, mediante los puntos cardinales como norte (abreviado "N"), sur (S), este (E), oeste (O) o direcciones espaciales (arriba, abajo, bajar, subir, entrar, salir, etc.). Algunas aventuras permiten también cosas como "ir a pozo". Normalmente podrás saber en qué dirección puedes ir por la descripción del sitio, aunque algunos juegos facilitan el comando "SALIDAS" que te dirá exactamente cuáles hay.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO SABER QUE OBJETOS LLEVO?');
	writeText(STR_NEWLINE);
	writeText('Teclea INVENTARIO (abreviado "I")');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO USO LOS OBJETOS?');
	writeText(STR_NEWLINE);
	writeText('Utiliza el verbo correcto, en lugar de USAR ESCOBA escribe BARRER.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO MIRAR DE CERCA UN OBJETO U OBSERVARLO MÁS DETALLADAMENTE?');
	writeText(STR_NEWLINE);
	writeText('Con el verbo examinar: EXAMINAR PLATO. Generalmente se puede usar la abreviatura "EX": EX PLATO.');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PUEDO VER DE NUEVO LA DESCRIPCIÓN DEL SITIO DONDE ESTOY?');
	writeText(STR_NEWLINE);
	writeText('Escribe MIRAR (abreviado "M").');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO HABLO CON LOS PERSONAJES?');
	writeText(STR_NEWLINE);
	writeText('Los modos más comunes son [PERSONAJE, FRASE] o [DECIR A PERSONAJE "FRASE"]. Por ejemplo: [LUIS, HOLA] o [DECIR A LUIS "HOLA"]. En algunas aventuras también se puede utilizar el formato [HABLAR A LUIS]. ');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO METO ALGO EN UN CONTENEDOR? ¿CÓMO LO SACO?');
	writeText(STR_NEWLINE);
	writeText('METER LLAVE EN CAJA. SACAR LLAVE DE CAJA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO PONGO ALGO SOBRE ALGO? ¿CÓMO LO QUITO?');
	writeText(STR_NEWLINE);
	writeText('PONER LLAVE EN MESA. COGER LLAVE DE MESA');
	writeText(STR_NEWLINE + STR_NEWLINE);
	writeText('¿CÓMO GRABO Y CARGO LA PARTIDA?');
	writeText(STR_NEWLINE);
	writeText('Usa las órdenes SAVE y LOAD, o GRABAR y CARGAR.');
	writeText(STR_NEWLINE + STR_NEWLINE);
}

//CND BNOTZERO C 1 2 0 0

function CNDbnotzero(flagno, bitno)
{
	if (bitno>=32) return false;
	return (bittest(getFlag(flagno), bitno));
}

//CND PLAYVIDEO A 14 2 2 0

var videoLoopCount;
var videoEscapable;
var videoElement;

function ACCplayvideo(strno, loopCount, settings)
{
	videoEscapable = settings & 1; // if bit 0 of settings is 1, video can be interrupted with ESC key
	if (loopCount == 0) loopCount = -1;
	videoLoopCount = loopCount;

	str = '<video id="videoframe" height="100%">';
	str = str + '<source src="dat/' + writemessages[strno] + '.mp4" type="video/mp4" codecs="avc1.4D401E, mp4a.40.2">';
	str = str + '<source src="dat/' + writemessages[strno] + '.webm" type="video/webm" codecs="vp8.0, vorbis">';
	str = str + '<source src="dat/' + writemessages[strno] + '.ogg" type="video/ogg" codecs="theora, vorbis">';
	str = str + '</video>';
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#videoframe').css('height','100%');
	$('#videoframe').css('display','block');
	$('#videoframe').css('margin-left','auto');
	$('#videoframe').css('margin-right','auto');
	$('#graphics').show();
	videoElement = document.getElementById('videoframe');
	videoElement.onended = function() 
	{
    	if (videoLoopCount == -1) videoElement.play();
    	else
    	{
    		videoLoopCount--;
    		if (videoLoopCount) videoElement.play();
    	}
	};
	videoElement.play();

}

// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_video_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#videoframe").length > 0) $("#videoframe").remove();	
	old_video_h_description_init();
}

// Hook into keypress to cancel video playing if ESC is pressed and video is skippable

var old_video_h_keydown =  h_keydown;
h_keydown = function (event)
{
 	if ((event.keyCode == 27) && (typeof videoElement != 'undefined') && (!videoElement.ended) && (videoEscapable)) 
 	{
 		videoElement.pause(); 
 		return false;  // we've finished attending ESC press
 	}
 	else return old_video_h_keydown(event);
}




//CND TEXTPIC A 2 2 0 0

function ACCtextpic(picno, align)
{
	var style = '';
	var post = '';
	var pre = '';
	switch(align)
	{
		case 0: post='<br style="clear:left">';break;
		case 1: style = 'float:left'; break;
		case 2: style = 'float:right'; break;
		case 3: pre='<center>';post='</center><br style="clear:left">';break;
	}
	filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (filename)
	{
		var texto = pre + "<img alt='' class='textpic' style='"+style+"' src='"+filename+"' />" + post;
		writeText(texto);
		$(".text").scrollTop($(".text")[0].scrollHeight);
	}
}
//CND OBJFOUND C 2 9 0 0

function CNDobjfound(attrno, locno)
{

	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return true; }
	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return false;
}

//CND PICTUREAT A 2 2 2 0

/*
In order to determine the actual size of both background image and pictureat image they should be loaded, thus two chained "onload" are needed. That is, 
background image is loaded to determine its size, then pictureat image is loaded to determine its size. Size of currently displayed background image cannot
be used as it may have been already stretched.
*/

function ACCpictureat(x,y,picno)
{
	var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
	if (!filename) return;

	// Check location has a picture, otherwise exit
	var currentBackgroundScreenImage = $('.location_picture');
	if (!currentBackgroundScreenImage) return;

	// Create a new image with the contents of current background image, to be able to calculate original height of image
	var virtualBackgroundImage = new Image();
	// Pass required data as image properties in order to be avaliable at "onload" event
	virtualBackgroundImage.bg_data=[];
	virtualBackgroundImage.bg_data.filename = filename; 
	virtualBackgroundImage.bg_data.x = x;
	virtualBackgroundImage.bg_data.y = y;
	virtualBackgroundImage.bg_data.picno = picno;
	virtualBackgroundImage.bg_data.currentBackgroundScreenImage = currentBackgroundScreenImage;


	// Event triggered when virtual background image is loaded
	virtualBackgroundImage.onload = function()
		{
			var originalBackgroundImageHeight = this.height;
			var scale = this.bg_data.currentBackgroundScreenImage.height() / originalBackgroundImageHeight;

			// Create a new image with the contents of picture to show with PICTUREAT, to be able to calculate height of image
			var virtualPictureAtImage = new Image();
			// Also pass data from background image as property so they are avaliable in the onload event
			virtualPictureAtImage.pa_data = [];
			virtualPictureAtImage.pa_data.x = this.bg_data.x;
			virtualPictureAtImage.pa_data.y = this.bg_data.y;
			virtualPictureAtImage.pa_data.picno = this.bg_data.picno;
			virtualPictureAtImage.pa_data.filename = this.bg_data.filename;
			virtualPictureAtImage.pa_data.scale = scale;
			virtualPictureAtImage.pa_data.currentBackgroundImageWidth = this.bg_data.currentBackgroundScreenImage.width();
			
			// Event triggered when virtual PCITUREAT image is loaded
			virtualPictureAtImage.onload = function ()
			{
		    		var imageHeight = this.height; 
					var x = Math.floor(this.pa_data.x * this.pa_data.scale);
					var y = Math.floor(this.pa_data.y * this.pa_data.scale);
					var newimageHeight = Math.floor(imageHeight * this.pa_data.scale);
					var actualBackgroundImageX = Math.floor((parseInt($('.graphics').width()) - this.pa_data.currentBackgroundImageWidth)/2);;
					var id = 'pictureat_' + this.pa_data.picno;

					// Add new image, notice we are not using the virtual image, but creating a new one
					$('.graphics').append('<img  alt="" id="'+id+'" style="display:none" />');				
					$('#' + id).css('position','absolute');
					$('#' + id).css('left', actualBackgroundImageX + x  + 'px');
					$('#' + id).css('top',y + 'px');
					$('#' + id).css('z-index','100');
					$('#' + id).attr('src', this.pa_data.filename);
					$('#' + id).css('height',newimageHeight + 'px');
					$('#' + id).show();
			}

			// Assign the virtual pictureat image the destinationsrc to trigger the "onload" event
			virtualPictureAtImage.src = this.bg_data.filename;
			};

	// Assign the virtual background image same src as current background to trigger the "onload" event
	virtualBackgroundImage.src = currentBackgroundScreenImage.attr("src");

}

//CND ISNOTRESP C 0 0 0 0

function CNDisnotresp()
{
	return !in_response;	
}

//CND ISSOUND C 1 0 0 0

function CNDissound(channelno)
{
	if ((channelno <1 ) || (channelno > MAX_CHANNELS)) return false;
    return channelActive(channelno);
}
//CND ZONE C 8 8 0 0

function CNDzone(locno1, locno2)
{

	if (loc_here()<locno1) return false;
	if (loc_here()>locno2) return false;
	return true;
}
//CND FADEOUT A 2 2 0 0

function ACCfadeout(channelno, value)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxfadeout(channelno, value);
}
//CND CLEAREXIT A 2 0 0 0

function ACCclearexit(wordno)
{
	if ((wordno >= NUM_CONNECTION_VERBS) || (wordno< 0 )) return;
	setConnection(loc_here(),wordno, -1);
}
//CND WHATOX2 A 1 0 0 0

function ACCwhatox2(flagno)
{	
	var auxNoun = getFlag(FLAG_NOUN1);
	var auxAdj = getFlag(FLAG_ADJECT1);
	setFlag(FLAG_NOUN1, getFlag(FLAG_NOUN2));
	setFlag(FLAG_ADJECT1, getFlag(FLAG_ADJECT2));
	var whatox2found = getReferredObject();
	setFlag(flagno,whatox2found);
	setFlag(FLAG_NOUN1, auxNoun);
	setFlag(FLAG_ADJECT1, auxAdj);
}
//CND COMMAND A 2 0 0 0

function ACCcommand(value)
{
	if (value) {$('.input').show();$('.input').focus();} else $('.input').hide();
}
//CND TITLE A 14 0 0 0

function ACCtitle(writeno)
{
	document.title = writemessages[writeno];
}
//CND LE C 1 2 0 0

function CNDle(flagno, valor)
{
	return (getFlag(flagno) <= valor);
}
//CND WARNINGS A 2 0 0 0

function ACCwarnings(value)
{
	if (value) showWarnings = true; else showWarnings = false;
}
//CND BCLEAR A 1 2 0 0

function ACCbclear(flagno, bitno)
{
	if (bitno>=32) return;
	setFlag(flagno, bitclear(getFlag(flagno), bitno));
}
//CND DIV A 1 2 0 0

function ACCdiv(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) / valor));
}
//CND OBJAT A 9 1 0 0

function ACCobjat(locno, flagno)
{
	setFlag(flagno, getObjectCountAt(locno));
}
//CND SILENCE A 2 0 0 0

function ACCsilence(channelno)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;
	sfxstop(channelno);
}
//CND SETEXIT A 2 2 0 0

function ACCsetexit(value, locno)
{
	if (value < NUM_CONNECTION_VERBS) setConnection(loc_here(), value, locno);
}
//CND EXITS A 8 5 0 0

function ACCexits(locno,mesno)
{
  writeText(getExitsText(locno,mesno));
}

//CND HOOK A 14 0 0 5

function ACChook(writeno)
{
	h_code(writemessages[writeno]);
}
//CND RANDOMX A 1 2 0 0

function ACCrandomx(flagno, value)
{
	 setFlag(flagno, 1 + Math.floor((Math.random()*value)));
}
//CND ISNOTDONE C 0 0 0 0

function CNDisnotdone()
{
	return !CNDisdone();
}

//CND ATGE C 8 0 0 0

function CNDatge(locno)
{
	return (getFlag(FLAG_LOCATION) >= locno);
}

//CND ATLE C 8 0 0 0

function CNDatle(locno)
{
	return (getFlag(FLAG_LOCATION) <= locno);
}

//CND RESP A 0 0 0 0

function ACCresp()
{
	in_response = true;
}	

//CND ISNOTSOUND C 1 0 0 0

function CNDisnotsound(channelno)
{
  if ((channelno <1) || (channelno >MAX_CHANNELS)) return false;
  return !(CNDissound(channelno));
}
//CND ASK W 14 14 1 0

// Global vars for ASK


var inAsk = false;
var ask_responses = null;
var ask_flagno = null;



function ACCask(writeno, writenoOptions, flagno)
{
	inAsk = true;
	writeWriteMessage(writeno);
	ask_responses = getWriteMessageText(writenoOptions);
	ask_flagno = flagno;
}



// hook replacement
var old_ask_h_keydown  = h_keydown;
h_keydown  = function (event)
{
	if (inAsk)
	{
		var keyCodeAsChar = String.fromCharCode(event.keyCode).toLowerCase();
		if (ask_responses.indexOf(keyCodeAsChar)!= -1)
		{
			setFlag(ask_flagno, ask_responses.indexOf(keyCodeAsChar));
			inAsk = false;
			event.preventDefault();
            $('.input').show();
		    $('.input').focus();
		    hideBlock();
			waitKeyCallback();
		};
		return false; // if we are in ASK condact, no keypress should be considered other than ASK response
	} else return old_ask_h_keydown(event);
}

//CND MUL A 1 2 0 0

function ACCmul(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) * valor));
}
//CND NPCAT A 9 1 0 0

function ACCnpcat(locno, flagno)
{
	setFlag(flagno,getNPCCountAt(locno));
}

//CND SOFTBLOCK A 2 0 0 0

function ACCsoftblock(procno)
{
   inBlock = true;
   disableInterrupt();

   $('.block_layer').css('display','none');
   $('.block_text').html('');
   $('.block_graphics').html('');
   $('.block_layer').css('background','transparent');
   if (procno == 0 ) unblock_process ==null; else unblock_process = procno;
   $('.block_layer').css('display','block');
}
//CND LISTCONTENTS A 9 0 0 0

function ACClistcontents(locno)
{
   ACClistat(locno, locno)
}
//CND SPACE A 0 0 0 0

function ACCspace()
{
	writeText(' ');
}
//CND ISNOTMUSIC C 0 0 0 0

function CNDisnotmusic()
{
  return !CNDismusic();
}

//CND BREAK A 0 0 0 0

function ACCbreak()
{
	doall_flag = false; 
	entry_for_doall = '';
}
//CND NORESP A 0 0 0 0

function ACCnoresp()
{
	in_response = false;
}	

//CND ISMUSIC C 0 0 0 0

function CNDismusic()
{
	return (CNDissound(0));	
}

//CND ISRESP C 0 0 0 0

function CNDisresp()
{
	return in_response;	
}

//CND MOD A 1 2 0 0

function ACCmod(flagno, valor)
{
	if (valor == 0) return;
	setFlag(flagno, Math.floor(getFlag(flagno) % valor));
}
//CND FADEIN A 2 2 2 0

function ACCfadein(sfxno, channelno, times)
{
	if ((channelno <1) || (channelno >MAX_CHANNELS)) return;  //SFX channels from 1 to MAX_CHANNELS, channel 0 is for location music and can't be used here
	sfxplay(sfxno, channelno, times, 'fadein');
}
//CND WHATOX A 1 0 0 0

function ACCwhatox(flagno)
{
	var whatoxfound = getReferredObject();
	setFlag(flagno,whatoxfound);
}

//CND GETEXIT A 2 2 0 0

function ACCgetexit(value,flagno)
{
	if (value >= NUM_CONNECTION_VERBS) 
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	var locno = getConnection(loc_here(),value);
	if (locno == -1)
		{
			setFlag(flagno, NO_EXIT);
			return;
		}
	setFlag(flagno,locno);
}
//CND LOG A 14 0 0 0

function ACClog(writeno)
{
  console_log(writemessages[writeno]);
}
//CND VOLUMEVIDEO A 2 0 0 0


function ACCvolumevideo(value)
{
	if (typeof videoElement != 'undefined') 
		videoElement.volume = value  / 65535;
}

//CND OBJNOTFOUND C 2 9 0 0

function CNDobjnotfound(attrno, locno)
{
	for (var i=0;i<num_objects;i++) 
		if ((getObjectLocation(i) == locno) && (CNDonotzero(i,attrno))) {setFlag(FLAG_ESCAPE, i); return false; }

	setFlag(FLAG_ESCAPE, EMPTY_OBJECT);
	return true;
}
//CND ISMOV C 0 0 0 0

function CNDismov()
{
	if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)==EMPTY_WORD)) return true;

	if ((getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_VERB)==EMPTY_WORD)) return true;

    if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (getFlag(FLAG_NOUN1)<NUM_CONNECTION_VERBS)) return true;
    
    return false;
}

//CND YOUTUBE A 14 0 0 0

function ACCyoutube(strno)
{

	var str = '<iframe id="youtube" width="560" height="315" src="http://www.youtube.com/embed/' + writemessages[strno] + '?autoplay=1&controls=0&modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>'
	$('.graphics').removeClass('hidden');
	$('.graphics').addClass('half_graphics');
	$('.text').removeClass('all_text');
	$('.text').addClass('half_text');
	$('.graphics').html(str);
	$('#youtube').css('height','100%');
	$('#youtube').css('display','block');
	$('#youtube').css('margin-left','auto');
	$('#youtube').css('margin-right','auto');
	$('.graphics').show();
}


// Hook into location description to avoid video playing to continue playing while hidden after changing location
var old_youtube_h_description_init = h_description_init ;
var h_description_init =  function  ()
{
	if ($("#youtube").length > 0) $("#youtube").remove();	
	old_youtube_h_description_init();
}
//CND LISTSAVEDGAMES A 0 0 0 0

function ACClistsavedgames()
{
  var numberofgames = 0;
  for(var savedgames in localStorage)
  {
    gamePrefix = savedgames.substring(0,16); // takes out ngpaws_savegame_
    if (gamePrefix == "ngpaws_savegame_")
    {
      gameName = savedgames.substring(16);
      writelnText(implementTag("EXTERN|loadgame('" + gameName + "')|" + gameName));
      numberofgames++;
    }
  }
  if (numberofgames == 0) 
  {
     if (getLang()=='EN') writelnText("NO SAVED GAMES FOUND."); else writelnText("No hay ninguna partida guardada.");
  }
}

//LIB LOADGAME

function loadgame(gametitle) 	
{
    filename = gametitle;
	var json_str;
	if (filename == null) filename = prompt(getSysMessageText(SYSMESS_LOADFILE),'');
    json_str = localStorage.getItem('ngpaws_savegame_' + filename.toUpperCase());
	if (json_str)
	{
		savegame_object = JSON.parse(json_str.trim());
		restoreSaveGameObject(savegame_object);
        ACCanykey();    //Only to make ACCdesc to work
		ACCdesc();
	}
	else
	{
		writeSysMessage(SYSMESS_FILENOTFOUND);
		ACCnewline();
		done_flag = true;
	}
	focusInput();
}

//CND CHANGECSS A 0 0 0 0

function ACCchangecss(style)
{
    //TODO: Enable random name of css'
    // Obtains an array of all <link>
    // elements.
    // Select your element using indexing.
    var theme = document.getElementsByTagName('link')[0];

    // Change the value of href attribute 
    // to change the css sheet.
    if (theme.getAttribute('href') == 'css.css') {
        theme.setAttribute('href', 'css_alt.css');
    } else {
        theme.setAttribute('href', 'css.css');
    }
    //TODO: Notify the user
    //if (getLang()=='EN') writelnText("No saved games found."); else writelnText("No hay ninguna partida guardada.");
}


// This file is (C) Carlos Sanchez 2014, released under the MIT license


// IMPORTANT: Please notice this file must be encoded with the same encoding the index.html file is, so the "normalize" function works properly.
//            As currently the ngpwas compiler generates utf-8, and the index.html is using utf-8 also, this file must be using that encoding.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                         Auxiliary functions                                            //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// General functions
String.prototype.rights= function(n){
    if (n <= 0)
       return "";
    else if (n > String(this).length)
       return this;
    else {
       var iLen = String(this).length;
       return String(this).substring(iLen, iLen - n);
    }
}


String.prototype.firstToLower= function()
{
	return  this.charAt(0).toLowerCase() + this.slice(1);	
}


// Returns true if using Internet Explorer 9 or below, where some features are not supported
function isBadIE () {
  var myNav = navigator.userAgent.toLowerCase();
  if (myNav.indexOf('msie') == -1) return false;
  ieversion =  parseInt(myNav.split('msie')[1]);
  return (ieversion<10);
}


function runningLocal()
{
	return (window.location.protocol == 'file:');
}


// Levenshtein function

function getLevenshteinDistance (a, b)
{
  if(a.length == 0) return b.length; 
  if(b.length == 0) return a.length; 
 
  var matrix = [];
 
  // increment along the first column of each row
  var i;
  for(i = 0; i <= b.length; i++){
    matrix[i] = [i];
  }
 
  // increment each column in the first row
  var j;
  for(j = 0; j <= a.length; j++){
    matrix[0][j] = j;
  }
 
  // Fill in the rest of the matrix
  for(i = 1; i <= b.length; i++){
    for(j = 1; j <= a.length; j++){
      if(b.charAt(i-1) == a.charAt(j-1)){
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
                                Math.min(matrix[i][j-1] + 1, // insertion
                                         matrix[i-1][j] + 1)); // deletion
      }
    }
  }
 
  return matrix[b.length][a.length];
};

// waitKey helper for all key-wait condacts

function waitKey(callbackFunction)
{
	waitkey_callback_function.push(callbackFunction);
	showAnykeyLayer();
}

function waitKeyCallback()
{
 	var callback = waitkey_callback_function.pop();
	if ( callback ) callback();
	if (describe_location_flag) descriptionLoop();  		
}


// Check DOALL entry

function skipdoall(entry)
{
	return  ((doall_flag==true) && (entry_for_doall!='') && (current_process==process_in_doall) && (entry_for_doall > entry));
}

// Dynamic attribute use functions
function getNextFreeAttribute()
{
	var value = nextFreeAttr;
	nextFreeAttr++;
	return value;
}


// Gender functions

function getSimpleGender(objno)  // Simple, for english
{
 	isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	if (isPlural) return "P";
 	isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	if (isFemale) return "F";
 	isMale = objectIsAttr(objno, ATTR_MALE);
 	if (isMale) return "M";
    return "N"; // Neuter
}

function getAdvancedGender(objno)  // Complex, for spanish
{
 	var isPlural = objectIsAttr(objno, ATTR_PLURALNAME);
 	var isFemale = objectIsAttr(objno, ATTR_FEMALE);
 	var isMale = objectIsAttr(objno, ATTR_MALE);

 	if (!isPlural) 
 	{
	 	if (isFemale) return "F";
	 	if (isMale) return "M";
	    return "N"; // Neuter
 	}
 	else
 	{
	 	if (isFemale) return "PF";
	 	if (isMale) return "PM";
	 	return "PN"; // Neuter plural
 	}

}

function getLang()
{
	var value = bittest(getFlag(FLAG_PARSER_SETTINGS),5);
	if (value) return "ES"; else return "EN";
}

function getObjectFixArticles(objno)
{
	var object_text = getObjectText(objno);
	var object_words = object_text.split(' ');
	if (object_words.length == 1) return object_text;
	var candidate = object_words[0];
	object_words.splice(0, 1);
	if (getLang()=='EN')
	{
		if ((candidate!='an') && (candidate!='a') && (candidate!='some')) return object_text;
		return 'the ' + object_words.join(' ');
	}
	else
	{
		if ( (candidate!='un') && (candidate!='una') && (candidate!='unos') && (candidate!='unas') && (candidate!='alguna') && (candidate!='algunos') && (candidate!='algunas') && (candidate!='algun')) return object_text;
		var gender = getAdvancedGender(objno);
		if (gender == 'F') return 'la ' + object_words.join(' ');
		if (gender == 'M') return 'el ' + object_words.join(' ');
		if (gender == 'N') return 'el ' + object_words.join(' ');
		if (gender == 'PF') return 'las ' + object_words.join(' ');
		if (gender == 'PM') return 'los ' + object_words.join(' ');
		if (gender == 'PN') return 'los ' + object_words.join(' ');
	}	


}



// JS level log functions
function console_log(string)
{
	if (typeof console != "undefined") console.log(string);
}


// Resources functions
function getResourceById(resource_type, id)
{
	for (var i=0;i<resources.length;i++)
	 if ((resources[i][0] == resource_type) && (resources[i][1]==id)) return resources[i][2];
	return false; 
}

// Flag read/write functions
function getFlag(flagno)
{
	 return flags[flagno];
}

function setFlag(flagno, value)
{
	 flags[flagno] = value;
}

// Locations functions
function loc_here()  // Returns current location, avoid direct use of flags
{
	 return getFlag(FLAG_LOCATION);
}


// Connections functions

function setConnection(locno1, dirno, locno2)
{
	connections[locno1][dirno] = locno2;
}

function getConnection(locno, dirno)
{
	return connections[locno][dirno];
}

// Objects text functions

function getObjectText(objno)
{
	return filterText(objects[objno]);
}


// Message text functions
function getMessageText(mesno)
{
	return filterText(messages[mesno]);
}

function getSysMessageText(sysno)
{
	return filterText(sysmessages[sysno]);
}

function getWriteMessageText(writeno)
{
	return filterText(writemessages[writeno]);
}

function getExitsText(locno,mesno)
{
  if ( locno === undefined ) return ''; // game hasn't fully initialised yet
  if ((getFlag(FLAG_LIGHT) == 0) || ((getFlag(FLAG_LIGHT) != 0) && lightObjectsPresent()))
  {
  		var exitcount = 0;
  		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1) exitcount++;
      if (exitcount)
      {
    		var message = getMessageText(mesno);
    		var exitcountprogress = 0;
    		for (i=0;i<NUM_CONNECTION_VERBS;i++) if (getConnection(locno, i) != -1)
    		{ 
    			exitcountprogress++;
    			message += getMessageText(mesno + 2 + i);
    			if (exitcountprogress == exitcount) message += getSysMessageText(SYSMESS_LISTEND);
    			if (exitcountprogress == exitcount-1) message += getSysMessageText(SYSMESS_LISTLASTSEPARATOR);
    			if (exitcountprogress <= exitcount-2) message += getSysMessageText(SYSMESS_LISTSEPARATOR);
  		  }
  		  return message;
      } else return getMessageText(mesno + 1);
  } else return getMessageText(mesno + 1);
}


// Location text functions
function getLocationText(locno)
{
	return  filterText(locations[locno]);
}



// Output processing functions
function implementTag(tag)
{
	tagparams = tag.split('|');
	for (var tagindex=0;tagindex<tagparams.length-1;tagindex++) tagparams[tagindex] = tagparams[tagindex].trim();
	if (tagparams.length == 0) {writeWarning(STR_INVALID_TAG_SEQUENCE_EMPTY); return ''}

	var resolved_hook_value = h_sequencetag(tagparams);
	if (resolved_hook_value!='') return resolved_hook_value;

	switch(tagparams[0].toUpperCase())
	{
		case 'URL': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					return '<a target="newWindow" href="' + tagparams[1]+ '">' + tagparams[2] + '</a>'; // Note: _blank would get the underscore character replaced by current selected object so I prefer to use a different target name as most browsers will open a new window
					break;
		case 'CLASS': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span class="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'STYLE': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'INK': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'PAPER': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					  return '<span style="background-color:' + tagparams[1]+ '">' + tagparams[2] + '</span>';
					  break;
		case 'OBJECT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objects[getFlag(tagparams[1])]) return getObjectFixArticles(getFlag(tagparams[1])); else return '';
					   break;
		case 'WEIGHT': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(objectsWeight[getFlag(tagparams[1])]) return objectsWeight[getFlag(tagparams[1])]; else return '';
					   break;
		case 'OLOCATION': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					      if(objectsLocation[getFlag(tagparams[1])]) return objectsLocation[getFlag(tagparams[1])]; else return '';
					      break;
		case 'MESSAGE':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(messages[getFlag(tagparams[1])]) return getMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'SYSMESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(sysmessages[getFlag(tagparams[1])]) return getSysMessageText(getFlag(tagparams[1])); else return '';
					   break;
		case 'LOCATION':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   if(locations[getFlag(tagparams[1])]) return getLocationText(getFlag(tagparams[1])); else return '';
		case 'EXITS':if (tagparams.length != 3 ) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return getExitsText(/^@\d+/.test(tagparams[1]) ? getFlag(tagparams[1].substr(1)) : tagparams[1],parseInt(tagparams[2],10));
					   break;
		case 'PROCESS':if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   callProcess(tagparams[1]);
					   return "";
					   break;
		case 'ACTION': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					   return '<a href="type: ' + tagparams[1] + '" onmouseup="orderEnteredLoop(\'' + tagparams[1]+ '\');return false;">' + tagparams[2] + '</a>';
					   break;
		case 'RESTART': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="restart()">' + tagparams[1] + '</a>';
					    break;
		case 'EXTERN': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					    return '<a href="javascript: void(0)" onmouseup="' + tagparams[1] + ' ">' + tagparams[2] + '</a>';
					    break;
		case 'TEXTPIC': if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						var style = '';
						var post = '';
						var pre = '';
						align = tagparams[2];
						switch(align)
						{
							case 1: style = 'float:left'; break;
							case 2: style = 'float:right'; break;
							case 3: post = '<br />';
							case 4: pre='<center>';post='</center>';break;
						}
						return pre + "<img class='textpic' style='"+style+"' src='"+ RESOURCES_DIR + tagparams[1]+"' />" + post;
					    break;
		case 'HTML': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return tagparams[1];
					    break;
		case 'FLAG': if (tagparams.length != 2) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
						return getFlag(tagparams[1]);
					    break;
		case 'OREF': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
   			        if(objects[getFlag(FLAG_REFERRED_OBJECT)]) return getObjectFixArticles(getFlag(FLAG_REFERRED_OBJECT)); else return '';
					break;
		case 'TT':  
		case 'TOOLTIP':
					if (tagparams.length != 3) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};
					var title = $('<span>'+tagparams[1]+'</span>').text().replace(/'/g,"&apos;").replace(/\n/g, "&#10;");
					var text = tagparams[2];
					return "<span title='"+title+"'>"+text+"</span>";
					break;
		case 'OPRO': if (tagparams.length != 1) {return '[[[' + STR_INVALID_TAG_SEQUENCE_BADPARAMS + ']]]'};  // returns the pronoun for a given object, used for english start database
					 switch (getSimpleGender(getFlag(FLAG_REFERRED_OBJECT)))
					 {
					 	case 'M' : return "him";
					 	case "F" : return "her";
					 	case "N" : return "it";
					 	case "P" : return "them";  // plural returns them
					 }
					break;

		default : return '[[[' + STR_INVALID_TAG_SEQUENCE_BADTAG + ' : ' + tagparams[0] + ']]]';
	}
}

function processTags(text)
{
	//Apply the {} tags filtering
	var pre, post, innerTag;
	tagfilter:
	while (text.indexOf('{') != -1)
	{
		if (( text.indexOf('}') == -1 ) || ((text.indexOf('}') < text.indexOf('{'))))
		{
			writeWarning(STR_INVALID_TAG_SEQUENCE + text);
			break tagfilter;
		}
		pre = text.substring(0,text.indexOf('{'));
		var openbracketcont = 1;
		pointer = text.indexOf('{') + 1;
		innerTag = ''
		while (openbracketcont>0)
		{
			if (text.charAt(pointer) == '{') openbracketcont++;
			if (text.charAt(pointer) == '}') openbracketcont--;
			if ( text.length <= pointer )
			{
				writeWarning(STR_INVALID_TAG_SEQUENCE + text);
				break tagfilter;
			}
			innerTag = innerTag + text.charAt(pointer);
			pointer++;
		}
		innerTag = innerTag.substring(0,innerTag.length - 1);
		post = text.substring(pointer);
		if (innerTag.indexOf('{') != -1 ) innerTag = processTags(innerTag); 
		innerTag = implementTag(innerTag);
		text = pre + innerTag + post;
	}
	return text;
}

function filterText(text)
{
	// ngPAWS sequences
	text = processTags(text);


	// Superglus sequences (only \n remains)
    text = text.replace(/\n/g, STR_NEWLINE);

	// PAWS sequences (only underscore)
	objno = getFlag(FLAG_REFERRED_OBJECT);
	if ((objno != EMPTY_OBJECT) && (objects[objno]))	text = text.replace(/_/g,objects[objno].firstToLower()); else text = text.replace(/_/g,'');
	text = text.replace(/¬/g,' ');

	return text;
}


// Text Output functions
function writeText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	text = h_writeText(text); // hook
	$('.text').append(text);
	$('.text').scrollTop($('.text')[0].scrollHeight);
	addToTranscript(text);
	if (!skipAutoComplete) addToAutoComplete(text);
	focusInput();
}

function writeWarning(text)
{
	if (showWarnings) writeText(text)
}

function addToTranscript(text)
{
	transcript = transcript + text;		
}

function writelnText(text, skipAutoComplete)
{
	if (typeof skipAutoComplete === 'undefined') skipAutoComplete = false;
	writeText(text + STR_NEWLINE, skipAutoComplete);
}

function writeMessage(mesno)
{
	if (messages[mesno]!=null) writeText(getMessageText(mesno)); else writeWarning(STR_NEWLINE + STR_WRONG_MESSAGE + ' [' + mesno + ']');
}

function writeSysMessage(sysno)
{
		if (sysmessages[sysno]!=null) writeText(getSysMessageText(sysno)); else writeWarning(STR_NEWLINE + STR_WRONG_SYSMESS + ' [' + sysno + ']');
		$(".text").scrollTop($(".text")[0].scrollHeight);
}

function writeWriteMessage(writeno)
{
		writeText(getWriteMessageText(writeno)); 
}

function writeObject(objno)
{
	writeText(getObjectText(objno));
}

function clearTextWindow()
{
	$('.text').empty();
}


function clearInputWindow()
{
	$('.prompt').val('');
}


function writeLocation(locno)
{
	if (locations[locno]!=null) writeText(getLocationText(locno) + STR_NEWLINE); else writeWarning(STR_NEWLINE + STR_WRONG_LOCATION + ' [' + locno + ']');
}

// Screen control functions

function clearGraphicsWindow()
{
	$('.graphics').empty();	
}


function clearScreen()
{
	clearInputWindow();
	clearTextWindow();
	clearGraphicsWindow();
}

function copyOrderToTextWindow(player_order)
{

	last_player_orders.push(player_order);
	last_player_orders_pointer = 0;
	clearInputWindow();
	writelnText(STR_PROMPT_START + player_order + STR_PROMPT_END, false);
}

function get_prev_player_order()
{
	if (!last_player_orders.length) return '';
	var last = last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];
	if (last_player_orders_pointer < last_player_orders.length - 1) last_player_orders_pointer++;
	return last;
}

function get_next_player_order()
{
	if (!last_player_orders.length || last_player_orders_pointer == 0) return '';
	last_player_orders_pointer--;
	return last_player_orders[last_player_orders.length - 1 - last_player_orders_pointer];

}



// Graphics functions


function hideGraphicsWindow()
{
		$('.text').removeClass('half_text');
		$('.text').addClass('all_text');
		$('.graphics').removeClass('half_graphics');
		$('.graphics').addClass('hidden');
		if ($('.location_picture')) $('.location_picture').remove();
}



function drawPicture(picno)  
{
	var pictureDraw = false;
	if (graphicsON) 
	{
		if ((isDarkHere()) && (!lightObjectsPresent())) picno = 0;
		var filename = getResourceById(RESOURCE_TYPE_IMG, picno);
		if (filename)
		{
			$('.graphics').removeClass('hidden');
			$('.graphics').addClass('half_graphics');
			$('.text').removeClass('all_text');
			$('.text').addClass('half_text');
			$('.graphics').html('<img alt="" class="location_picture" src="' +  filename + '" />');
			$('.location_picture').css('height','100%');
			pictureDraw = true;
		}
	}

	if (!pictureDraw) hideGraphicsWindow();
}




function clearPictureAt() // deletes all pictures drawn by "pictureAT" condact
{
	$.each($('.graphics img'), function () {
		if ($(this)[0].className!= 'location_picture') $(this).remove();
	});

}

// Turns functions

function incTurns()
{
	turns = getFlag(FLAG_TURNS_LOW) + 256 * getFlag(FLAG_TURNS_HIGH)  + 1;
	setFlag(FLAG_TURNS_LOW, turns % 256);
	setFlag(FLAG_TURNS_HIGH, Math.floor(turns / 256));
}

// input box functions

function disableInput()
{
	$(".input").prop('disabled', true); 
}

function enableInput()
{
	$(".input").prop('disabled', false); 
}

function focusInput()
{
	$(".prompt").focus();
	timeout_progress = 0;
}

// Object default attributes functions

function objectIsNPC(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_NPC);
}

function objectIsLight(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_LIGHT);
}

function objectIsWearable(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_WEARABLE);
}

function objectIsContainer(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_CONTAINER);
}

function objectIsSupporter(objno)
{
	if (objno > last_object_number) return false;
	return bittest(getObjectLowAttributes(objno), ATTR_SUPPORTER);
}


function objectIsAttr(objno, attrno)
{
	if (attrno > 63) return false;
	var attrs = getObjectLowAttributes(objno);
	if (attrno > 31)
	{
		attrs = getObjectHighAttributes(objno);
		attrno = attrno - 32;
	}
	return bittest(attrs, attrno);
}

function isAccesibleContainer(objno)
{
	if (objectIsSupporter(objno)) return true;   // supporter
	if ( objectIsContainer(objno) && !objectIsAttr(objno, ATTR_OPENABLE) ) return true;  // No openable container
	if ( objectIsContainer(objno) && objectIsAttr(objno, ATTR_OPENABLE) && objectIsAttr(objno, ATTR_OPEN)  )  return true;  // No openable & open container
	return false;
}

//Objects and NPC functions

function findMatchingObject(locno)
{
	for (var i=0;i<num_objects;i++)
		if ((locno==-1) || (getObjectLocation(i) == locno))
		 if (((objectsNoun[i]) == getFlag(FLAG_NOUN1)) && (((objectsAdjective[i]) == EMPTY_WORD) || ((objectsAdjective[i]) == getFlag(FLAG_ADJECT1))))  return i;
	return EMPTY_OBJECT;
}

function getReferredObject()
{
	var objectfound = EMPTY_OBJECT; 
	refobject_search: 
	{
		object_id = findMatchingObject(LOCATION_CARRIED);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(LOCATION_WORN);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(loc_here());
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	

		object_id = findMatchingObject(-1);
		if (object_id != EMPTY_OBJECT)	{objectfound = object_id; break refobject_search;}	
	}
	return objectfound;
}


function getObjectLowAttributes(objno)
{
	return objectsAttrLO[objno];
}

function getObjectHighAttributes(objno)
{
	return objectsAttrHI[objno]
}


function setObjectLowAttributes(objno, attrs)
{
	objectsAttrLO[objno] = attrs;
}

function setObjectHighAttributes(objno, attrs)
{
	objectsAttrHI[objno] = attrs;
}


function getObjectLocation(objno)
{
	if (objno > last_object_number) 
		writeWarning(STR_INVALID_OBJECT + ' [' + objno + ']');
	return objectsLocation[objno];
}

function setObjectLocation(objno, locno)
{
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) - 1);
	objectsLocation[objno] = locno;
	if (objectsLocation[objno] == LOCATION_CARRIED) setFlag(FLAG_OBJECTS_CARRIED_COUNT, getFlag(FLAG_OBJECTS_CARRIED_COUNT) + 1);
}



// Sets all flags associated to  referred object by current LS  
function setReferredObject(objno) 
{
	if (objno == EMPTY_OBJECT)
	{
		setFlag(FLAG_REFERRED_OBJECT, EMPTY_OBJECT);
		setFlag(FLAG_REFERRED_OBJECT_LOCATION, LOCATION_NONCREATED);
		setFlag(FLAG_REFERRED_OBJECT_WEIGHT, 0);
		setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, 0);
		setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, 0);
		return;
	}
	setFlag(FLAG_REFERRED_OBJECT, objno);
	setFlag(FLAG_REFERRED_OBJECT_LOCATION, getObjectLocation(objno));
	setFlag(FLAG_REFERRED_OBJECT_WEIGHT, getObjectWeight(objno));
	setFlag(FLAG_REFERRED_OBJECT_LOW_ATTRIBUTES, getObjectLowAttributes(objno));
	setFlag(FLAG_REFERRED_OBJECT_HIGH_ATTRIBUTES, getObjectHighAttributes(objno));

}


function getObjectWeight(objno) 
{
	var weight = objectsWeight[objno];
	if ( ((objectIsContainer(objno)) || (objectIsSupporter(objno))) && (weight!=0)) // Container with zero weigth are magic boxes, anything you put inside weigths zero
  		weight = weight + getLocationObjectsWeight(objno);
	return weight;
}


function getLocationObjectsWeight(locno) 
{
	var weight = 0;
	for (var i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			objweight = objectsWeight[i];
			weight += objweight;
			if (objweight > 0)
			{
				if (  (objectIsContainer(i)) || (objectIsSupporter(i)) )
				{	
					weight += getLocationObjectsWeight(i);
				}
			}
		}
	}
	return weight;
}

function getObjectCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
	{
		if (getObjectLocation(i) == locno) 
		{
			attr = getObjectLowAttributes(i);
			if (!bittest(getFlag(FLAG_PARSER_SETTINGS),3)) count ++;  // Parser settings say we should include NPCs as objects
			 else if (!objectIsNPC(i)) count++;     // or object is not an NPC
		}
	}
	return count;
}


function getObjectCountAtWithAttr(locno, attrnoArray) 
{
	var count = 0;
	for (var i=0;i<num_objects;i++)
		if (getObjectLocation(i) == locno)  
			for (var j=0;j<attrnoArray.length;j++)
				if (objectIsAttr(i, attrnoArray[j])) count++;
	return count;
}


function getNPCCountAt(locno) 
{
	var count = 0;
	for (i=0;i<num_objects;i++)
		if ((getObjectLocation(i) == locno) &&  (objectIsNPC(i))) count++;
	return count;
}


// Location light function

function lightObjectsAt(locno) 
{
	return getObjectCountAtWithAttr(locno, [ATTR_LIGHT]) > 0;
}


function lightObjectsPresent() 
{
  if (lightObjectsAt(LOCATION_CARRIED)) return true;
  if (lightObjectsAt(LOCATION_WORN)) return true;
  if (lightObjectsAt(loc_here())) return true;
  return false;
}


function isDarkHere()
{
	return (getFlag(FLAG_LIGHT) != 0);
}

// Sound functions


function preloadsfx()
{
	for (var i=0;i<resources.length;i++)
	 	if (resources[i][0] == 'RESOURCE_TYPE_SND') 
	 	{
	 		var fileparts = resources[i][2].split('.');
			var basename = fileparts[0];
			var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] , preload: true} );
	 	}
}

function sfxplay(sfxno, channelno, times, method)
{

	if (!soundsON) return;
	if ((channelno <0) || (channelno >MAX_CHANNELS)) return;
	if (times == 0) times = -1; // more than 4000 million times
	var filename = getResourceById(RESOURCE_TYPE_SND, sfxno);
	if (filename)
	{
		var fileparts = filename.split('.');
		var basename = fileparts[0];
		var mySound = new buzz.sound( basename, {  formats: [ "ogg", "mp3" ] });
		if (soundChannels[channelno]) soundChannels[channelno].stop();
		soundLoopCount[channelno] = times;
		mySound.bind("ended", function(e) {
			for (sndloop=0;sndloop<MAX_CHANNELS;sndloop++)
				if (soundChannels[sndloop] == this)
				{
					if (soundLoopCount[sndloop]==-1) {this.play(); return }
					soundLoopCount[sndloop]--;
					if (soundLoopCount[sndloop] > 0) {this.play(); return }
					sfxstop(sndloop);
					return;
				}
		});
		soundChannels[channelno] = mySound;
		if (method=='play')	mySound.play(); else mySound.fadeIn(2000);
	}
}

function playLocationMusic(locno)
{
	if (soundsON) 
		{
			sfxstop(0);
			sfxplay(locno, 0, 0, 'play');
		}
}

function musicplay(musicno, times)  
{
	sfxplay(musicno, 0, times);
}

function channelActive(channelno)
{
	if (soundChannels[channelno]) return true; else return false;
}


function sfxstopall() 
{
	for (channelno=0;channelno<MAX_CHANNELS;channelno++) sfxstop(channelno);

}


function sfxstop(channelno)
{
	if (soundChannels[channelno]) 
		{
			soundChannels[channelno].unbind('ended');
			soundChannels[channelno].stop();
			soundChannels[channelno] = null;
		}
}

function sfxvolume(channelno, value)
{
	if (soundChannels[channelno]) soundChannels[channelno].setVolume(Math.floor( value * 100 / 65535)); // Inherited volume condact uses a number among 0 and 65535, buzz library uses 0-100.
}

function isSFXPlaying(channelno)
{
	if (!soundChannels[channelno]) return false;
	return true;
}


function sfxfadeout(channelno, value)
{
	if (!soundChannels[channelno]) return;
	soundChannels[channelno].fadeOut(value, function() { sfxstop(channelno) });
}

// *** Process functions ***

function callProcess(procno)
{
	if (inEND) return;
	current_process = procno;
	var prostr = procno.toString(); 
	while (prostr.length < 3) prostr = "0" + prostr;
	if (procno==0) in_response = true;
	if (doall_flag && in_response) done_flag = false;
	if (!in_response) done_flag = false;
	h_preProcess(procno);
    eval("pro" + prostr + "()");
	h_postProcess(procno);
	if (procno==0) in_response = false;
}

// Bitwise functions

function bittest(value, bitno)
{
	mask = 1 << bitno;
	return ((value & mask) != 0);
}

function bitset(value, bitno)
{

	mask = 1 << bitno;
	return value | mask;
}

function bitclear(value, bitno)
{
	mask = 1 << bitno;
	return value & (~mask);
}


function bitneg(value, bitno) 
{
	mask = 1 << bitno;
	return value ^ mask;

}

// Savegame functions
function getSaveGameObject()
{
	var savegame_object = new Object();
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	savegame_object.flags = flags.slice();
	savegame_object.objectsLocation = objectsLocation.slice();
	savegame_object.objectsWeight = objectsWeight.slice();
	savegame_object.objectsAttrLO = objectsAttrLO.slice();
	savegame_object.objectsAttrHI = objectsAttrHI.slice();
	savegame_object.connections = connections.slice();
	savegame_object.last_player_orders = last_player_orders.slice();
	savegame_object.last_player_orders_pointer = last_player_orders_pointer;
	savegame_object.transcript = transcript;
	savegame_object = h_saveGame(savegame_object);
	return savegame_object;
}

function restoreSaveGameObject(savegame_object)
{
	flags = savegame_object.flags;
	// Notice that slice() is used to make sure a copy of each array is assigned to the object, no the arrays themselves
	objectsLocation = savegame_object.objectsLocation.slice();
	objectsWeight = savegame_object.objectsWeight.slice();
	objectsAttrLO = savegame_object.objectsAttrLO.slice();
	objectsAttrHI = savegame_object.objectsAttrHI.slice();
	connections = savegame_object.connections.slice();
	last_player_orders = savegame_object.last_player_orders.slice();
	last_player_orders_pointer = savegame_object.last_player_orders_pointer;
	transcript = savegame_object.transcript;
	h_restoreGame(savegame_object);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        The parser                                                      //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function loadPronounSufixes()
{

    var swapped;

	for (var j=0;j<vocabulary.length;j++) if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_PRONOUN)
			 pronoun_suffixes.push(vocabulary[j][VOCABULARY_WORD]);
	// Now sort them so the longest are first, so you rather replace SELOS in (COGESELOS=>COGE SELOS == >TAKE THEM) than LOS (COGESELOS==> COGESE LOS ==> TAKExx THEM) that woul not be understood (COGESE is not a verb, COGE is)
    do {
        swapped = false;
        for (var i=0; i < pronoun_suffixes.length-1; i++) 
        {
            if (pronoun_suffixes[i].length < pronoun_suffixes[i+1].length) 
            {
                var temp = pronoun_suffixes[i];
                pronoun_suffixes[i] = pronoun_suffixes[i+1];
                pronoun_suffixes[i+1] = temp;
                swapped = true;
            }
        }
    } while (swapped);
}


function findVocabulary(word, forceDisableLevenshtein)  
{
	// Pending: in general this function is not very efficient. A solution where the vocabulary array is sorted by word so the first search can be binary search
	//          and possible typos are precalculated, so the distance is a lookup table instead of a function, would be much more efficient. On the other hand,
	//          the current solution is fast enough with a 1000+ words game that I don't consider improving this function to have high priority now.

	// Search word in vocabulary
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_WORD] == word)
			 return vocabulary[j];

	if (forceDisableLevenshtein) return null;

	if (word.length <=4) return null; // Don't try to fix typo for words with less than 5 length

	if (bittest(getFlag(FLAG_PARSER_SETTINGS), 8)) return null; // If matching is disabled, we won't try to use levhenstein distance

	// Search words in vocabulary with a Levenshtein distance of 1
	var distance2_match = null;
	for (var k=0;k<vocabulary.length;k++)
	{
		if ([WORDTYPE_VERB,WORDTYPE_NOUN,WORDTYPE_ADJECT,WORDTYPE_ADVERB].indexOf(vocabulary[k][VOCABULARY_TYPE])  != -1 )
		{
			var distance = getLevenshteinDistance(vocabulary[k][VOCABULARY_WORD], word);
			if ((!distance2_match) && (distance==2)) distance2_match = vocabulary[k]; // Save first word with distance=2, in case we don't find any word with distance 1
			if (distance <= 1) return vocabulary[k];
		}
	} 

	// If we found any word with distance 2, return it, only if word was at least 7 characters long
	if ((distance2_match) &&  (word.length >6)) return distance2_match;

	// Word not found
	return null;
}

function normalize(player_order)   
// Removes accented characters and makes sure every sentence separator (colon, semicolon, quotes, etc.) has one space before and after. Also, all separators are converted to comma
{
	var originalchars = 'áéíóúäëïöüâêîôûàèìòùÁÉÍÓÚÄËÏÖÜÂÊÎÔÛÀÈÌÒÙ';
	var i;
	var output = '';
	var pos;

	for (i=0;i<player_order.length;i++) 
	{
		pos = originalchars.indexOf(player_order.charAt(i));
		if (pos!=-1) output = output + "aeiou".charAt(pos % 5); else 
		{
			ch = player_order.charAt(i);
				if ((ch=='.') || (ch==',') || (ch==';') || (ch=='"') || (ch=='\'') || (ch=='«') || (ch=='»')) output = output + ' , '; else output = output + player_order.charAt(i);
		}

	}
	return output;
}

function toParserBuffer(player_order)  // Converts a player order in a list of sentences separated by dot.
{
     player_order = normalize(player_order);
     player_order = player_order.toUpperCase();
    
	 var words = player_order.split(' ');
	 for (var q=0;q<words.length;q++)
	 {
	 	words[q] = words[q].trim();
	 	if  (words[q]!=',')
	 	{
	 		words[q] = words[q].trim();
	 		foundWord = findVocabulary(words[q], false);
	 		if (foundWord)
	 		{
	 			if (foundWord[VOCABULARY_TYPE]==WORDTYPE_CONJUNCTION)
	 			{
	 			words[q] = ','; // Replace conjunctions with commas
		 		} 
	 		}
	 	}
	 }

	 var output = '';
	 for (q=0;q<words.length;q++)
	 {
	 	if (words[q] == ',') output = output + ','; else output = output + words[q] + ' ';
	 }
	 output = output.replace(/ ,/g,',');
	 output = output.trim();
	 player_order_buffer = output;
}

function getSentencefromBuffer()
{
	var sentences = player_order_buffer.split(',');
	var result = sentences[0];
	sentences.splice(0,1);
	player_order_buffer = sentences.join();
	return result;
}

function processPronounSufixes(words)  
{
	// This procedure will split pronominal sufixes into separated words, so COGELA will become COGE LA at the end, and work exactly as TAKE IT does.
	// it's only for spanish so if lang is english then it makes no changes
	if (getLang() == 'EN') return words;
	var verbFound = false;
	if (!bittest(getFlag(FLAG_PARSER_SETTINGS),0)) return words;  // If pronoun sufixes inactive, just do nothing
	// First, we clear the word list from any match with pronouns, cause if we already have something that matches pronouns, probably is just concidence, like in COGE LA LLAVE
	var filtered_words = [];
	for (var q=0;q < words.length;q++)
	{
		foundWord = findVocabulary(words[q], false);
		if (foundWord) 
			{
				if (foundWord[VOCABULARY_TYPE] != WORDTYPE_PRONOUN) filtered_words[filtered_words.length] = words[q];
			}
			else filtered_words[filtered_words.length] = words[q];
	}
	words = filtered_words;

	// Now let's start trying to get sufixes
	new_words = [];
	for (var k=0;k < words.length;k++)
	{
		words[k] = words[k].trim();
		foundWord = findVocabulary(words[k], true); // true to disable Levenshtein distance applied
		if (foundWord) if (foundWord[VOCABULARY_TYPE] == WORDTYPE_VERB) verbFound = true;  // If we found a verb, we don't look for pronoun sufixes, as they have to come together with verb
		suffixFound = false;
		pronunsufix_search:
		for (var l=0;(l<pronoun_suffixes.length) && (!suffixFound) && (!verbFound);l++)
		{

			if (pronoun_suffixes[l] == words[k].rights(pronoun_suffixes[l].length))
			{
				var verb_part = words[k].substring(0,words[k].length - pronoun_suffixes[l].length);
				var checkWord = findVocabulary(verb_part, false);
				if ((!checkWord)  || (checkWord[VOCABULARY_TYPE] != WORDTYPE_VERB))  // If the part before the supposed-to-be pronoun sufix is not a verb, then is not a pronoun sufix
				{
					new_words.push(words[k]);	
					continue pronunsufix_search;
				}
				new_words.push(verb_part);  // split the word in two parts: verb + pronoun. Since that very moment it works like in english (COGERLO ==> COGER LO as of TAKE IT)
				new_words.push(pronoun_suffixes[l]);
				suffixFound = true;
				verbFound = true;
			}
		}
		if (!suffixFound) new_words.push(words[k]);
	}
	return new_words;
}

function getLogicSentence()
{
	parser_word_found = false; ;
	aux_verb = -1;
	aux_noun1 = -1;
	aux_adject1 = -1;
	aux_adverb = -1;
	aux_pronoun = -1
	aux_pronoun_adject = -1
	aux_preposition = -1;
	aux_noun2 = -1;
	aux_adject2 = -1;
	initializeLSWords();
	SL_found = false;

	var order = getSentencefromBuffer();
	setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS),1)); // Initialize flag that says an unknown word was found in the sentence


	words = order.split(" ");
	words = processPronounSufixes(words);
	wordsearch_loop:
	for (var i=0;i<words.length;i++)
	{
		original_word = currentword = words[i];
		if (currentword.length>10) currentword = currentword.substring(0,MAX_WORD_LENGHT);
		foundWord = findVocabulary(currentword, false);
		if (foundWord)
		{
			wordtype = foundWord[VOCABULARY_TYPE];
			word_id = foundWord[VOCABULARY_ID];

			switch (wordtype)
			{
				case WORDTYPE_VERB: if (aux_verb == -1)  aux_verb = word_id; 
				        			break;

				case WORDTYPE_NOUN: if (aux_noun1 == -1) aux_noun1 = word_id; else if (aux_noun2 == -1) aux_noun2 = word_id;
									break;

				case WORDTYPE_ADJECT: if (aux_adject1 == -1) aux_adject1 = word_id; else if (aux_adject2 == -1) aux_adject2 = word_id;
									  break;

				case WORDTYPE_ADVERB: if (aux_adverb == -1) aux_adverb = word_id;
				        			  break;

				case WORDTYPE_PRONOUN: if (aux_pronoun == -1) 
											{
												aux_pronoun = word_id;
												if ((previous_noun != EMPTY_WORD) && (aux_noun1 == -1))
												{
													aux_noun1 = previous_noun;
													if (previous_adject != EMPTY_WORD) aux_adject1 = previous_adject;
												}
											}

				        			   break;

				case WORDTYPE_CONJUNCTION: break wordsearch_loop; // conjunction or nexus. Should not appear in this function, just added for security
				
				case WORDTYPE_PREPOSITION: if (aux_preposition == -1) aux_preposition = word_id;
										   if (aux_noun1!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),2));  // Set bit that determines that a preposition word was found after first noun
										   break;
			}

			// Nouns that can be converted to verbs
			if ((aux_noun1!=-1) && (aux_verb==-1) && (aux_noun1 < NUM_CONVERTIBLE_NOUNS))
			{
				aux_verb = aux_noun1;
				aux_noun1 = -1;
			}

			if ((aux_verb==-1) && (aux_noun1!=-1) && (previous_verb!=EMPTY_WORD)) aux_verb = previous_verb;  // Support "TAKE SWORD AND SHIELD" --> "TAKE WORD AND TAKE SHIELD"

			if ((aux_verb!=-1) || (aux_noun1!=-1) || (aux_adject1!=-1 || (aux_preposition!=-1) || (aux_adverb!=-1))) SL_found = true;



		} else if (aux_verb!=-1) setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS),1));  // Set bit that determines that an unknown word was found after the verb
	}

	if (SL_found)
	{
		if (aux_verb != -1) setFlag(FLAG_VERB, aux_verb);
		if (aux_noun1 != -1) setFlag(FLAG_NOUN1, aux_noun1);
		if (aux_adject1 != -1) setFlag(FLAG_ADJECT1, aux_adject1);
		if (aux_adverb != -1) setFlag(FLAG_ADVERB, aux_adverb);
		if (aux_pronoun != -1) 
			{
				setFlag(FLAG_PRONOUN, aux_noun1);
				setFlag(FLAG_PRONOUN_ADJECT, aux_adject1);
			}
			else
			{
				setFlag(FLAG_PRONOUN, EMPTY_WORD);
				setFlag(FLAG_PRONOUN_ADJECT, EMPTY_WORD);
			}
		if (aux_preposition != -1) setFlag(FLAG_PREP, aux_preposition);
		if (aux_noun2 != -1) setFlag(FLAG_NOUN2, aux_noun2);
		if (aux_adject2 != -1) setFlag(FLAG_ADJECT2, aux_adject2);
		setReferredObject(getReferredObject());
		previous_verb = aux_verb;
		if ((aux_noun1!=-1) && (aux_noun1>=NUM_PROPER_NOUNS))
		{
			previous_noun = aux_noun1;
			if (aux_adject1!=-1) previous_adject = aux_adject1;
		}
		
	}
	if ((aux_verb + aux_noun1+ aux_adject1 + aux_adverb + aux_pronoun + aux_preposition + aux_noun2 + aux_adject2) != -8) parser_word_found = true;

	return SL_found;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                        Main functions and main loop                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Interrupt functions

function enableInterrupt()
{
	interruptDisabled = false;
}

function disableInterrupt()
{
	interruptDisabled = true;
}

function timer()
{
	// Timeout control
	timeout_progress=  timeout_progress + 1/32;  //timer happens every 40 milliseconds, but timeout counter should only increase every 1.28 seconds (according to PAWS documentation)
	timeout_length = getFlag(FLAG_TIMEOUT_LENGTH);
	if ((timeout_length) && (timeout_progress> timeout_length))  // time for timeout
	{
		timeout_progress = 0;
		if (($('.prompt').val() == '')  || (($('.prompt').val()!='') && (!bittest(getFlag(FLAG_TIMEOUT_SETTINGS),0))) )  // but first check there is no text type, or is allowed to timeout when text typed already
		{
			setFlag(FLAG_TIMEOUT_SETTINGS, bitset(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
			writeSysMessage(SYSMESS_TIMEOUT);	
			callProcess(PROCESS_TURN);
		}
	}	

	// PAUSE condact control
	if (inPause)
	{
		pauseRemainingTime = pauseRemainingTime - 40; // every tick = 40 milliseconds
		if (pauseRemainingTime<=0)
		{
			inPause = false;
			hideAnykeyLayer();
			waitKeyCallback()
		}
	}

	// Interrupt process control
	if (!interruptDisabled)
	if (interruptProcessExists)
	{
		callProcess(interrupt_proc);
		setFlag(FLAG_PARSER_SETTINGS, bitclear(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
	}

}

// Initialize and finalize functions

function farewell()
{
	writeSysMessage(SYSMESS_FAREWELL);
	ACCnewline();
}


function initializeConnections()
{
  connections = [].concat(connections_start);
}

function initializeObjects()
{
  for (i=0;i<objects.length;i++)
  {
  	objectsAttrLO = [].concat(objectsAttrLO_start);
  	objectsAttrHI = [].concat(objectsAttrHI_start);
  	objectsLocation = [].concat(objectsLocation_start);
  	objectsWeight = [].concat(objectsWeight_start);
  }
}

function  initializeLSWords()
{
  setFlag(FLAG_PREP,EMPTY_WORD);
  setFlag(FLAG_NOUN2,EMPTY_WORD);
  setFlag(FLAG_ADJECT2,EMPTY_WORD);
  setFlag(FLAG_PRONOUN,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_VERB,EMPTY_WORD);
  setFlag(FLAG_NOUN1,EMPTY_WORD);
  setFlag(FLAG_ADJECT1,EMPTY_WORD);
  setFlag(FLAG_ADVERB,EMPTY_WORD);
}


function initializeFlags()
{
  flags = [];
  for (var  i=0;i<FLAG_COUNT;i++) flags.push(0);
  setFlag(FLAG_MAXOBJECTS_CARRIED,4);
  setFlag(FLAG_PARSER_SETTINGS,9); // Pronoun sufixes active, DOALL and others ignore NPCs, etc. 00001001
  setFlag(FLAG_MAXWEIGHT_CARRIED,10);
  initializeLSWords();
  setFlag(FLAG_OBJECT_LIST_FORMAT,64); // List objects in a single sentence (comma separated)
  setFlag(FLAG_OBJECTS_CARRIED_COUNT,carried_objects);  // FALTA: el compilador genera esta variable, hay que cambiarlo en el compilador, ERA numero_inicial_de_objetos_llevados
}

function initializeInternalVars()
{
	num_objects = last_object_number + 1;
	transcript = '';
	timeout_progress = 0;
	previous_noun = EMPTY_WORD;
	previous_verb = EMPTY_WORD;
	previous_adject = EMPTY_WORD;
	player_order_buffer = '';
	last_player_orders = [];
	last_player_orders_pointer = 0;
	graphicsON = true; 
	soundsON = true; 
	interruptDisabled = false;
	unblock_process = null;
	done_flag = false;
	describe_location_flag =false;
	in_response = false;
	success = false;
	doall_flag = false;
	entry_for_doall	= '';
}

function initializeSound()
{
	sfxstopall();
}




function initialize()
{
	preloadsfx();
	initializeInternalVars();
	initializeSound();
	initializeFlags();
	initializeObjects();
	initializeConnections();
}



// Main loops

function descriptionLoop()
{
	do
	{
		describe_location_flag = false;
		if (!getFlag(FLAG_MODE)) clearTextWindow();
		if ((isDarkHere()) && (!lightObjectsPresent())) writeSysMessage(SYSMESS_ISDARK); else writeLocation(loc_here()); 
		h_description_init();
		playLocationMusic(loc_here());
		if (loc_here()) drawPicture(loc_here()); else hideGraphicsWindow(); // Don't show picture at location 0
		ACCminus(FLAG_AUTODEC2,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC3,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC4,1);
		callProcess(PROCESS_DESCRIPTION);
		h_description_post();
		if (describe_location_flag) continue; // descriptionLoop() again without nesting
		describe_location_flag = false;
		callProcess(PROCESS_TURN);
		if (describe_location_flag) continue; 
		describe_location_flag = false;
		focusInput();
		break; // Dirty trick to make this happen just one, but many times if descriptioLoop() should be repeated
	} while (true);

}

function orderEnteredLoop(player_order)
{
	previous_verb = EMPTY_WORD;
	setFlag(FLAG_TIMEOUT_SETTINGS, bitclear(getFlag(FLAG_TIMEOUT_SETTINGS),7)); // Clears timeout bit
	if (player_order == '') {writeSysMessage(SYSMESS_SORRY); ACCnewline(); return; };	
	player_order = h_playerOrder(player_order); //hook
	copyOrderToTextWindow(player_order);
	toParserBuffer(player_order);
	do 
	{
		describe_location_flag = false;
		ACCminus(FLAG_AUTODEC5,1);
		ACCminus(FLAG_AUTODEC6,1);
		ACCminus(FLAG_AUTODEC7,1);
		ACCminus(FLAG_AUTODEC8,1);
		if (isDarkHere()) ACCminus(FLAG_AUTODEC9,1);
		if ((isDarkHere()) && (lightObjectsAt(loc_here())==0)) ACCminus(FLAG_AUTODEC10,1);
		
		if (describe_location_flag) 
		{
			descriptionLoop();
			return;
		};

		if (getLogicSentence())
		{
			incTurns();
			done_flag = false;
			callProcess(PROCESS_RESPONSE); // Response table
			if (describe_location_flag) 
			{
				descriptionLoop();
				return;
			};
			if (!done_flag) 
			{
				if ((getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) && (CNDmove(FLAG_LOCATION)))
				{
					descriptionLoop();
					return;
				} else if (getFlag(FLAG_VERB)<NUM_CONNECTION_VERBS) {writeSysMessage(SYSMESS_WRONGDIRECTION);ACCnewline();}	else {writeSysMessage(SYSMESS_CANTDOTHAT);ACCnewline();};

			}
		} else
		{
			h_invalidOrder(player_order);
			if (parser_word_found) {writeSysMessage(SYSMESS_IDONTUNDERSTAND);   ACCnewline() }
			    		      else {writeSysMessage(SYSMESS_NONSENSE_SENTENCE); ACCnewline() };	
		}  
		callProcess(PROCESS_TURN);
	} while (player_order_buffer !='');
	previous_verb = ''; // Can't use previous verb if a new order is typed (we keep previous noun though, it can be used)
	focusInput();
}


function restart()
{
	location.reload();	
}


function hideBlock()
{
	clearInputWindow();
    $('.block_layer').hide('slow');
    enableInterrupt();   	
    $('.input').show();  
    focusInput();
}

function hideAnykeyLayer()
{
	$('.anykey_layer').hide();
    $('.input').show();  
    focusInput();   
}

function showAnykeyLayer()
{
	$('.anykey_layer').show();
    $('.input').hide();  
}

//called when the block layer is closed
function closeBlock()
{
	if (!inBlock) return;
	inBlock = false;
	hideBlock();
    var proToCall = unblock_process;
	unblock_process = null;
	callProcess(proToCall);
	if (describe_location_flag) descriptionLoop();
}

function setInputPlaceHolder()
{
	var prompt_msg = getFlag(FLAG_PROMPT);
	if (!prompt_msg)
	{
		var random = Math.floor((Math.random()*100));
		if (random<30) prompt_msg = SYSMESS_PROMPT0; else
		if ((random>=30) && (random<60)) prompt_msg = SYSMESS_PROMPT1; else
		if ((random>=60) && (random<90)) prompt_msg = SYSMESS_PROMPT2; else
		if (random>=90) prompt_msg = SYSMESS_PROMPT3;
	}
	$('.prompt').attr('placeholder', $('<div>'+getSysMessageText(prompt_msg).replace(/(?:<br>)*$/,'').replace( /<br>/g, ', ' )+'</div>').text());
}


function divTextScrollUp()
{
   	var currentPos = $('.text').scrollTop();
	if (currentPos>=DIV_TEXT_SCROLL_STEP) $('.text').scrollTop(currentPos - DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop(0);
}

function divTextScrollDown()
{
   	var currentPos = $('.text').scrollTop();
   	if (currentPos <= ($('.text')[0].scrollHeight - DIV_TEXT_SCROLL_STEP)) $('.text').scrollTop(currentPos + DIV_TEXT_SCROLL_STEP); else $('.text').scrollTop($('.text')[0].scrollHeight);
}

// Autocomplete functions

function predictiveText(currentText)
{
	if (currentText == '') return currentText;
	var wordToComplete;
	var words = currentText.split(' ');
	if (autocompleteStep!=0) wordToComplete = autocompleteBaseWord; else wordToComplete = words[words.length-1];
	words[words.length-1] = completedWord(wordToComplete);
	return words.join(' ');
}


function initAutoComplete()
{
	for (var j=0;j<vocabulary.length;j++)
		if (vocabulary[j][VOCABULARY_TYPE] == WORDTYPE_VERB)
			if (vocabulary[j][VOCABULARY_WORD].length >= 3)
				autocomplete.push(vocabulary[j][VOCABULARY_WORD].toLowerCase());
}

function addToAutoComplete(sentence)
{
	var words = sentence.split(' ');
	for (var i=0;i<words.length;i++)
	{
		var finalWord = '';
		for (var j=0;j<words[i].length;j++)
		{
			var c = words[i][j].toLowerCase();
			if ("abcdefghijklmnopqrstuvwxyzáéíóúàèìòùçäëïÖüâêîôû".indexOf(c) != -1) finalWord = finalWord + c;
			else break;
		}
	
		if (finalWord.length>=3) 
		{
			var index = autocomplete.indexOf(finalWord);
			if (index!=-1) autocomplete.splice(index,1);
			autocomplete.push(finalWord);
		}
	}
}

function completedWord(word)
{
	if (word=='') return '';
   autocompleteBaseWord  =word;
   var foundCount = 0;
   for (var i = autocomplete.length-1;i>=0; i--)
   {
   	  if (autocomplete[i].length > word.length) 
   	  	 if (autocomplete[i].indexOf(word)==0) 
   	  	 	{
   	  	 		foundCount++;
   	  	 		if (foundCount>autocompleteStep)
   	  	 		{
   	  	 			autocompleteStep++;
   	  	 			return autocomplete[i];
   	  	 		}
   	  	 	}
   }
   return word;
}


// Exacution starts here, called by the html file on document.ready()
function start()
{
	h_init(); //hook
	$('.graphics').addClass('half_graphics');
	$('.text').addClass('half_text');
	if (isBadIE()) alert(STR_BADIE)
	loadPronounSufixes();	
    setInputPlaceHolder();
    initAutoComplete();

	// Assign keypress action for input box (detect enter key press)
	$('.prompt').keypress(function(e) {  
    	if (e.which == 13) 
    	{ 
    		setInputPlaceHolder();
    		player_order = $('.prompt').val();
    		if (player_order.charAt(0) == '#')
    		{
    			addToTranscript(player_order + STR_NEWLINE);
    			clearInputWindow();
    		} 
    		else
    		if (player_order!='') 
    				orderEnteredLoop(player_order);
    	}
    });

	// Assign arrow up key press to recover last order
    $('.prompt').keyup( function(e) {
    	if (e.which  == 38) $('.prompt').val(get_prev_player_order());
    	if (e.which  == 40) $('.prompt').val(get_next_player_order());
    });


    // Assign tab keydown to complete word
    $('.prompt').keydown( function(e) {
    	if (e.which == 9) 
    		{
    			$('.prompt').val(predictiveText($('.prompt').val()));
    			e.preventDefault();
    		} else 
    		{
		    	autocompleteStep = 0;
    			autocompleteBaseWord = ''; // Any keypress other than tab resets the autocomplete feature
    		}
    });

    //Detect resize to change flag 12
     $(window).resize(function () {
     	setFlag(FLAG_PARSER_SETTINGS, bitset(getFlag(FLAG_PARSER_SETTINGS), 4));  // Set bit at flag that marks that a window resize happened 
     	clearPictureAt();
     	return;
     });


     // assign any click on block layer --> close it
     $(document).click( function(e) {

	// if waiting for END response
	if (inEND)
	{
		restart();
		return;
	}

     	if (inBlock)
     	{
     		closeBlock();
     		e.preventDefault();
     		return;
     	}

     	if (inAnykey)  // return for ANYKEY, accepts mouse click
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		waitKeyCallback();
     		e.preventDefault();
     		return;
    	}

     });

     //Make tap act as click
    //document.addEventListener('touchstart', function(e) {$(document).click(); }, false);   
     
     
	$(document).keydown(function(e) {

		if (!h_keydown(e)) return; // hook

		// if waiting for END response
		if (inEND)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					location.reload();
					break;
				case endNOresponseCode:
					inEND = false;
					sfxstopall();
					$('body').hide('slow');
					break;
			}
			return;
		}


		// if waiting for QUIT response
		if (inQUIT)
		{
			var endYESresponse = getSysMessageText(SYSMESS_YES);
			var endNOresponse = getSysMessageText(SYSMESS_NO);
			if (!endYESresponse.length) endYESresponse = 'Y'; // Prevent problems with empy message
			if (!endNOresponse.length) endNOresponse = 'N'; 
			var endYESresponseCode = endYESresponse.charCodeAt(0);
			var endNOresponseCode = endNOresponse.charCodeAt(0);

			switch ( e.keyCode )
			{
				case endYESresponseCode:
				case 13: // return
				case 32: // space
					inQUIT=false;
					e.preventDefault();
					waitKeyCallback();
					return;
				case endNOresponseCode:
					inQUIT=false;
					waitkey_callback_function.pop();
					hideAnykeyLayer();
					e.preventDefault();
					break;
			}
		}

		// ignore uninteresting keys
		switch ( e.keyCode )
		{
			case 9:  // tab   \ keys used during
			case 13: // enter / keyboard navigation
			case 16: // shift
			case 17: // ctrl
			case 18: // alt
			case 20: // caps lock
			case 91: // left Windows key
			case 92: // left Windows key
			case 93: // left Windows key
			case 225: // right alt
				// do not focus the input - the user was probably doing something else
				// (e.g. alt-tab'ing to another window)
				return;
		}


		if (inGetkey)  // return for getkey
     	{
     		setFlag(getkey_return_flag, e.keyCode);
     		getkey_return_flag = null;
     		inGetkey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
      	}

     	// Scroll text window using PgUp/PgDown
        if (e.keyCode==33)  // PgUp
        {
        	divTextScrollUp();
        	e.preventDefault();
        	return;
        }
        if (e.keyCode==34)  // PgDown
        {
        	divTextScrollDown();
        	return;
        }


     	if (inAnykey)  // return for anykey
     	{
     		inAnykey = false;
     		hideAnykeyLayer();
     		e.preventDefault();
     		waitKeyCallback();
     		return;
     	}

		// if keypress and block displayed, close it
     	if (inBlock)
     		{
     			closeBlock();
     			e.preventDefault();
     			return;
     		}


     	// if ESC pressed and transcript layer visible, close it
     	if ((inTranscript) &&  (e.keyCode == 27)) 
     		{
     			$('.transcript_layer').hide();
     			inTranscript = false;
     			e.preventDefault();
     			return;
     		}

	// focus the input if the user is likely to expect it
	// (but not if they're e.g. ctrl+c'ing some text)
	switch ( e.keyCode )
	{
		case 8: // backspace
		case 9: // tab
		case 13: // enter
			break;
		default:
			if ( !e.ctrlKey && !e.altKey ) focusInput();
	}

	});


    $(document).bind('wheel mousewheel',function(e)
    {
  		if((e.originalEvent.wheelDelta||-e.originalEvent.deltaY) > 0) divTextScrollUp(); else divTextScrollDown();
    });


	initialize();
	descriptionLoop();
	focusInput();
	
	h_post();  //hook

    // Start interrupt process
    setInterval( timer, TIMER_MILLISECONDS );

}

$('document').ready(
	function ()
	{
		start();
	}
	);

// VOCABULARY

vocabulary = [];
vocabulary.push([42, "5", 1]);
vocabulary.push([123, "ABOUT", 0]);
vocabulary.push([118, "ACTIVATE", 0]);
vocabulary.push([30, "AKRO", 1]);
vocabulary.push([30, "AKROL", 1]);
vocabulary.push([2, "AND", 5]);
vocabulary.push([9, "ASCE", 1]);
vocabulary.push([9, "ASCEND", 1]);
vocabulary.push([13, "AT", 6]);
vocabulary.push([90, "ATTA", 0]);
vocabulary.push([90, "ATTACK", 0]);
vocabulary.push([62, "BAR", 1]);
vocabulary.push([64, "BEVERAGE", 1]);
vocabulary.push([91, "BLAS", 0]);
vocabulary.push([91, "BLAST", 0]);
vocabulary.push([51, "BUTT", 1]);
vocabulary.push([51, "BUTTON", 1]);
vocabulary.push([38, "CARD", 1]);
vocabulary.push([122, "CHANGECSS", 0]);
vocabulary.push([9, "CLIM", 1]);
vocabulary.push([9, "CLIMB", 1]);
vocabulary.push([40, "CONS", 0]);
vocabulary.push([40, "CONSUME", 0]);
vocabulary.push([58, "COVA", 1]);
vocabulary.push([58, "COVALIUM", 1]);
vocabulary.push([21, "CRUI", 1]);
vocabulary.push([21, "CRUISER", 1]);
vocabulary.push([57, "CRYS", 1]);
vocabulary.push([57, "CRYSTAL", 1]);
vocabulary.push([10, "D", 1]);
vocabulary.push([37, "DEAC", 1]);
vocabulary.push([37, "DEACTIVATE", 1]);
vocabulary.push([10, "DESC", 1]);
vocabulary.push([10, "DESCEND", 1]);
vocabulary.push([65, "DIVE", 0]);
vocabulary.push([71, "DOOR", 1]);
vocabulary.push([10, "DOWN", 1]);
vocabulary.push([64, "DRIN", 1]);
vocabulary.push([64, "DRINK", 1]);
vocabulary.push([101, "DROP", 0]);
vocabulary.push([3, "E", 1]);
vocabulary.push([3, "EAST", 1]);
vocabulary.push([40, "EAT", 0]);
vocabulary.push([11, "ENTER", 1]);
vocabulary.push([32, "EX", 0]);
vocabulary.push([32, "EXAM", 0]);
vocabulary.push([32, "EXAMINE", 0]);
vocabulary.push([111, "EXITS", 0]);
vocabulary.push([114, "EXTRACT", 0]);
vocabulary.push([53, "FOOD", 1]);
vocabulary.push([9, "FOR", 6]);
vocabulary.push([3, "FROM", 6]);
vocabulary.push([121, "GAMES", 1]);
vocabulary.push([100, "GET", 0]);
vocabulary.push([101, "GIVE", 0]);
vocabulary.push([109, "GO", 0]);
vocabulary.push([62, "GOLD", 1]);
vocabulary.push([26, "GRAF", 1]);
vocabulary.push([26, "GRAFLON", 1]);
vocabulary.push([33, "HEAT", 0]);
vocabulary.push([20, "HELP", 0]);
vocabulary.push([90, "HIT", 0]);
vocabulary.push([90, "HURT", 0]);
vocabulary.push([104, "I", 0]);
vocabulary.push([35, "ICE", 1]);
vocabulary.push([7, "IN", 6]);
vocabulary.push([123, "INFO", 0]);
vocabulary.push([40, "INGE", 0]);
vocabulary.push([40, "INGEST", 0]);
vocabulary.push([39, "INSE", 0]);
vocabulary.push([39, "INSERT", 0]);
vocabulary.push([6, "INTO", 6]);
vocabulary.push([104, "INV", 0]);
vocabulary.push([104, "INVE", 1]);
vocabulary.push([104, "INVEN", 1]);
vocabulary.push([104, "INVENT", 0]);
vocabulary.push([104, "INVENTORY", 0]);
vocabulary.push([2, "IT", 4]);
vocabulary.push([56, "JEKR", 1]);
vocabulary.push([56, "JEKRA", 1]);
vocabulary.push([41, "JOSH", 0]);
vocabulary.push([90, "KICK", 0]);
vocabulary.push([90, "KILL", 0]);
vocabulary.push([105, "L", 0]);
vocabulary.push([59, "LANC", 1]);
vocabulary.push([59, "LANCE", 1]);
vocabulary.push([12, "LEAVE", 1]);
vocabulary.push([23, "LIST", 1]);
vocabulary.push([108, "LOAD", 0]);
vocabulary.push([105, "LOOK", 0]);
vocabulary.push([60, "MAP", 1]);
vocabulary.push([33, "MELT", 0]);
vocabulary.push([1, "N", 1]);
vocabulary.push([5, "NE", 1]);
vocabulary.push([1, "NORTH", 1]);
vocabulary.push([5, "NORTHEAST", 1]);
vocabulary.push([6, "NORTHWEST", 1]);
vocabulary.push([6, "NW", 1]);
vocabulary.push([5, "OF", 6]);
vocabulary.push([4, "OFF", 6]);
vocabulary.push([12, "ON", 6]);
vocabulary.push([70, "OPEN", 0]);
vocabulary.push([10, "OUT", 6]);
vocabulary.push([11, "OVER", 6]);
vocabulary.push([54, "PLAN", 1]);
vocabulary.push([54, "PLANT", 1]);
vocabulary.push([15, "PLAY", 0]);
vocabulary.push([50, "PRES", 0]);
vocabulary.push([50, "PRESS", 0]);
vocabulary.push([52, "PROB", 1]);
vocabulary.push([52, "PROBE", 1]);
vocabulary.push([50, "PUSH", 0]);
vocabulary.push([113, "PUT", 0]);
vocabulary.push([106, "QUIT", 0]);
vocabulary.push([32, "READ", 0]);
vocabulary.push([32, "REGISTER", 0]);
vocabulary.push([112, "REMOVE", 0]);
vocabulary.push([61, "RIFL", 1]);
vocabulary.push([61, "RIFLE", 1]);
vocabulary.push([17, "RUG", 1]);
vocabulary.push([2, "S", 1]);
vocabulary.push([107, "SAVE", 0]);
vocabulary.push([120, "SAVED", 1]);
vocabulary.push([16, "SAY", 0]);
vocabulary.push([7, "SE", 1]);
vocabulary.push([32, "SEAR", 0]);
vocabulary.push([28, "SEPT", 1]);
vocabulary.push([28, "SEPTULE", 1]);
vocabulary.push([91, "SHOO", 0]);
vocabulary.push([91, "SHOOT", 0]);
vocabulary.push([18, "SIT", 0]);
vocabulary.push([21, "SKYCRUISER", 1]);
vocabulary.push([45, "SLEE", 0]);
vocabulary.push([45, "SLEEP", 0]);
vocabulary.push([64, "SODA", 1]);
vocabulary.push([2, "SOUT", 1]);
vocabulary.push([2, "SOUTH", 1]);
vocabulary.push([7, "SOUTHEAST", 1]);
vocabulary.push([8, "SOUTHWEST", 1]);
vocabulary.push([21, "SPACECRUIS", 1]);
vocabulary.push([115, "SPIT", 0]);
vocabulary.push([19, "STAN", 0]);
vocabulary.push([19, "STAND", 0]);
vocabulary.push([29, "STAT", 1]);
vocabulary.push([29, "STATION", 1]);
vocabulary.push([106, "STOP", 0]);
vocabulary.push([8, "SW", 1]);
vocabulary.push([65, "SWIM", 0]);
vocabulary.push([119, "SWITCH", 0]);
vocabulary.push([100, "TAKE", 0]);
vocabulary.push([14, "TELE", 0]);
vocabulary.push([14, "TELEPORT", 0]);
vocabulary.push([27, "TERM", 1]);
vocabulary.push([27, "TERMINAN", 1]);
vocabulary.push([2, "THEM", 4]);
vocabulary.push([2, "THEN", 5]);
vocabulary.push([13, "THRO", 1]);
vocabulary.push([13, "THROUGH", 1]);
vocabulary.push([116, "THROW", 0]);
vocabulary.push([2, "TO", 6]);
vocabulary.push([47, "TRAI", 1]);
vocabulary.push([47, "TRAIN", 1]);
vocabulary.push([31, "TRAM", 1]);
vocabulary.push([80, "TREE", 1]);
vocabulary.push([117, "TURN", 0]);
vocabulary.push([22, "TYPE", 0]);
vocabulary.push([9, "U", 1]);
vocabulary.push([9, "UP", 1]);
vocabulary.push([34, "USE", 0]);
vocabulary.push([4, "W", 1]);
vocabulary.push([24, "WAIT", 0]);
vocabulary.push([113, "WEAR", 0]);
vocabulary.push([4, "WEST", 1]);
vocabulary.push([8, "WITH", 6]);
vocabulary.push([111, "X", 0]);
vocabulary.push([25, "ZAGR", 1]);
vocabulary.push([25, "ZAGRO", 1]);
vocabulary.push([91, "ZAP", 0]);



// SYS MESSAGES

total_sysmessages=68;

sysmessages = [];

sysmessages[0] = "IT'S TOO DARK TO SEE ANYTHING.";
sysmessages[1] = "{CLASS|addinfo|ALSO VISIBLE:}<br>";
sysmessages[2] = "\nCOMMAND?<br>";
sysmessages[3] = "\nCOMMAND?<br>";
sysmessages[4] = "\nCOMMAND?<br>";
sysmessages[5] = "\nCOMMAND?<br>";
sysmessages[6] = "INPUT NOT UNDERSTOOD.<br>";
sysmessages[7] = "THAT MOVEMENT IS NOT POSSIBLE.<br>";
sysmessages[8] = "NOT POSSIBLE.<br>";
sysmessages[9] = "PRESENTLY CARRYING:";
sysmessages[10] = "(WORN)";
sysmessages[11] = "NOTHING.<br>";
sysmessages[12] = "REALLY QUIT (Y/N)?<br>";
sysmessages[13] = "\nGAME TERMINATED.\n\nANOTHER ATTEMPT?<br>";
sysmessages[14] = "{CLASS|center|\nPRESS A KEY TO RESET COMPUTER.\n\n }<br>";
sysmessages[15] = "CONFIRMED.<br><br>";
sysmessages[16] = "<br>{CLASS|center|PRESS ANY KEY TO CONTINUE}<br>";
sysmessages[17] = "You have typed ";
sysmessages[18] = " turn";
sysmessages[19] = "s";
sysmessages[20] = ".<br>";
sysmessages[21] = "You have scored ";
sysmessages[22] = "%.<br>";
sysmessages[23] = "YOU ARE NOT WEARING {OREF}.<br>";
sysmessages[24] = "YOU ARE ALREADY WEARING {OREF}.<br>";
sysmessages[25] = "ALREADY CARRIED.<br>";
sysmessages[26] = "OBJECT NOT VISIBLE.<br>";
sysmessages[27] = "NOT POSSIBLE: EXCESS CARRIED.<br>";
sysmessages[28] = "NOT CARRIED.<br>";
sysmessages[29] = "YOU ARE ALREADY WEARING {OREF}.<br>";
sysmessages[30] = "Y<br>";
sysmessages[31] = "N<br>";
sysmessages[32] = "More…<br>";
sysmessages[33] = "><br>";
sysmessages[34] = "<br>";
sysmessages[35] = "TIME PASSES…<br>";
sysmessages[36] = "";
sysmessages[37] = "";
sysmessages[38] = "";
sysmessages[39] = "";
sysmessages[40] = "NOT POSSIBLE.<br>";
sysmessages[41] = "NOT POSSIBLE.<br>";
sysmessages[42] = "<br>";
sysmessages[43] = "NOT POSSIBLE: EXCESS CARRIED.<br>";
sysmessages[44] = "YOU PUT {OREF} INTO<br>";
sysmessages[45] = "{OREF} IS NOT INTO<br>";
sysmessages[46] = "<br>";
sysmessages[47] = "<br>";
sysmessages[48] = "<br>";
sysmessages[49] = "NOT CARRIED.<br>";
sysmessages[50] = "NOT POSSIBLE.<br>";
sysmessages[51] = ".<br>";
sysmessages[52] = "THAT IS NOT INTO<br>";
sysmessages[53] = "NOTHING AT ALL<br>";
sysmessages[54] = "FILE NOT FOUND.<br>";
sysmessages[55] = "FILE CORRUPT.<br>";
sysmessages[56] = "I/O ERROR. FILE NOT SAVED.<br>";
sysmessages[57] = "DIRECTORY FULL.<br>";
sysmessages[58] = "Please enter savegame name you used when saving the game status.";
sysmessages[59] = "Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>";
sysmessages[60] = "Please enter savegame name. Remember to note down the name you choose (Or type 'SAVED GAMES' to retrieve saved game names), as it will be requested in order to restore the game status.";
sysmessages[61] = "<br>";
sysmessages[62] = "SORRY? PLEASE TRY OTHER WORDS.<br>";
sysmessages[63] = "HERE<br>";
sysmessages[64] = "YOU CAN SEE<br>";
sysmessages[65] = "YOU CAN SEE<br>";
sysmessages[66] = "INSIDE YOU SEE<br>";
sysmessages[67] = "ON TOP YOU SEE<br>\n\n\n\n";

// USER MESSAGES

total_messages=16;

messages = [];

messages[68] = "<br>";
messages[69] = "{CLASS|tab|{ACTION|DOWN|DOWN}}▸ UNKNOWN\n\n";
messages[1000] = "Exits: ";
messages[1001] = "You can't see any exits\n";
messages[1003] = "{ACTION|north|north}";
messages[1004] = "{ACTION|south|south}";
messages[1005] = "{ACTION|east|east}";
messages[1006] = "{ACTION|west|west}";
messages[1011] = "{ACTION|up|up}";
messages[1012] = "{ACTION|down|down}";
messages[1013] = "{ACTION|tele|tele}";
messages[1014] = "{ACTION|thro|thro}";
messages[1015] = "{ACTION|into|into}";
messages[1016] = "{ACTION|out|out}";
messages[1023] = "{ACTION|spac|spac}";
messages[1049] = "{ACTION|trai|trai}\n\n\n";

// WRITE MESSAGES

total_writemessages=81;

writemessages = [];

writemessages[0] = "RESPONSE_START";
writemessages[1] = "RESPONSE_USER";
writemessages[2] = "YOU CLIMB A TREE BUT FAIL TO FIND ANYTHING OF INTEREST SO YOU RETURN TO THE GROUND.";
writemessages[3] = "THE SHIP AUTO-TELEPORT ACTIVATES AND YOU DEMATERIALIZE.";
writemessages[4] = "YOU DE-MATERIALIZE… BUT NEVER RE-MATERIALIZE!";
writemessages[5] = "YOU LOSE (of course!) AND THE COMPUTER STARTS A NEW GAME.";
writemessages[6] = "JEKRA SAYS, &quot;SHUT UP, HUMANOID!&quot;";
writemessages[7] = "The planets which are in the ship's range are found by\nTYPE LIST PLANETS\nTo visit a planet\nTYPE (name of planet)";
writemessages[8] = "SEARCH HIGH & LOW!";
writemessages[9] = "The ship's computer responds,\n\n{CLASS|planettext|SCANNERS INDICATE PLANETS GRAFLON, TERMINAN, SEPTULE, AKROL IN RANGE.\nALSO ZAGRO SPACEPORT, STATION 1.}\n";
writemessages[10] = "You set course for the Spaceport at Zagro. Soon you land and get out.";
writemessages[11] = "YOU SET A COURSE FOR THE PLANET AND SOON ENTER ORBIT ABOUT IT. YOU THEN GO TO THE TELEPORT AND BEAM DOWN.";
writemessages[12] = "YOU SET A COURSE FOR THE PLANET AND SOON ENTER ORBIT ABOUT IT. YOU THEN GO TO THE TELEPORT AND BEAM DOWN.";
writemessages[13] = "YOU SET A COURSE FOR THE PLANET AND SOON ENTER ORBIT ABOUT IT. YOU THEN GO TO THE TELEPORT AND BEAM DOWN.";
writemessages[14] = "You set course and soon dock with the slowly spinning space station. You go out through the large docking bay doors into the station.";
writemessages[15] = "YOU SET A COURSE FOR THE PLANET AND SOON ENTER ORBIT ABOUT IT. YOU THEN GO TO THE TELEPORT AND BEAM DOWN.";
writemessages[16] = "THE COMPUTER DEACTIVATES THE ELECTRIC CHARGE ON THE FLOOR.";
writemessages[17] = "THE COMPUTER PRINTS, &quot;GOOD MORNING. ALL SECUIRITY WORKING. ELECTRIC FLOOR ACTIVATED.&quot;";
writemessages[18] = "YOU WAIT FOR A TIME…";
writemessages[19] = "THE CARD HAS ENTRANCE OK STAMPED ON IT IN MAGNETIC INK.";
writemessages[20] = "THIS IS WHAT YOU DESIRE!";
writemessages[21] = "A SHINY, BLACK METAL.";
writemessages[22] = "IT EMITS HEAT.";
writemessages[23] = "THE MAP READS: &quot;When carried, this map will guide beings through the wastelands.&quot;";
writemessages[24] = "IT IS FULLY CHARGED.";
writemessages[25] = "IT IS OLD BUT IT FULFILLS ITS MISSION.";
writemessages[26] = "IT IS WAITING AT THE PLATFORM.";
writemessages[27] = "You can't see any train.";
writemessages[28] = "THERE MIGHT BE SOMETHING FROZEN IN THE ICE…";
writemessages[29] = "A WELL HIDDEN DOOR IS IN THE FLOOR BELOW YOU.";
writemessages[30] = "THERE IS A SPACE BEHIND THE WATERFALL…";
writemessages[31] = "You can't see {OREF} here.";
writemessages[32] = "You can't see that here.";
writemessages[33] = "Maybe that is not important.";
writemessages[34] = "YOU SEE NOTHING RELEVANT.";
writemessages[35] = "USING THE THERMA LANCE, YOU MELT THE ICE.";
writemessages[36] = "USING THE THERMA LANCE, YOU MELT THE ICE.";
writemessages[37] = "YOU INSERT THE CARD. THE VOICE SAYS, &quot;THANK YOU.&quot;\nA DOOR OPENS AND YOU GO IN.";
writemessages[38] = "YOU INGEST THE FOOD.";
writemessages[39] = "YOU INGEST THE FOOD.";
writemessages[40] = "YOU INGEST THE FOOD.";
writemessages[41] = "YOU INGEST THE FOOD.";
writemessages[42] = "YOU INGEST THE FOOD.";
writemessages[43] = "YOU INGEST THE FOOD.";
writemessages[44] = "YOU INGEST THE FOOD.";
writemessages[45] = "YOU INGEST THE FOOD.";
writemessages[46] = "\n{CLASS|CENTER|GALAXIAS\n\n© 1984  1985 DELTA 4 Software}\nAuthor: Fergus McNeill\nGame written using THE QUILL and THE ILLUSTRATOR from Gilsoft.\nNo unauthorized copying, etc.";
writemessages[47] = "\n{CLASS|CENTER|GALAXIAS\n© 1986 DELTA 4 Software\nAuthor: Fergus McNeill}";
writemessages[48] = "\nA fantastic SCI-FI adventure with over 100 locations";
writemessages[49] = "\nYou play the mercenary, Kas Wellan, a space pirate of some reputation. You have heard about a &quot;red crystal&quot; and have decided to search for it and &quot;obtain&quot; it for yourself.";
writemessages[50] = "Good luck";
writemessages[51] = "YOU SLEEP FOR A TIME…";
writemessages[52] = "AS YOU PRESS THE BUTTON, 2 LARGE MISSILES EXPLODE OUT OF THE HARD GROUND NEARBY AND LOCK ONTO YOUR SHIP AS TARGET.\nIN A MOMENT, YOUR SHIP IS DUST!";
writemessages[53] = "YOU HAVE A DRINK.";
writemessages[54] = "YOU HAVE A DRINK.";
writemessages[55] = "YOU HAVE A DRINK.";
writemessages[56] = "YOU DON'T HAVE ANYTHING TO DRINK.";
writemessages[57] = "YOU ARE EATEN BY A LARGE REPTILE SWIMMING IN THE DEPTHS!";
writemessages[58] = "YOU ARE REFRESHED BY YOUR PLEASANT SWIM.";
writemessages[59] = "YOU ARE REFRESHED BY YOUR PLEASANT SWIM.";
writemessages[60] = "YOU ARE REFRESHED BY YOUR PLEASANT SWIM.";
writemessages[61] = "THE AIR RUSHES OUT LEAVING YOU IN A VACUUM. YOUR BLOOD BOILS, AND YOU EXPLODE!";
writemessages[62] = "THE AIR RUSHES OUT LEAVING YOU IN A VACUUM. YOUR BLOOD BOILS, AND YOU EXPLODE!";
writemessages[63] = "USING THE PROBE, YOU OPEN THE DOOR OF THE VAULT.";
writemessages[64] = "YOU ATTACK THE PLANT BUT FAIL TO KILL IT.";
writemessages[65] = "YOU ATTACK JEKRA AND WITH ONE BLOW, KNOCK HIM DOWN, DEAD.";
writemessages[66] = "YOU SHOOT THE PLANT. IT EXPLODES INTO A CLOUD OF BLUE SPARKS. ONLY A CHARRED REMNANT IS LEFT.";
writemessages[67] = "YOU SHOOT JEKRA. HE DIES IN A CLOUD OF BLUE SPARKS.";
writemessages[68] = "WHAT DO YOU WANT TO DRINK?";
writemessages[69] = "JEKRA HAPPILY TAKES THE COVALIUM AND GIVES THE MAP TO YOU.";
writemessages[70] = "RESPONSE_DEFAULT_START";
writemessages[71] = "A HIDDEN LASER SHOOTS AND YOU EXPLODE INTO A MILLION GLOWING FRAGMENTS.";
writemessages[72] = "JEKRA SAYS, &quot;I WANT COVALIUM.&quot;";
writemessages[73] = "RESPONSE_DEFAULT_END";
writemessages[74] = "PRO1";
writemessages[75] = "Press a key";
writemessages[76] = "PRO2";
writemessages[77] = "THE PLANT ATTACKS YOU WITH GREAT VIGOUR!";
writemessages[78] = "Well done! You have proved that you are a true adventurer.\nNow write to DELTA 4 for your next quest!";
writemessages[79] = "AS YOU STEP ONTO THE FLOOR THERE IS A BLINDING FLASH AND 20000000 VOLTS RUN THROUGH YOU.";
writemessages[80] = "YOUR STRENGTH IS GONE…\nYOU ARE DEAD!";

// LOCATION MESSAGES

total_location_messages=103;

locations = [];

locations[0] = "YOU ARE IN ZAGRO SPACEPORT.\n THE WALLS ARE MADE OF MIRROR GLASS AND THE ROOF GLOWS TO BATHE THE WHOLE AREA IN A DEEP GREEN LIGHT.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ MAIN CITY COMPLEX\n {CLASS|tab|{ACTION|EAST|EAST}}▸ LANDING BAY\n {CLASS|tab|{ACTION|WEST|WEST}}▸ ENGINEERING CENTRE\n\n <hr />";
locations[1] = "YOU ARE IN THE ENGINEERING CENTRE. THE FLOOR IN HERE IS LITTERED WITH OLD RUBBISH.\n THE WALLS ARE GREASY AND HERE AND THERE, PATCHES OF OIL LIE SOAKING INTO THE WORK-MATS\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ MAIN SPACEPORT AREA\n {CLASS|tab|{ACTION|WEST|WEST}}▸ TELEPORT UNIT\n\n <hr />";
locations[2] = "YOU ARE IN A {ACTION|TELEPORT|TELEPORT} UNIT.\n IT IS SMALL, ABOUT TWICE THE SIZE OF A PHONE BOX ON EARTH. THE UNIT ITSELF IS MADE OF SOME UNBREAKABLE, TRANSPARENT MATERIAL.\n\n EXITS\n {CLASS|bigtab|{ACTION|TELEPORT|TELEPORT}}▸ UNKNOWN\n {CLASS|bigtab|{ACTION|EAST|EAST}}▸ ENGINEERING CENTRE\n\n <hr />";
locations[3] = "YOU ARE IN THE LANDING BAY OF ZAGRO SPACEPORT.\n THE FLOOR IS SCORCHED AND VERY DIRTY. AT THE FAR END OF THE LANDING BAY 2 HUGE DOORS OPEN OUT INTO SPACE. AIR IS KEPT IN BY A FORCE FIELD.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ MAIN SPACEPORT AREA\n\n <hr />";
locations[4] = "YOU ARE IN A FOOD CONSUMPTION CENTRE. MANY SMOOTHLY CURVED GLASS TABLES HOVER JUST ABOVE THE FLOOR. THE SEATING IS ALSO MADE OF THIS FLOATING GLASS.\n THE FLOOR IS PLUSHLY CARPETED.\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ GLASS STREET\n\n <hr />";
locations[5] = "YOU ARE ON A SPARKLING GLASS STREETWAY. ALTHOUGH IT IS SMOOTH, THE GLASS GLITTERS LIKE CRYSTAL. A FRIENDLY FOOD DISPENSER SAYS, &quot;Hello. It is my pleasure and  honour to provide you with consumables.&quot;\n SOME STEPS LEAD {ACTION|DOWN|DOWN}.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ MAIN SPACEPORT AREA\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ TUNNELWALK\n {CLASS|tab|{ACTION|EAST|EAST}}▸ {CLASS|name|METALON BAR}\n {CLASS|tab|{ACTION|WEST|WEST}}▸ FOOD CONSUMPTION AREA\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ DOME AREA\n\n <hr />";
locations[6] = "YOU ARE IN THE {CLASS|name|METALON BAR}. THIS IS A &quot;SHADY&quot; AREA FREQUENTED BY SPACE-PIRATES.\n BEHIND THE BAR, THE WALL IS BURNT WITH MANY LASER SCORCHES!\n A DOOR IN THE {ACTION|EAST|EAST} WALL HAS &quot;KEEP OUT OR DIE&quot; WRITTEN ON IT IN LARGE LETTERS.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ ALCOHOL RECLAMATION AREA\n {CLASS|tab|{ACTION|EAST|EAST}}▸ UNKNOWN\n {CLASS|tab|{ACTION|WEST|WEST}}▸ GLASS STREET\n\n <hr />";
locations[7] = "YOU ARE IN THE BAR'S BACKROOM. THE WALLS ARE A DINGY YELLOW COLOUR AND ON THE FLOOR LIES AN OLD RUG. THE ROOM IS NOT VERY WELL LIT BUT YOU CAN SEE A SMALL TABLE AND A CHAIR IN THE CORNER.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ {CLASS|name|METALON BAR}\n\n <hr />";
locations[8] = "YOU ARE IN THE DISINTEGRATOR ROOM AT THE\n {CLASS|center|W R O N G  &nbsp;T I M E ! ! !}\n YOU ARE BEING DISINTEGRATED…\n\n… OH DEAR!\n\n";
locations[9] = "YOU ARE IN A LARGE DOME-SHAPED HALL. THE DOME ITSELF IS MADE OF A STRANGE GLASS WHICH SEEMS TO BE CONTINUALLY CHANGING COLOUR. THERE ARE SOME SYNTHETIC PLANTS HOVERING ON A FORCE-FIELD ABOVE. THE FLOOR IS MADE OF SHINING BLACK TILES.\n\n EXITS\n {CLASS|tab|{ACTION|UP|UP}}▸ GLASS STREET\n {CLASS|tab|{ACTION|WEST|WEST}}▸ WARNING DISINTEGRATOR!\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ TELEPORT UNIT\n\n <hr />";
locations[10] = "YOU ARE IN A LONG TUNNEL-WALK. IT IS LIKE A MASSIVE TUBE WITH A SLIGHTLY FLATTENED BASE FOR WALKING ON. THE TRANSLUCENT WALLS LET IT BE LIT FROM OUTSIDE WITH A FAINT PINKISH GLOW.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ GLASS STREET\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ SECURITY CHECKPOINT\n\n <hr />";
locations[11] = "YOU ARE IN THE ALCOHOL RECLAMATION CENTRE, OR AS IT IS KNOWN ON EARTH, THE  LOO. SOFT MUSIC IS BEING PLAYED TO HELP YOU RELAX!\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ {CLASS|name|METALON BAR}\n\n <hr />";
locations[12] = "YOU ARE IN THE DEFENCE / COMMUNICATIONS TOWER. HERE, ULTRA-COMPLEX TRACKING DEVICES CAN LOCK ONTO ANY ENEMY SPACECRAFT AND BLOW IT TO BITS IN A MATTER OF NANOSECONDS.\n\n EXITS\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ SECURITY CHECKPOINT\n\n <hr />";
locations[13] = "YOU ARE IN A SMALL {ACTION|TELEPORT|TELEPORT} UNIT IN THE DOME-SHAPED HALL. THE UNIT APPEARS TO BE MADE OF SOME KIND OF INDESTRUCTIBLE MATERIAL.\n\n EXITS\n {CLASS|bigtab|{ACTION|NORTH|NORTH}}▸ DOMED HALL\n {CLASS|bigtab|{ACTION|TELEPORT|TELEPORT}}▸ UNKNOWN\n\n <hr />";
locations[14] = "YOU ARE AT THE SECURITY CHECKPOINT. YOU CAN SEE THROUGH 2 HUGE GATES, THE EDGE OF THE CITY. SOME STAIRS LEAD {ACTION|UP|UP}. THERE IS ALSO A GUARD'S HUT HERE.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ TUNNELWALK\n {CLASS|tab|{ACTION|EAST|EAST}}▸ GATES AND BEYOND\n {CLASS|tab|{ACTION|UP|UP}}▸ UNKNOWN\n\n <hr />";
locations[15] = "YOU ARE IN A BARREN WASTELAND. THE GROUND IS GREY EARTH, TOTALLY INFERTILE. THE LANDSCAPE IS THAT OF LOW DREARY HILLS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|EAST|EAST}}▸ UNKNOWN\n {CLASS|tab|{ACTION|WEST|WEST}}▸ UNKNOWN\n\n <hr />";
locations[16] = "YOU ARE STANDING BY AN OLD METAL DOME STICKING OUT OF THE GROUND. A SLOT OPENS AND A SYNTHESIZED VOICE SAYS, &quot;Insert pass card for entry.&quot;\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|EAST|EAST}}▸ UNKNOWN\n {CLASS|tab|{ACTION|WEST|WEST}}▸ UNKNOWN\n\n <hr />";
locations[17] = "YOU ARE IN A LONG, WARM ROOM. IN THE FLOOR IS A FROTHING JACCUZI. THE TILES ROUND ABOUT IT ALL BEAR THE EMBLEM,\n  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\n  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ■■■■■■■\n  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;■ ■■■■■ ■\n  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ■ ■■■ ■\n  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;■■■■■\n  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ■■■\n  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;■\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ LIVING QUARTERS\n <hr />";
locations[18] = "YOU ARE IN COMFORTABLE LIVING QUARTERS.THERE IS A SUNKEN AREA IN THE MIDDLE OF THE FLOOR, WITH SEATS ROUND ITS EDGE.\n THERE IS A FOOD DISPENSER ON THE WALL. IT SAYS, &quot;Hi there. It is my great honour and pleasure to provide you with consumables.&quot;\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ SLEEP AREA\n {CLASS|tab|{ACTION|EAST|EAST}}▸ COMPUTER ROOM\n {CLASS|tab|{ACTION|WEST|WEST}}▸ JACCUZI\n\n <hr />";
locations[19] = "YOU ARE IN A DUSTY COMPUTER ROOM. ONE OF THE TERMINALS IS STILL WORKING AND EMITS A SOFT HUMMING NOISE. IT LOOKS LIKE AN OLD SINITRAN MODEL WITH A NEW REPLACEMENT KEYBOARD.\nTO THE EAST IS A MASSIVE SECU-VAULT BEHIND A DOOR.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|EAST|EAST}}▸ VAULT\n {CLASS|tab|{ACTION|WEST|WEST}}▸ LIVING QUARTERS\n\n <hr />";
locations[20] = "YOU ARE IN THE SECURITY VAULT. THERE IS A METAL TABLE IN THE MIDDLE OF THE FLOOR. THE WHOLE ROOM IS PAINTED GOLD.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ COMPUTER ROOM\n\n <hr />";
locations[21] = "YOU ARE IN THE SLEEP AREA. THERE ARE SEVERAL MATTRESSES LYING ON THE FLOOR, FIRMLY SECURED. ABOVE, A SOFT BLUE LIGHT PULSES BRIGHT, DIM, THEN BRIGHT AGAIN.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ COMPUTER ROOM\n\n <hr />";
locations[22] = "YOU ARE IN THE LASER TURRET OF THE SKYCRUISER. AT THE MOMENT, THE POWERFUL SCANNER/TRACKING SYSTEMS ARE OFF BUT WHEN THEY ARE NEEDED THEY CAN BE LETHAL. LOOKING OUT, YOU HAVE A GREAT VIEW OF YOUR SURROUNDINGS.\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ BRIDGE\n\n <hr />";
locations[23] = "YOU ARE ON THE BRIDGE OF THE META-GALACTIC SKYCRUISER. ALL AROUND YOU, LIGHTS FLICKER ON AND OFF AND SCANNERS EXAMINE THE SURROUNDING AREA FOR ANY POSSIBLE DANGERS.\nTHERE IS A LARGE CONTROL AND SYSTEMS UNIT HERE, INTERFACED TO A COMPUTER AND KEYBOARD.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ GUN TOWER\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ TUBEWALK\n\n <hr />";
locations[24] = "YOU ARE IN THE DIMLY-LIT SLEEP COMPARTMENTS. PALE GREY BEDS ON THE FLOOR ARE BATHED IN A DULL RED LIGHT. A SOFT HUMMING SOUND (USED TO ENCOURAGE SLEEP) CAN BE HEARD.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ LIVING AREAS\n\n <hr />";
locations[25] = "YOU ARE STANDING IN THE SHIP'S TELEPORT UNIT. A CONTROL PANEL IS FLASHING ON THE WALL.\n\n EXITS\n {CLASS|bigtab|{ACTION|TELEPORT|TELEPORT}}▸ NEAREST PLANET\n {CLASS|bigtab|{ACTION|EAST|EAST}}▸ TUBEWALK\n\n <hr />";
locations[26] = "YOU ARE STANDING IN A HEXAGONAL PRISM SHAPED TUBEWALK. THE MAIN DOORS OF THE CRUISER ARE IN THE FLOOR. THEY SEEM TO BE UNDER THE CONTROL OF A SMALL, METALLIC UNIT FIXED TO THE WALL.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ BRIDGE\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ TUBEWAY\n {CLASS|tab|{ACTION|EAST|EAST}}▸ LIVING AREAS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ TELEPORT UNIT\n\n <hr />";
locations[27] = "YOU ARE IN THE CRUISER'S LIVING AREAS. THIS IS WHERE THE CREW OF THE SHIP WOULD SPEND THEIR FREE TIME. THERE ARE SEVERAL BLUE AND SILVER CHAIRS AND IN THE CENTRE OF THE ROOM, A LARGE, HOLOGRAMIC-CHESS GAME IS IN PROGRESS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ SLEEP COMPARTMENTS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ WASHROOM\n {CLASS|tab|{ACTION|WEST|WEST}}▸ TUBEWALK\n\n <hr />";
locations[28] = "YOU ARE IN THE WASHROOM. THE FLOOR IS GLITTER-TILED WITH GREEN OCTAGONS.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ LIVING AREAS\n\n <hr />";
locations[29] = "YOU ARE IN THE CRUISER'S TINY ESCAPE SHIP. A COMPLEX CONTROL PANEL IN FRONT OF YOU FLASHES BRIGHTLY.\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ TUBEWAY\n\n <hr />";
locations[30] = "YOU ARE IN A LONG TUBEWAY WHICH LINKS THE DOCKING BAY WITH THE REST OF THE CRUISER. HARSH, WHITE LIGHTS GLARE DOWN FROM ABOVE.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ TUBEWALK\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ DOCKING BAY\n {CLASS|tab|{ACTION|EAST|EAST}}▸ ENGINE ROOM\n {CLASS|tab|{ACTION|WEST|WEST}}▸ ESCAPE SHIP\n\n <hr />";
locations[31] = "YOU ARE IN THE CRUISER'S ENGINE ROOM. THE FLOOR IS COVERED WITH GRIMY SHEETS OF METAL WHICH RATTLE AS YOU WALK ON THEM.\nHUGE ION-DRIVES HUM ALL AROUND YOU AND THERE IS A DULL BLUE GLOW FROM THE GENERATORS.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ TUBEWAY\n {MESSAGE|61} <hr />";
locations[32] = "YOU ARE IN A HIDDEN COMPARTMENT BENEATH THE ENGINE ROOM FLOOR. THIS PROBABLY USED TO BE USED FOR SMUGGLING.\n\n EXITS\n {CLASS|tab|{ACTION|UP|UP}}▸ ENGINE ROOM\n\n <hr />";
locations[33] = "YOU ARE IN THE CRUISER'S DOCKING BAY, BESIDE THE MASSIVE AIR-LOCK DOORS. HIGH ABOVE YOU,THE ROOF SPARKLES WITH INDICATOR LIGHTS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ TUBEWAY\n\n <hr />";
locations[34] = "YOU ARE IN A FOREST OF HIGH TREES. LIGHT FILTERS THROUGH THE LEAVES AND STRANGE, LONG GRASSES RISE UP TO YOUR WAIST. WIERD ANIMAL SOUNDS CAN BE HEARD ALL AROUND.\n\n EXITS\n ALL DIRECTIONS>UNKNOWN\n\n <hr />";
locations[35] = "YOU ARE IN A JUNGLE CLEARING, CIRCLED ABOUT BY MANY STRAIGHT, TALL TREES. IT IS QUITE HOT AND HUMID HERE.\n\n EXITS\n {ACTION|WEST|WEST}▸ FOREST\n {ACTION|SOUTH|SOUTH}▸ RIVER\n\n <hr />";
locations[36] = "YOU ARE IN A FLATTENED CLEARING. THE GROUND IS STREWN WITH BONES AND PARTIALLY EATEN CREATURES. ALL THE TREES CLOSE BY HAVE HAD THEIR LEAVES TORN OFF.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ FOREST\n {CLASS|tab|{ACTION|EAST|EAST}}▸ RIVER\n {CLASS|tab|{ACTION|WEST|WEST}}▸ UNKNOWN\n\n <hr />";
locations[37] = "YOU ARE STANDING AT THE RIVER,\nA FAST FLOWING TORRENT COMING FROM THE {ACTION|SOUTH|SOUTH}. TO THE {ACTION|EAST|EAST}, YOU CAN SEE SOME OLD HUTS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ JUNGLE CLEARING\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ UPSTREAM\n {CLASS|tab|{ACTION|EAST|EAST}}▸ VILLAGE\n {CLASS|tab|{ACTION|WEST|WEST}}▸ UNKNOWN\n\n <hr />";
locations[38] = "YOU ARE IN THE CENTRE OF THE VILLAGE. THE GROUND HERE IS SANDY AND HAS MANY FOOTPRINTS IN IT. THERE ARE SEVERAL GRASS-ROOFED MUD-HUTS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ HUT\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ ANOTHER HUT\n {CLASS|tab|{ACTION|WEST|WEST}}▸ RIVER\n\n <hr />";
locations[39] = "YOU ARE BESIDE A WATERFALL WHICH FLOWS INTO A SHALLOW POOL. THE RIVER FLOWS {ACTION|NORTH|NORTH}. A PATH LEADS TO THE TOP OF THE WATERFALL.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ RIVER\n {CLASS|tab|{ACTION|UP|UP}}▸ TOP OF WATERFALL\n\n <hr />";
locations[40] = "YOU ARE INSIDE THE HUT. THE EARTH FLOOR HAS SOME STRAW MATTING ON IT AND A HOLE IN THE ROOF SERVES AS A CHIMNEY. ASHES INDICATE THAT THE FIRE WAS LIT IN THE CENTRE OF THE FLOOR.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ OUT\n\n <hr />";
locations[41] = "YOU ARE AT THE EDGE OF A DEEP POOL. THE SURFACE IS RIPPLED NEAR THE CENTRE AND SOME BUBBLES ARE RISING CLOSER TO THE EDGE. THE WHOLE POOL IS BOUNDED BY REEDS.\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ CLEARING\n\n <hr />";
locations[42] = "YOU ARE INSIDE A SMALL MUD-HUT. THE FLOOR IS PLAIN EARTH AND HOLES IN ONE OF THE WALLS SERVE AS WINDOWS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ {ACTION|OUT|OUT}\n\n <hr />";
locations[43] = "YOU ARE IN A CAVE BEHIND THE WATERFALL.THE TORRENTIAL CURTAIN MAKES THE WALLS SPARKLE AS THE SUN FILTERS THROUGH.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ WATERFALL";
locations[44] = "YOU ARE AT THE TOP OF THE HIGH WATERFALL.A STEEP PATH LEADS {ACTION|DOWN|DOWN} TO THE POOL BELOW.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ WATERFALL\n\n <hr />";
locations[45] = "YOU ARE ON A HIGH MOUNTAIN. AN UNCLIMABLE CLIFF BLOCKS YOUR WAY UP AND A WINDING PATH LEADS BACK {ACTION|DOWN|DOWN} FROM HERE.\n\n EXITS\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ FOOT OF MOUNTAIN\n\n <hr />";
locations[46] = "YOU ARE STANDING AT THE FOOT OF A HIGH MOUNTAIN IN THE WRECKAGE OF AN OLD CARGO SHIP. A WINDING PATH LEADS UP THE MOUNTAIN.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ WATERFALL\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|WEST|WEST}}▸ UP THE MOUNTAIN\n\n <hr />";
locations[47] = "YOU ARE IN A HOT DESERT.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ FOOT OF MOUNTAIN\n\n <hr />";
locations[48] = "YOU ARE STANDING IN A ROCKY CANYON. SAND LIES BLOWN AGAINST THE SHEER CLIFF WALLS. THERE IS A CAVE MOUTH TO THE {ACTION|SOUTH|SOUTH}.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ CAVE\n {CLASS|tab|{ACTION|EAST|EAST}}▸ UNKNOWN\n\n <hr />";
locations[49] = "YOU ARE IN A SANDY DESERT PLAIN. LINES OF JAGGED ROCKS STICK UP OUT OF THE SAND. TO THE {ACTION|WEST|WEST}, THE ROCKS RISE INTO A CANYON.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|WEST|WEST}}▸ CANYON\n\n <hr />";
locations[50] = "YOU ARE IN AN EMPTY SCHOOL AREA. THE ROWS OF EMPTY DESKS ARE STREWN WITH FADED BOOKS. AT THE FRONT OF THE ROOM,AN ANCIENT COMPUTER TERMINAL SITS, DEAD.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ MAIN CITY\n\n <hr />";
locations[51] = "YOU ARE IN THE TERMINAN DEFENCE CENTRE. IN THE CENTRE OF THE MAIN CONSOLE, IS A LARGE RED BUTTON. THE COMPUTER SYSTEMS STILL SEEM TO BE WORKING AND AN\n\n  &nbsp; All Systems Operational.\n\n MESSAGE FLASHES ON A VDU SCREEN.\n\n EXITS\n {CLASS|tab|{ACTION|UP|UP}}▸ MAIN CITY\n\n <hr />";
locations[52] = "YOU ARE IN A DARK CAVE, ON A SET OF WINDING STEPS WHICH LEAD AWAY INTO THE DARKNESS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|UP|UP}}▸ UNKNOWN\n\n <hr />";
locations[53] = "YOU ARE IN A BLEAK, BARREN AREA OF LAND. TO THE {ACTION|NORTH|NORTH} IS A DESERT AND TO THE {ACTION|SOUTH|SOUTH} IS A RANGE OF MOUNTAINS. IN THE {ACTION|EAST|EAST} YOU SEE A GREAT CITY.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ DESERT\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ MOUNTAINS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ CITY\n\n <hr />";
locations[54] = "YOU ARE IN THE CENTRE OF A HUGE, BUT DESERTED CITY. A LOW WIND MOANS THROUGH THE PLASTIC TREES AND BITS OF PAPER BLOW ACROSS THE STREET.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ LARGE BUILDING\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ SMALL BUILDINGS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ TELEPORT UNIT\n {CLASS|tab|{ACTION|WEST|WEST}}▸ BLEAK LAND\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ UNDERGROUND BUNKER\n\n <hr />";
locations[55] = "YOU ARE IN A TELEPORT UNIT. ON THE CONTROL PANEL, A SMALL RED LIGHT IS FLASHING.\n\n EXITS\n {CLASS|bigtab|{ACTION|TELEPORT|TELEPORT}}▸ UNKNOWN\n {CLASS|bigtab|{ACTION|WEST|WEST}}▸ MAIN CITY\n\n <hr />";
locations[56] = "YOU FIND YOURSELF ON A HIGH PLATEAU IN THE MOUNTAINS. THE HIGHEST PEAKS ARE TO THE {ACTION|EAST|EAST}, AND IN FRONT OF YOU, SOME STEPS LEAD {ACTION|DOWN|DOWN} INTO THE ROCK.\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ MOUNTAINS\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ UNKNOWN\n\n <hr />";
locations[57] = "YOU ARE IN A RANGE OF HIGH MOUNTAINS.TO THE {ACTION|WEST|WEST} IS A PLATEAU. THERE IS SNOW ON THE GROUND.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ BLEAK LAND\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|WEST|WEST}}▸ PLATEAU\n\n <hr />";
locations[58] = "YOU ARE IN A FAMILY LIVING UNIT. THERE IS A GLASS TABLE AND SOME CHAIRS, ON A THIN, YELLOW CARPET. A TRACE-LAMP ON THE CEILING, FOLLOWS YOU LIKE A SPOTLIGHT AS YOU MOVE ABOUT, ILLUMINATING ONLY THE PART OF THE ROOM YOU ARE IN.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ MAIN CITY\n {CLASS|tab|{ACTION|EAST|EAST}}▸ REST CUBICLES\n\n <hr />";
locations[59] = "YOU ARE IN A ROOM FULL OF REST-CUBICLES. THE ZERO-GRAVITY INSIDE THESE VERTICAL TUBES ALLOWS A BEING TO SLEEP IN COMFORT. THIS ROOM ALSO HAS A TRACE-LAMP.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ LIVING UNIT\n\n <hr />";
locations[60] = "YOU ARE IN A FLAT AREA OF BURNT GRASS. THERE ARE SOME MOUNTAINS TO THE {ACTION|NORTH|NORTH}.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ MOUNTAINS\n Scanners lack sufficient data for print on other directions\n\n <hr />";
locations[61] = "YOU ARE ON THE ROOF OF A MEGA-STORY BUILDING. THE STREETS ARE 1OOO FLOORS DOWN! BESIDE YOU IS A JETLIFT IN WHICH YOU CAN GET {ACTION|DOWN|DOWN} TO STREET LEVEL.\n\n EXITS\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ [IN JETLIFT] GROUND FLOOR\n\n <hr />";
locations[62] = "YOU ARE IN THE SIRUS CYBORNETICS CORPORATION'S ROBOT FACTORY. HERE 2OOOO ANDROIDS ARE MADE PER DAY! HUGE MACHINES ASSEMBLE THE ROBOT AND THEN PLACE IT IN A CARGO-VAC TELEPORT SYSTEM FOR EXPORT.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ WALKWAY\n {CLASS|tab|{ACTION|EAST|EAST}}▸ TRAVEL PORT\n\n <hr />";
locations[63] = "YOU ARE IN A &#34;TransUniverse&#34; TRAVEL-PORT. A ROBOT TRUNDLES AND GIVES YOU A BODY SCAN AND, BEING SATISFIED THAT YOU HAVE NO PETS ON YOUR PERSON, LETS YOU GO FREE. THE M {ACTION|EXAM TRAIN|TRAIN} IS HERE.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ SIRUS' FACTORY\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ PARK\n {CLASS|tab|{ACTION|IN|IN}}▸ [IN TRAIN] CLASSIFIED\n\n <hr />";
locations[64] = "YOU ARE BY THE JETLIFT IN THE FOYER OF A MEGA-STORY BUILDING. TO THE {ACTION|SOUTH|SOUTH}, THE MAIN DOORS OPEN OUT ONTO THE STREET.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ STREET\n {CLASS|tab|{ACTION|UP|UP}}▸ [IN JETLIFT] TO ROOF\n\n <hr />";
locations[65] = "YOU ARE IN THE OFFICES OF THE LAW-ENFORCEMENT-SQUAD. SPRAYED ON THE WALL, IN GREEN PAINT, IS THE MESSAGE:\n\n  &nbsp;BeWarE Of tHe FiLTh!!!!\n\n THE WALLS ARE BLOODSTAINED AND FULL OF KNIFE MARKS!\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ OUT\n\n <hr />";
locations[66] = "YOU ARE ON A WHITE WALKWAY WHICH CURVES THROUGH THE CITY. TO THE {ACTION|EAST|EAST} IS THE PARK. ANALYSIS SHOWS THAT THE WHITE MATERIAL IS DYED GOLD!!!\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ SIRUS' FACTORY\n {CLASS|tab|{ACTION|EAST|EAST}}▸ PARK\n\n <hr />";
locations[67] = "YOU ARE IN THE CITY PARK. A SIGN HERE READS:\n\n {CLASS|center|PEOPLE WHO DROP LITTER WILL BE SHOT}\n THE SYNTHETIC TREES SWAY GENTLY IN THE BREEZE.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ TRAVEL-PORT\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ LIVING COMPLEX\n {CLASS|tab|{ACTION|EAST|EAST}}▸ STREET\n {CLASS|tab|{ACTION|WEST|WEST}}▸ WALKWAY\n\n <hr />";
locations[68] = "YOU ARE ON A STRANGE STREET (ANALYSIS SHOWS IT IS MADE OF PLATINUM!) NEAR THE PARK.\nTO THE {ACTION|NORTH|NORTH} IS A BUILDING WHO'S TOP IS ABOVE THE PALE GREEN CLOUDS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ BUILDING\n {CLASS|tab|{ACTION|EAST|EAST}}▸ UNKNOWN\n {CLASS|tab|{ACTION|WEST|WEST}}▸ PARK\n\n <hr />";
locations[69] = "YOU ARE OUTSIDE THE SPORTCENTRE. A SIGN ON THE WALL READS:\n\n {CLASS|center|WARNING! DO NOT ENTER WHILE GAMES ARE IN PROGRESS.}\n\n TO THE {ACTION|NORTH|NORTH} IS A GREY BUILDING BELONGING TO THE LAW ENFORCEMEMT SQUAD.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ GREY BUILDING\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ SPORT ARENA\n {CLASS|tab|{ACTION|EAST|EAST}}▸ SPORT STADIUM\n {CLASS|tab|{ACTION|WEST|WEST}}▸ STREET\n\n <hr />";
locations[70] = "YOU ARE IN THE SPORT STADIUM. BELOW YOU, IN THE ARENA, THE LASERGAMES ARE GOING ON. A BALL OF SOLID, LASER GENERATED ENERGY IS PASSED BETWEEN THE 2 PLAYERS WHO RETURN IT USING HAND-HELD FORCE-FIELDS. (VERY DANGEROUS!)\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ CHANGING ROOMS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ OUT\n\n <hr />";
locations[71] = "YOU ARE IN A SMALL HOUSE. THERE IS ONLY ONE ROOM. THE FLOOR AND WALLS ARE PALE BLUE,THE CEILING BEING WHITE. IN THE CENTRE OF THE ROOM IS A RED, PLASTIC TABLE WITH SOME CHAIRS.\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ OUT\n\n <hr />";
locations[72] = "YOU ARE AT THE HUB OF THE LIVING COMPLEX. ALL AROUND ARE HOUSES EXCEPT {ACTION|NORTH|NORTH} WHERE THE PARK IS.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ PARK\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ HOUSE\n {CLASS|tab|{ACTION|EAST|EAST}}▸ HOUSE\n {CLASS|tab|{ACTION|WEST|WEST}}▸ HOUSE\n\n <hr />";
locations[73] = "YOU ARE IN A SMALL, ONE-ROOMED HOUSE.THE WALLS AND CARPET ARE PALE BLUE AND THE CEILING IS A FADED WHITE COLOUR. IN THE CENTRE OF THE ROOM ARE A SET OF CHAIRS AND A RED PLASTIC TABLE.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ OUT\n\n <hr />";
locations[74] = "YOU ARE IN THE LASERGAMES ARENA. THE WHOLE PLACE IS DESERTED BUT THE GROUND IS SCORCHED AND STILL SMOULDERING!HIGH ABOVE, ON THE DOMED CEILING,THE SCOREBOARD READS:\n {CLASS|center|ZAXY 93 / KYSEL 58}\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ OUT\n {CLASS|tab|{ACTION|EAST|EAST}}▸ CHANGING ROOMS\n\n <hr />";
locations[75] = "YOU ARE IN THE SHOWERS. THERE ARE SEVERAL SHOWER-HEADS IN A ROW ON THE WALL HERE.\n\n EXITS\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ CHANGING ROOMS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ ARENA\n\n <hr />";
locations[76] = "YOU ARE IN A SMALL HOUSE. THE SINGLE ROOM HAS A BLUE FLOOR AND WALLS, THE CEILING WHITE WITH GRIMY PATCHES. THERE IS A RED, PLASTIC TABLE AND CHAIRS IN THE CENTRE OF THE ROOM.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ OUT\n\n <hr />";
locations[77] = "YOU ARE IN THE CHANGING ROOMS IN THE SPORTCENTRE. THRE ARE SEVERAL LOCKERS AND BENCHES HERE. SOME STAIRS LEAD {ACTION|UP|UP} TO THE STADIUM.\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ SHOWERS\n {CLASS|tab|{ACTION|UP|UP}}▸ STADIUM\n\n <hr />";
locations[78] = "YOU ARE AT AN OLD MINING STATION IN THE BADLANDS. THE M {ACTION|EXAM TRAIN|TRAIN} IS AT THE PLATFORM AND TO THE {ACTION|EAST|EAST} YOU CAN SEE THE MINE.\n\n EXITS\n {CLASS|tab|{ACTION|IN|IN}}▸ [IN TRAIN] TRAVEL-PORT\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ UNKNOWN\n {CLASS|tab|{ACTION|EAST|EAST}}▸ MINE\n\n <hr />";
locations[79] = "YOU ARE IN THE MINE. TO THE {ACTION|WEST|WEST}, YOU CAN SEE RAILS LEADING OUT INTO THE LIGHT. IT IS DARK.\n\n EXITS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ OUT\n\n <hr />";
locations[80] = "YOU ARE IN A DESERT IN THE EVIL BADLANDS. THE GREY SAND BLOWS ABOUT YOUR FEET. THE SKY IS GOING BLACK!\n\n EXITS\n Scanners unoperational due to sandstorm.\n\n <hr />";
locations[81] = "YOU ARE IN A AUTO-TRAK LASER TURRET. YOU ARE UNABLE TO USE THE WEAPONS BECAUSE THEY ARE IN MODE &quot;auto&quot;. THROUGH THE PROTECTIVE PERSPEX DOME, YOU CAN SEE THE STARS AND YOUR STARCRUISER, DOCKED.\n\n EXITS\n {CLASS|tab|{ACTION|DOWN|DOWN}}▸ DOCKING BAY\n\n <hr />";
locations[82] = "YOU ARE IN THE DOCKING BAY OF THE SPACE STATION. TO THE {ACTION|NORTH|NORTH} YOU CAN SEE YOUR STARCRUISER. A SIGN READS:\n\n {CLASS|center|WARNING! PERSONNEL NOW PASSING THROUGH AIRLOCK MAY CAUSE DOCKING TERMINATION.}\n\n EXITS\n {CLASS|tab|{ACTION|NORTH|NORTH}}▸ STARCRUISER\n {CLASS|tab|{ACTION|SOUTH|SOUTH}}▸ TUBEWALK\n {CLASS|tab|{ACTION|EAST|EAST}}▸ LIFE SUPPORT SYSTEMS\n {CLASS|tab|{ACTION|WEST|WEST}}▸ LIVING COMPLEX\n {CLASS|tab|{ACTION|UP|UP}}▸ LASER TURRET\n\n <hr />";
locations[83] = "YOU ARE IN THE CONTROL CENTRE OF THE LIFE-SUPPORT SYSTEMS. BRIGHT LIGHTS FLASH ON AND OFF, DAZZLING YOU.\n\n EXITS\n {CLASS|tab|{ACTION|EAST|EAST}}▸ POWER CENTRE\n {CLASS|tab|{ACTION|WEST|WEST}}▸ DOCKING BAY\n\n <hr />";
locations[84] = "YOU ARE IN THE SPACE STATION'S POWER CENTRE.\n\n SCANNER MALFUNCTION";
locations[85] = "YOU ARE IN THE SLEEP AREA. A SEA OF CYAN MATTRESSES STRETCHES OUT BEFORE YOU.\n\n SCANNER MALFUNCTION";
locations[86] = "YOU ARE IN THE FOOD CENTRE. ROWS OF RED TABLES SIT EMPTY.\n\n SCANNER MALFUNCTION";
locations[87] = "YOU ARE IN A LONG TUBE-LIKE WALKWAY.\n\n SCANNER MALFUNCTION";
locations[88] = "YOU ARE IN THE CIVILIAN LIVING COMPLEX, AN OPEN AREA OF NO REAL PURPOSE.\n\n SCANNER MALFUNCTION";
locations[89] = "YOU ARE IN A LONG, TUBE WALKWAY.\n\n SCANNER MALFUNCTION";
locations[90] = "YOU ARE IN THE CENTRAL HUB OF THE SPACE STATION. YOU FEEL VERY LIGHT HERE DUE TO THE LOW DEGREE OF ARTIFICIAL GRAVITY.\n\n SCANNER MALFUNCTION";
locations[91] = "YOU ARE IN A LONG, TUBE WALKWAY.\n\n SCANNER MALFUNCTION";
locations[92] = "YOU ARE IN THE PERSONNEL LIVING QUARTERS.\n\n SCANNER MALFUNCTION";
locations[93] = "YOU ARE IN A LONG, TUBE WALKWAY.\n\n SCANNER MALFUNCTION";
locations[94] = "YOU ARE IN THE CENTRAL CONTROL CENTRE. STRANGE CONSOLES COVER THE CYAN WALLS.\n\n SCANNER MALFUNCTION ";
locations[95] = "YOU ARE IN THE HUGE ARTIFICIAL GARDENS, WALKING BETWEEN GREEN PLANTS UNDER A STARRY DOME.\n\n SCANNER MALFUNCTION ";
locations[96] = "YOU ARE IN THE RECREATION CENTRE OF THE SPACE STATION, A COMPLEX OF SPORTS HALLS.\n\n SCANNER MALFUNCTION ";
locations[97] = "YOU ARE IN A CAVE OF ICE.\n\n {CLASS|scanner|SCANNER FROZEN }";
locations[98] = "YOU ARE IN THE CENTRE OF A FROZEN LAKE.\n\n {CLASS|scanner|SCANNER FROZEN }";
locations[99] = "YOU ARE IN THE DEPTHS OF AN ICY VALLEY.\n\n {CLASS|scanner|SCANNER FROZEN }";
locations[100] = "YOU ARE IN THE REMAINS OF A DESOLATE POLAR VILLAGE.\n\n {CLASS|scanner|SCANNER FROZEN }";
locations[101] = "YOU ARE IN AN IGLOO.\n\n {CLASS|scanner|SCANNER FROZEN }";
locations[102] = "";

// CONNECTIONS

connections = [];
connections_start = [];

connections[0] = [ -1, -1, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[1] = [ -1, -1, -1, 0, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[2] = [ -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[3] = [ -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, 23, -1, -1, -1, -1 ];
connections[4] = [ -1, -1, -1, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[5] = [ -1, 0, 10, 6, 4, -1, -1, -1, -1, -1, 9, -1, -1, -1, -1, -1 ];
connections[6] = [ -1, -1, 11, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[7] = [ -1, -1, -1, -1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[8] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[9] = [ -1, -1, 13, -1, 8, -1, -1, -1, -1, 5, -1, -1, -1, -1, -1, -1 ];
connections[10] = [ -1, 5, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[11] = [ -1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[12] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1, -1, -1, -1 ];
connections[13] = [ -1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[14] = [ -1, 10, -1, 15, -1, -1, -1, -1, -1, 12, -1, -1, -1, -1, -1, -1 ];
connections[15] = [ -1, 15, -1, 15, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[16] = [ -1, 15, -1, 15, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[17] = [ -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[18] = [ -1, -1, 21, 19, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[19] = [ -1, 16, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[20] = [ -1, -1, -1, -1, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[21] = [ -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[22] = [ -1, -1, -1, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[23] = [ -1, -1, 26, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[24] = [ -1, -1, 27, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[25] = [ -1, -1, -1, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[26] = [ -1, 23, 30, 27, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[27] = [ -1, 24, -1, 28, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[28] = [ -1, -1, -1, -1, 27, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[29] = [ -1, -1, -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[30] = [ -1, 26, 33, 31, 29, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[31] = [ -1, -1, -1, -1, 30, -1, -1, -1, -1, -1, 32, -1, -1, -1, -1, -1 ];
connections[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 31, -1, -1, -1, -1, -1, -1 ];
connections[33] = [ -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[34] = [ -1, 34, 36, 35, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[35] = [ -1, -1, 37, -1, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[36] = [ -1, 34, -1, 37, 41, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[37] = [ -1, 35, 39, 38, 36, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[38] = [ -1, 40, 42, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[39] = [ -1, 37, 43, -1, -1, -1, -1, -1, -1, 44, -1, -1, -1, 43, -1, -1 ];
connections[40] = [ -1, -1, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, 38, -1, -1, -1 ];
connections[41] = [ -1, -1, -1, 36, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[42] = [ -1, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 38, -1, -1, -1 ];
connections[43] = [ -1, 39, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1 ];
connections[44] = [ -1, -1, 46, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1, -1, -1 ];
connections[45] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 46, -1, -1, -1, -1, -1 ];
connections[46] = [ -1, 44, 47, -1, 45, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[47] = [ -1, 46, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[48] = [ -1, -1, 52, 49, -1, -1, -1, -1, -1, -1, -1, 52, -1, -1, -1, -1 ];
connections[49] = [ -1, -1, 53, -1, 48, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[50] = [ -1, -1, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, 54, -1, -1, -1 ];
connections[51] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1 ];
connections[52] = [ -1, 48, -1, -1, -1, -1, -1, -1, -1, 56, -1, -1, -1, -1, -1, -1 ];
connections[53] = [ -1, 49, 57, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[54] = [ -1, 50, 58, 55, 53, -1, -1, -1, -1, -1, 51, -1, -1, -1, -1, -1 ];
connections[55] = [ -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[56] = [ -1, -1, -1, 57, -1, -1, -1, -1, -1, -1, 52, -1, -1, -1, -1, -1 ];
connections[57] = [ -1, 53, 60, -1, 56, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[58] = [ -1, 54, -1, 59, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[59] = [ -1, -1, -1, -1, 58, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[60] = [ -1, 57, 60, 60, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[61] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 64, -1, -1, -1, -1, -1 ];
connections[62] = [ -1, -1, 66, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[63] = [ -1, 62, 67, -1, -1, -1, -1, -1, -1, -1, -1, 78, -1, -1, -1, -1 ];
connections[64] = [ -1, -1, 68, -1, -1, -1, -1, -1, -1, 61, -1, -1, 68, -1, -1, -1 ];
connections[65] = [ -1, -1, 69, -1, -1, -1, -1, -1, -1, -1, -1, -1, 69, -1, -1, -1 ];
connections[66] = [ -1, 62, -1, 67, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[67] = [ -1, 63, 72, 68, 66, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[68] = [ -1, 64, -1, 69, 67, -1, -1, -1, -1, -1, -1, 64, -1, -1, -1, -1 ];
connections[69] = [ -1, 65, 74, 70, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[70] = [ -1, -1, -1, 77, 69, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[71] = [ -1, -1, -1, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[72] = [ -1, 67, 76, 73, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[73] = [ -1, -1, -1, -1, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[74] = [ -1, 69, -1, 75, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[75] = [ -1, -1, 77, -1, 74, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[76] = [ -1, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[77] = [ -1, 75, -1, -1, -1, -1, -1, -1, -1, 70, -1, -1, -1, -1, -1, -1 ];
connections[78] = [ -1, -1, 80, 79, -1, -1, -1, -1, -1, -1, -1, 63, -1, -1, -1, -1 ];
connections[79] = [ -1, -1, -1, -1, 78, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[80] = [ -1, 78, 80, 80, 80, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[81] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 82, -1, -1, -1, -1, -1 ];
connections[82] = [ -1, 33, 87, 83, 88, -1, -1, -1, -1, 81, -1, -1, -1, -1, -1, -1 ];
connections[83] = [ -1, -1, -1, 84, 82, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[84] = [ -1, -1, -1, 84, 83, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[85] = [ -1, -1, 86, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[86] = [ -1, 85, 92, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[87] = [ -1, 82, 90, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[88] = [ -1, 82, 95, 89, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[89] = [ -1, -1, -1, 90, 88, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[90] = [ -1, 87, 93, 91, 89, -1, -1, -1, -1, 94, -1, -1, -1, -1, -1, -1 ];
connections[91] = [ -1, -1, -1, 92, 90, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[92] = [ -1, 86, 96, -1, 91, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[93] = [ -1, 90, 96, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[94] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 90, -1, -1, -1, -1, -1 ];
connections[95] = [ -1, 88, -1, 96, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[96] = [ -1, 93, -1, 92, 95, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[97] = [ -1, -1, -1, 98, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[98] = [ -1, -1, -1, 99, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[99] = [ -1, -1, -1, 100, 98, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections[100] = [ -1, 101, -1, -1, 99, -1, -1, -1, -1, -1, -1, 101, -1, -1, -1, -1 ];
connections[101] = [ -1, -1, 100, -1, -1, -1, -1, -1, -1, -1, -1, -1, 100, -1, -1, -1 ];
connections[102] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];

connections_start[0] = [ -1, -1, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[1] = [ -1, -1, -1, 0, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[2] = [ -1, -1, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[3] = [ -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, 23, -1, -1, -1, -1 ];
connections_start[4] = [ -1, -1, -1, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[5] = [ -1, 0, 10, 6, 4, -1, -1, -1, -1, -1, 9, -1, -1, -1, -1, -1 ];
connections_start[6] = [ -1, -1, 11, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[7] = [ -1, -1, -1, -1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[8] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[9] = [ -1, -1, 13, -1, 8, -1, -1, -1, -1, 5, -1, -1, -1, -1, -1, -1 ];
connections_start[10] = [ -1, 5, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[11] = [ -1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[12] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 14, -1, -1, -1, -1, -1 ];
connections_start[13] = [ -1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[14] = [ -1, 10, -1, 15, -1, -1, -1, -1, -1, 12, -1, -1, -1, -1, -1, -1 ];
connections_start[15] = [ -1, 15, -1, 15, 14, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[16] = [ -1, 15, -1, 15, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[17] = [ -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[18] = [ -1, -1, 21, 19, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[19] = [ -1, 16, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[20] = [ -1, -1, -1, -1, 19, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[21] = [ -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[22] = [ -1, -1, -1, 23, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[23] = [ -1, -1, 26, -1, 22, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[24] = [ -1, -1, 27, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[25] = [ -1, -1, -1, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[26] = [ -1, 23, 30, 27, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[27] = [ -1, 24, -1, 28, 26, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[28] = [ -1, -1, -1, -1, 27, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[29] = [ -1, -1, -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[30] = [ -1, 26, 33, 31, 29, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[31] = [ -1, -1, -1, -1, 30, -1, -1, -1, -1, -1, 32, -1, -1, -1, -1, -1 ];
connections_start[32] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 31, -1, -1, -1, -1, -1, -1 ];
connections_start[33] = [ -1, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[34] = [ -1, 34, 36, 35, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[35] = [ -1, -1, 37, -1, 34, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[36] = [ -1, 34, -1, 37, 41, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[37] = [ -1, 35, 39, 38, 36, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[38] = [ -1, 40, 42, -1, 37, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[39] = [ -1, 37, 43, -1, -1, -1, -1, -1, -1, 44, -1, -1, -1, 43, -1, -1 ];
connections_start[40] = [ -1, -1, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, 38, -1, -1, -1 ];
connections_start[41] = [ -1, -1, -1, 36, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[42] = [ -1, 38, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 38, -1, -1, -1 ];
connections_start[43] = [ -1, 39, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1 ];
connections_start[44] = [ -1, -1, 46, -1, -1, -1, -1, -1, -1, -1, 39, -1, -1, -1, -1, -1 ];
connections_start[45] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 46, -1, -1, -1, -1, -1 ];
connections_start[46] = [ -1, 44, 47, -1, 45, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[47] = [ -1, 46, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[48] = [ -1, -1, 52, 49, -1, -1, -1, -1, -1, -1, -1, 52, -1, -1, -1, -1 ];
connections_start[49] = [ -1, -1, 53, -1, 48, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[50] = [ -1, -1, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, 54, -1, -1, -1 ];
connections_start[51] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1 ];
connections_start[52] = [ -1, 48, -1, -1, -1, -1, -1, -1, -1, 56, -1, -1, -1, -1, -1, -1 ];
connections_start[53] = [ -1, 49, 57, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[54] = [ -1, 50, 58, 55, 53, -1, -1, -1, -1, -1, 51, -1, -1, -1, -1, -1 ];
connections_start[55] = [ -1, -1, -1, -1, 54, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[56] = [ -1, -1, -1, 57, -1, -1, -1, -1, -1, -1, 52, -1, -1, -1, -1, -1 ];
connections_start[57] = [ -1, 53, 60, -1, 56, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[58] = [ -1, 54, -1, 59, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[59] = [ -1, -1, -1, -1, 58, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[60] = [ -1, 57, 60, 60, 60, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[61] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 64, -1, -1, -1, -1, -1 ];
connections_start[62] = [ -1, -1, 66, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[63] = [ -1, 62, 67, -1, -1, -1, -1, -1, -1, -1, -1, 78, -1, -1, -1, -1 ];
connections_start[64] = [ -1, -1, 68, -1, -1, -1, -1, -1, -1, 61, -1, -1, 68, -1, -1, -1 ];
connections_start[65] = [ -1, -1, 69, -1, -1, -1, -1, -1, -1, -1, -1, -1, 69, -1, -1, -1 ];
connections_start[66] = [ -1, 62, -1, 67, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[67] = [ -1, 63, 72, 68, 66, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[68] = [ -1, 64, -1, 69, 67, -1, -1, -1, -1, -1, -1, 64, -1, -1, -1, -1 ];
connections_start[69] = [ -1, 65, 74, 70, 68, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[70] = [ -1, -1, -1, 77, 69, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[71] = [ -1, -1, -1, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[72] = [ -1, 67, 76, 73, 71, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[73] = [ -1, -1, -1, -1, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[74] = [ -1, 69, -1, 75, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[75] = [ -1, -1, 77, -1, 74, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[76] = [ -1, 72, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[77] = [ -1, 75, -1, -1, -1, -1, -1, -1, -1, 70, -1, -1, -1, -1, -1, -1 ];
connections_start[78] = [ -1, -1, 80, 79, -1, -1, -1, -1, -1, -1, -1, 63, -1, -1, -1, -1 ];
connections_start[79] = [ -1, -1, -1, -1, 78, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[80] = [ -1, 78, 80, 80, 80, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[81] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 82, -1, -1, -1, -1, -1 ];
connections_start[82] = [ -1, 33, 87, 83, 88, -1, -1, -1, -1, 81, -1, -1, -1, -1, -1, -1 ];
connections_start[83] = [ -1, -1, -1, 84, 82, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[84] = [ -1, -1, -1, 84, 83, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[85] = [ -1, -1, 86, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[86] = [ -1, 85, 92, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[87] = [ -1, 82, 90, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[88] = [ -1, 82, 95, 89, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[89] = [ -1, -1, -1, 90, 88, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[90] = [ -1, 87, 93, 91, 89, -1, -1, -1, -1, 94, -1, -1, -1, -1, -1, -1 ];
connections_start[91] = [ -1, -1, -1, 92, 90, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[92] = [ -1, 86, 96, -1, 91, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[93] = [ -1, 90, 96, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[94] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 90, -1, -1, -1, -1, -1 ];
connections_start[95] = [ -1, 88, -1, 96, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[96] = [ -1, 93, -1, 92, 95, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[97] = [ -1, -1, -1, 98, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[98] = [ -1, -1, -1, 99, 97, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[99] = [ -1, -1, -1, 100, 98, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
connections_start[100] = [ -1, 101, -1, -1, 99, -1, -1, -1, -1, -1, -1, 101, -1, -1, -1, -1 ];
connections_start[101] = [ -1, -1, 100, -1, -1, -1, -1, -1, -1, -1, -1, -1, 100, -1, -1, -1 ];
connections_start[102] = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];


resources=[];
resources.push([RESOURCE_TYPE_IMG, 16, "dat/location16.svg"]);
resources.push([RESOURCE_TYPE_IMG, 21, "dat/location21.svg"]);
resources.push([RESOURCE_TYPE_IMG, 26, "dat/location26.svg"]);
resources.push([RESOURCE_TYPE_IMG, 35, "dat/location35.svg"]);
resources.push([RESOURCE_TYPE_IMG, 43, "dat/location43.svg"]);
resources.push([RESOURCE_TYPE_IMG, 49, "dat/location49.svg"]);
resources.push([RESOURCE_TYPE_IMG, 51, "dat/location51.svg"]);
resources.push([RESOURCE_TYPE_IMG, 61, "dat/location61.svg"]);
resources.push([RESOURCE_TYPE_IMG, 66, "dat/location66.svg"]);
resources.push([RESOURCE_TYPE_IMG, 67, "dat/location67.svg"]);
resources.push([RESOURCE_TYPE_IMG, 81, "dat/location81.svg"]);
resources.push([RESOURCE_TYPE_IMG, 95, "dat/location95.svg"]);
resources.push([RESOURCE_TYPE_IMG, 98, "dat/location98.svg"]);
resources.push([RESOURCE_TYPE_IMG, 102, "dat/location102.png"]);


 //OBJECTS

objects = [];
objectsAttrLO = [];
objectsAttrHI = [];
objectsLocation = [];
objectsNoun = [];
objectsAdjective = [];
objectsWeight = [];
objectsAttrLO_start = [];
objectsAttrHI_start = [];
objectsLocation_start = [];
objectsWeight_start = [];

objects[0] = "A LASER PROBE";
objectsNoun[0] = 255;
objectsAdjective[0] = 255;
objectsLocation[0] = 1;
objectsLocation_start[0] = 1;
objectsWeight[0] = 1;
objectsWeight_start[0] = 1;
objectsAttrLO[0] = 1;
objectsAttrLO_start[0] = 1;
objectsAttrHI[0] = 0;
objectsAttrHI_start[0] = 0;

objects[1] = "SOME FOOD";
objectsNoun[1] = 255;
objectsAdjective[1] = 255;
objectsLocation[1] = 4;
objectsLocation_start[1] = 4;
objectsWeight[1] = 1;
objectsWeight_start[1] = 1;
objectsAttrLO[1] = 0;
objectsAttrLO_start[1] = 0;
objectsAttrHI[1] = 0;
objectsAttrHI_start[1] = 0;

objects[2] = "SOME FOOD";
objectsNoun[2] = 255;
objectsAdjective[2] = 255;
objectsLocation[2] = 7;
objectsLocation_start[2] = 7;
objectsWeight[2] = 1;
objectsWeight_start[2] = 1;
objectsAttrLO[2] = 0;
objectsAttrLO_start[2] = 0;
objectsAttrHI[2] = 0;
objectsAttrHI_start[2] = 0;

objects[3] = "SOME FOOD";
objectsNoun[3] = 255;
objectsAdjective[3] = 255;
objectsLocation[3] = 252;
objectsLocation_start[3] = 252;
objectsWeight[3] = 1;
objectsWeight_start[3] = 1;
objectsAttrLO[3] = 0;
objectsAttrLO_start[3] = 0;
objectsAttrHI[3] = 0;
objectsAttrHI_start[3] = 0;

objects[4] = "SOME FOOD";
objectsNoun[4] = 255;
objectsAdjective[4] = 255;
objectsLocation[4] = 14;
objectsLocation_start[4] = 14;
objectsWeight[4] = 1;
objectsWeight_start[4] = 1;
objectsAttrLO[4] = 0;
objectsAttrLO_start[4] = 0;
objectsAttrHI[4] = 0;
objectsAttrHI_start[4] = 0;

objects[5] = "SOME FOOD";
objectsNoun[5] = 255;
objectsAdjective[5] = 255;
objectsLocation[5] = 27;
objectsLocation_start[5] = 27;
objectsWeight[5] = 1;
objectsWeight_start[5] = 1;
objectsAttrLO[5] = 0;
objectsAttrLO_start[5] = 0;
objectsAttrHI[5] = 0;
objectsAttrHI_start[5] = 0;

objects[6] = "SOME FOOD";
objectsNoun[6] = 255;
objectsAdjective[6] = 255;
objectsLocation[6] = 40;
objectsLocation_start[6] = 40;
objectsWeight[6] = 1;
objectsWeight_start[6] = 1;
objectsAttrLO[6] = 0;
objectsAttrLO_start[6] = 0;
objectsAttrHI[6] = 0;
objectsAttrHI_start[6] = 0;

objects[7] = "SOME FOOD";
objectsNoun[7] = 255;
objectsAdjective[7] = 255;
objectsLocation[7] = 73;
objectsLocation_start[7] = 73;
objectsWeight[7] = 1;
objectsWeight_start[7] = 1;
objectsAttrLO[7] = 0;
objectsAttrLO_start[7] = 0;
objectsAttrHI[7] = 0;
objectsAttrHI_start[7] = 0;

objects[8] = "SOME FOOD";
objectsNoun[8] = 255;
objectsAdjective[8] = 255;
objectsLocation[8] = 86;
objectsLocation_start[8] = 86;
objectsWeight[8] = 1;
objectsWeight_start[8] = 1;
objectsAttrLO[8] = 0;
objectsAttrLO_start[8] = 0;
objectsAttrHI[8] = 0;
objectsAttrHI_start[8] = 0;

objects[9] = "A DEAD PLANT";
objectsNoun[9] = 255;
objectsAdjective[9] = 255;
objectsLocation[9] = 252;
objectsLocation_start[9] = 252;
objectsWeight[9] = 1;
objectsWeight_start[9] = 1;
objectsAttrLO[9] = 0;
objectsAttrLO_start[9] = 0;
objectsAttrHI[9] = 0;
objectsAttrHI_start[9] = 0;

objects[10] = "DEAD JEKRA";
objectsNoun[10] = 255;
objectsAdjective[10] = 255;
objectsLocation[10] = 252;
objectsLocation_start[10] = 252;
objectsWeight[10] = 1;
objectsWeight_start[10] = 1;
objectsAttrLO[10] = 0;
objectsAttrLO_start[10] = 0;
objectsAttrHI[10] = 0;
objectsAttrHI_start[10] = 0;

objects[11] = "A SPACECRUISER";
objectsNoun[11] = 255;
objectsAdjective[11] = 255;
objectsLocation[11] = 3;
objectsLocation_start[11] = 3;
objectsWeight[11] = 1;
objectsWeight_start[11] = 1;
objectsAttrLO[11] = 0;
objectsAttrLO_start[11] = 0;
objectsAttrHI[11] = 0;
objectsAttrHI_start[11] = 0;

objects[12] = "THE CRYSTAL";
objectsNoun[12] = 255;
objectsAdjective[12] = 255;
objectsLocation[12] = 20;
objectsLocation_start[12] = 20;
objectsWeight[12] = 1;
objectsWeight_start[12] = 1;
objectsAttrLO[12] = 0;
objectsAttrLO_start[12] = 0;
objectsAttrHI[12] = 0;
objectsAttrHI_start[12] = 0;

objects[13] = "A LARGE PLANT";
objectsNoun[13] = 255;
objectsAdjective[13] = 255;
objectsLocation[13] = 36;
objectsLocation_start[13] = 36;
objectsWeight[13] = 1;
objectsWeight_start[13] = 1;
objectsAttrLO[13] = 0;
objectsAttrLO_start[13] = 0;
objectsAttrHI[13] = 0;
objectsAttrHI_start[13] = 0;

objects[14] = "SOME COVALIUM";
objectsNoun[14] = 255;
objectsAdjective[14] = 255;
objectsLocation[14] = 43;
objectsLocation_start[14] = 43;
objectsWeight[14] = 1;
objectsWeight_start[14] = 1;
objectsAttrLO[14] = 0;
objectsAttrLO_start[14] = 0;
objectsAttrHI[14] = 0;
objectsAttrHI_start[14] = 0;

objects[15] = "A THERMA LANCE";
objectsNoun[15] = 255;
objectsAdjective[15] = 255;
objectsLocation[15] = 84;
objectsLocation_start[15] = 84;
objectsWeight[15] = 1;
objectsWeight_start[15] = 1;
objectsAttrLO[15] = 0;
objectsAttrLO_start[15] = 0;
objectsAttrHI[15] = 0;
objectsAttrHI_start[15] = 0;

objects[16] = "JEKRA";
objectsNoun[16] = 255;
objectsAdjective[16] = 255;
objectsLocation[16] = 101;
objectsLocation_start[16] = 101;
objectsWeight[16] = 1;
objectsWeight_start[16] = 1;
objectsAttrLO[16] = 0;
objectsAttrLO_start[16] = 0;
objectsAttrHI[16] = 0;
objectsAttrHI_start[16] = 0;

objects[17] = "A MAP OF THE WASTELANDS";
objectsNoun[17] = 255;
objectsAdjective[17] = 255;
objectsLocation[17] = 252;
objectsLocation_start[17] = 252;
objectsWeight[17] = 1;
objectsWeight_start[17] = 1;
objectsAttrLO[17] = 0;
objectsAttrLO_start[17] = 0;
objectsAttrHI[17] = 0;
objectsAttrHI_start[17] = 0;

objects[18] = "A LASER RIFLE";
objectsNoun[18] = 255;
objectsAdjective[18] = 255;
objectsLocation[18] = 252;
objectsLocation_start[18] = 252;
objectsWeight[18] = 1;
objectsWeight_start[18] = 1;
objectsAttrLO[18] = 0;
objectsAttrLO_start[18] = 0;
objectsAttrHI[18] = 0;
objectsAttrHI_start[18] = 0;

objects[19] = "A GOLD BAR";
objectsNoun[19] = 255;
objectsAdjective[19] = 255;
objectsLocation[19] = 32;
objectsLocation_start[19] = 32;
objectsWeight[19] = 1;
objectsWeight_start[19] = 1;
objectsAttrLO[19] = 0;
objectsAttrLO_start[19] = 0;
objectsAttrHI[19] = 0;
objectsAttrHI_start[19] = 0;

objects[20] = "A DRINK";
objectsNoun[20] = 255;
objectsAdjective[20] = 255;
objectsLocation[20] = 6;
objectsLocation_start[20] = 6;
objectsWeight[20] = 1;
objectsWeight_start[20] = 1;
objectsAttrLO[20] = 0;
objectsAttrLO_start[20] = 0;
objectsAttrHI[20] = 0;
objectsAttrHI_start[20] = 0;

objects[21] = "A DRINK";
objectsNoun[21] = 255;
objectsAdjective[21] = 255;
objectsLocation[21] = 18;
objectsLocation_start[21] = 18;
objectsWeight[21] = 1;
objectsWeight_start[21] = 1;
objectsAttrLO[21] = 0;
objectsAttrLO_start[21] = 0;
objectsAttrHI[21] = 0;
objectsAttrHI_start[21] = 0;

objects[22] = "A DRINK";
objectsNoun[22] = 255;
objectsAdjective[22] = 255;
objectsLocation[22] = 86;
objectsLocation_start[22] = 86;
objectsWeight[22] = 1;
objectsWeight_start[22] = 1;
objectsAttrLO[22] = 0;
objectsAttrLO_start[22] = 0;
objectsAttrHI[22] = 0;
objectsAttrHI_start[22] = 0;

objects[23] = "A PASS CARD\n\n";
objectsNoun[23] = 255;
objectsAdjective[23] = 255;
objectsLocation[23] = 65;
objectsLocation_start[23] = 65;
objectsWeight[23] = 1;
objectsWeight_start[23] = 1;
objectsAttrLO[23] = 0;
objectsAttrLO_start[23] = 0;
objectsAttrHI[23] = 0;
objectsAttrHI_start[23] = 0;

last_object_number =  23; 
carried_objects = 0;
total_object_messages=24;

